import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shadow/Constants/ColorConstants.dart';

class Loading extends StatelessWidget {

  final color;
  final spinColor;

  Loading({this.color, this.spinColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color == null ? grey1 : this.color,
      child: Center(
        child: SpinKitWave(
          color: spinColor == null ? aquamarine : this.spinColor,
          size: 75,
        ),
      ),
    );
  }
}
