import 'package:flutter/material.dart';

const white = Colors.white;
const aquamarine = Color.fromRGBO(0, 255, 203, 1.00);
const grey1 = Color.fromRGBO(12, 12, 13, 1.00);
const grey5 = Color.fromRGBO(241, 241, 242, 1.00);
const grey4 = Color.fromRGBO(188, 189, 194, 1.00);
const grey2 = Color.fromRGBO(44, 44, 48, 1.00);
const grey3 = Color.fromRGBO(127, 127, 133, 1.00);
final gradientBlue = Color(0xff2b5876);
final gradientPurple = Color(0xff4e4376);
final tiffanyBlue = Color.fromRGBO(2, 170, 176, 1.00);
final caribbeanGreen = Color.fromRGBO(0, 205, 172, 1.00);

//Graph Colors
const graphPurple = Color.fromRGBO(81, 81, 217, 1.00);
const graphRed = Color.fromRGBO(222, 105, 133, 1.00);
const graphYellow = Color.fromRGBO(246, 244, 187, 1.00);
const graphBlue = Color.fromRGBO(0, 178, 255, 100);

var containerDecorationBlueGradient = BoxDecoration(
    gradient: LinearGradient(
        colors: [
          gradientBlue,
          gradientPurple
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight
    ),
    borderRadius: BorderRadius.all(Radius.circular(20))
);

final Shader textLinearGradientPurple = LinearGradient(
  colors: <Color>[
    gradientBlue,
    gradientPurple
  ],
).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

var bluePurpleGradient = LinearGradient(
    colors: [
      gradientBlue,
      gradientPurple
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight
);

var containerDecorationAquamarineGradient = BoxDecoration(
    gradient: LinearGradient(
        colors: [
          caribbeanGreen,
          tiffanyBlue,
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight
    ),
    borderRadius: BorderRadius.all(Radius.circular(20))
);

final Shader textLinearGradientAquamarine = LinearGradient(
  colors: <Color>[
    caribbeanGreen,
    tiffanyBlue,
  ],
).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

var greenBlueGradient = LinearGradient(
    colors: [
      caribbeanGreen,
      tiffanyBlue,
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight
);