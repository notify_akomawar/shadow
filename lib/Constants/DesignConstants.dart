import 'package:flutter/material.dart';

import 'ColorConstants.dart';

FocusNode myFocusNode = new FocusNode();

var searchInputDecoration = InputDecoration(
  labelStyle: TextStyle(
      color: myFocusNode.hasFocus ? Colors.black : Colors.black
  ),
  hintStyle: TextStyle(color: Colors.black),
  focusedBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.black, style: BorderStyle.solid),
  ),
);

const textInputDecoration = InputDecoration(
  labelStyle: TextStyle(color: Colors.black, fontSize: 16),
  hintStyle: TextStyle(color: Colors.black, fontSize: 12),
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Color.fromRGBO(127, 127, 133, 1.00)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  ),
);

var emailInputDecoration = InputDecoration(
  labelStyle: TextStyle(
      color: myFocusNode.hasFocus ? aquamarine : aquamarine,
      fontWeight: FontWeight.bold,
      fontSize: 16
  ),
  errorStyle: TextStyle(color: aquamarine),
  errorBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Color.fromRGBO(240, 86, 86, 1.00))
  ),
  hintStyle: TextStyle(color: Color.fromRGBO(62, 62, 66, 1.00)),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color.fromRGBO(240, 86, 86, 1.00)),
  ),
);

var formInputBoxDecoration = InputDecoration(
  fillColor: Colors.white,
  labelStyle: TextStyle(
      color: myFocusNode.hasFocus ? grey1 : grey1,
      fontWeight: FontWeight.bold,
      fontSize: 16
  ),
  errorStyle: TextStyle(color: aquamarine),
  errorBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Color.fromRGBO(240, 86, 86, 1.00))
  ),
  hintStyle: TextStyle(color: Color.fromRGBO(62, 62, 66, 1.00)),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color.fromRGBO(240, 86, 86, 1.00)),
  ),
);

var passwordInputDecoration = InputDecoration(
  labelText: 'Password',
  labelStyle: TextStyle(
      color: myFocusNode.hasFocus ? aquamarine : aquamarine,
      fontWeight: FontWeight.bold,
      fontSize: 16
  ),
  hintText: 'your password',
  hintStyle: TextStyle(color: Color.fromRGBO(62, 62, 66, 1.00)),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  ),
  errorBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Color.fromRGBO(240, 86, 86, 1.00)),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color.fromRGBO(240, 86, 86, 1.00)),
  ),
);

const whiteTitleTextStyle = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    fontFamily: 'Gilroy'
);

const blackTitleTextStyle = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 24,
    fontFamily: 'Gilroy'
);

const normalWhiteTextStyle = TextStyle(
    color: Colors.white,
    fontFamily: 'Gilroy'
);

const descTextStyle = TextStyle(
  color: Colors.grey,
  fontFamily: 'Gilroy'
);

const amberTextStyle = TextStyle(
    color: Colors.amber,
    fontWeight: FontWeight.bold,
    fontFamily: 'Gilroy'
);

//////////////////////////////////////////////////// grey bold
const grey1BoldTextStyle = TextStyle(
  color: grey1,
  fontWeight: FontWeight.bold,
  fontFamily: 'Gilroy'
);

const grey2BoldTextStyle = TextStyle(
    color: grey2,
    fontWeight: FontWeight.bold,
    fontFamily: 'Gilroy'
);

const grey3BoldTextStyle = TextStyle(
    color: grey3,
    fontWeight: FontWeight.bold,
    fontFamily: 'Gilroy'
);

const grey4BoldTextStyle = TextStyle(
    color: grey4,
    fontWeight: FontWeight.bold,
    fontFamily: 'Gilroy'
);

const grey5BoldTextStyle = TextStyle(
    color: grey5,
    fontWeight: FontWeight.bold,
    fontFamily: 'Gilroy'
);

///////////////////////////////////////////////////////////////// grey normal
const grey1NormalTextStyle = TextStyle(
    color: grey1,
    fontFamily: 'Gilroy'
);

const grey2NormalTextStyle = TextStyle(
    color: grey2,
    fontFamily: 'Gilroy'
);
const grey3NormalTextStyle = TextStyle(
    color: grey3,
    fontFamily: 'Gilroy'
);

const grey4NormalTextStyle = TextStyle(
    color: grey4,
    fontFamily: 'Gilroy'
);
const grey5NormalTextStyle = TextStyle(
    color: grey5,
    fontFamily: 'Gilroy'
);

///////////////////////////////////////////////////////// aquamarine
const aquamarineBoldTextStyle = TextStyle(
  color: aquamarine,
  fontWeight: FontWeight.bold,
  fontFamily: 'Gilroy',
);

const aquamarineNormalTextStyle = TextStyle(
  color: aquamarine,
  fontFamily: 'Gilroy',
);

/////////////////////////////////////////////////////  white
const whiteBoldTextStyle = TextStyle(
  color: white,
  fontWeight: FontWeight.bold,
  fontFamily: 'Gilroy',
);

const whiteNormalTextStyle = TextStyle(
  color: white,
  fontFamily: 'Gilroy',
);





final Shader linearGradientPink = LinearGradient(
  colors: <Color>[Color(0xffDA44bb), Color(0xff8921aa)],
).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));
final Shader linearGradientGreen = LinearGradient(
  colors: <Color>[Color(0xFF38FFC3), Color(0xFF00FFF0)],
).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));