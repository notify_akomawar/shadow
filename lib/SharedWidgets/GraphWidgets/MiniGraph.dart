import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class MiniGraph extends StatelessWidget {
  final double height;
  final double width;
  final List optionsList;
  final bool isColored;
  
  MiniGraph({this.height, this.width, this.optionsList, this.isColored});

  List<Future<double>> get _height {
    List<double> voteCountList = List();
    double maxVoteCount;
    double scaleFactor;

    List notAllowed = ['Enter Option 3', 'Enter Option 4', 'Enter Option 5'];

    for(var option in optionsList) {
      if(!notAllowed.contains(option['option'])) voteCountList.add((option['votes'].toDouble()));
    }

    maxVoteCount = voteCountList.reduce(max);
    maxVoteCount == 0 ? scaleFactor = 0 : scaleFactor = height/maxVoteCount;

    for(int index = 0; index < voteCountList.length; index++) {
      voteCountList[index] *= scaleFactor;
    }

    List<Future<double>> heights = [];

    for(var voteCount in voteCountList) {
      heights.add(Future<double>.value(voteCount));
    }

    return heights;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: Stack(
        children: [
          CustomPaint(
            size: Size(height, width),
            painter: GraphAxis(height: height, width: width),
          ),
          Container(
            height: height,
            width: width,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(width: 2),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 3.0, right: 1),
                    child: FutureBuilder(
                      future: _height[0],
                      initialData: 0.0,
                      builder: (context, snapshot) {
                        return AnimatedContainer(
                          duration: Duration(seconds: 1),
                          height: snapshot.data,
                          decoration: BoxDecoration(
                              color: ColorChooser.graphColors[int.parse(optionsList[0]['color'])],
                              borderRadius: BorderRadius.all(Radius.circular(2))
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 3.0, right: 1),
                    child: FutureBuilder(
                      future: _height[1],
                      initialData: 0.0,
                      builder: (context, snapshot) {
                        return AnimatedContainer(
                          duration: Duration(seconds: 1),
                          height: snapshot.data,
                          decoration: BoxDecoration(
                              color:  ColorChooser.graphColors[int.parse(optionsList[1]['color'])],
                              borderRadius: BorderRadius.all(Radius.circular(2))
                          ),
                        );
                      },
                    ),
                  ),
                ),
                if(_height.length == 3) Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 3.0, right: 1),
                    child: FutureBuilder(
                      future: _height[2],
                      initialData: 0.0,
                      builder: (context, snapshot) {
                        return AnimatedContainer(
                          duration: Duration(seconds: 1),
                          height: snapshot.data,
                          decoration: BoxDecoration(
                              color: ColorChooser.graphColors[int.parse(optionsList[2]['color'])],
                              borderRadius: BorderRadius.all(Radius.circular(2))
                          ),
                        );
                      },
                    ),
                  ),
                ),
                if(_height.length == 4) Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 3.0, right: 1),
                    child: FutureBuilder(
                      future: _height[3],
                      initialData: 0.0,
                      builder: (context, snapshot) {
                        return AnimatedContainer(
                          duration: Duration(seconds: 1),
                          height: snapshot.data,
                          decoration: BoxDecoration(
                              color: ColorChooser.graphColors[int.parse(optionsList[3]['color'])],
                              borderRadius: BorderRadius.all(Radius.circular(2))
                          ),
                        );
                      },
                    )
                  ),
                ),
                if(_height.length == 5) Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 3.0, right: 1),
                    child: FutureBuilder(
                      future: _height[4],
                      initialData: 0.0,
                      builder: (context, snapshot) {
                        return AnimatedContainer(
                          duration: Duration(seconds: 1),
                          height: snapshot.data,
                          decoration: BoxDecoration(
                              color: ColorChooser.graphColors[int.parse(optionsList[4]['color'])],
                              borderRadius: BorderRadius.all(Radius.circular(2))
                          ),
                        );
                      },
                    )
                  ),
                ),
              ],
            )
          )
        ],
      ),
    );
  }
}

class GraphAxis extends CustomPainter {
  final height;
  final width;
  GraphAxis({this.height, this.width});

  @override
  void paint(Canvas canvas, Size size) {
    final p1 = Offset(0, height);
    final p2 = Offset(width, height);
    final paint = Paint()
      ..color = grey5
      ..strokeWidth = 2;
    canvas.drawLine(p1, p2, paint);

    final p3 = Offset(0, height);
    final p4 = Offset(0, 0);
    final paint2 = Paint()
      ..color = grey5
      ..strokeWidth = 2;
    canvas.drawLine(p3, p4, paint2);

  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}