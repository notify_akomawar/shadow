import 'dart:math';

import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';

class Confetti extends StatefulWidget {
  @override
  _ConfettiState createState() => _ConfettiState();
}

class _ConfettiState extends State<Confetti> {
  ConfettiController confettiController;
  int counter = 0;

  @override
  void initState() {
    confettiController = ConfettiController(duration: const Duration(seconds: 1));
    super.initState();
  }

  @override
  void dispose() {
    confettiController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(counter < 1) confettiController.play();
    counter += 1;
    return Stack(
      children: <Widget>[
//        CENTER -- Blast
        Align(
          alignment: Alignment.topCenter,
          child: ConfettiWidget(
            confettiController: confettiController,
            numberOfParticles: 20,
            emissionFrequency: 0.1,
            blastDirectionality: BlastDirectionality.explosive, // don't specify a direction, blast randomly
            shouldLoop: false, // start again as soon as the animation is finished
            colors: const [
              Colors.green,
              Colors.blue,
              Colors.pink,
              Colors.orange,
              Colors.purple
            ], // manually specify the colors to be used
          ),
        ),

//        CENTER RIGHT -- Emit left
//        Align(
//          alignment: Alignment.centerRight,
//          child: ConfettiWidget(
//            confettiController: confettiController,
//            blastDirection: pi, // radial value - LEFT
//            particleDrag: 0.05, // apply drag to the confetti
//            emissionFrequency: 0.05, // how often it should emit
//            numberOfParticles: 20, // number of particles to emit
//            gravity: 0.05, // gravity - or fall speed
//            shouldLoop: false,
//            colors: const [
//              Colors.green,
//              Colors.blue,
//              Colors.pink
//            ], // manually specify the colors to be used
//          ),
//        ),

//        //CENTER LEFT - Emit right
//        Align(
//          alignment: Alignment.centerLeft,
//          child: ConfettiWidget(
//            confettiController: confettiController,
//            blastDirection: 0, // radial value - RIGHT
//            emissionFrequency: 0.6,
//            minimumSize: const Size(10,
//                10), // set the minimum potential size for the confetti (width, height)
//            maximumSize: const Size(50,
//                50), // set the maximum potential size for the confetti (width, height)
//            numberOfParticles: 1,
//            gravity: 0.1,
//          ),
//        ),

//        //TOP CENTER - shoot down
//        Align(
//          alignment: Alignment.topCenter,
//          child: ConfettiWidget(
//            confettiController: confettiController,
//            blastDirectionality: BlastDirectionality.explosive,
//            maxBlastForce: 10, // set a lower max blast force
//            minBlastForce: 2, // set a lower min blast force
//            emissionFrequency: 0.05,
//            numberOfParticles: 50, // a lot of particles at once
//            gravity: 1,
//          ),
//        ),

//        BOTTOM CENTER SHOOT UP
//        Align(
//          alignment: Alignment.bottomCenter,
//          child: ConfettiWidget(
//            confettiController: confettiController,
//            blastDirection: -pi / 2,
//            emissionFrequency: 0.01,
//            numberOfParticles: 20,
//            maxBlastForce: 100,
//            minBlastForce: 80,
//            gravity: 0.3,
//          ),
//        ),
      ],
    );
  }

  Text _display(String text) {
    return Text(
      text,
      style: const TextStyle(color: Colors.white, fontSize: 20),
    );
  }
}
