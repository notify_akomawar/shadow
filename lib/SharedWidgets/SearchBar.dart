import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart' as flappy;
import 'package:flappy_search_bar/search_bar_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:network_to_file_image/network_to_file_image.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Constants/Widgets/Loading.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';
import 'package:shadow/Services/CloudStorageService.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/Services/SearchService.dart';

import 'HomePageElements.dart';


class FlappySearchBar extends StatefulWidget {

  final List toSearch;

  const FlappySearchBar({Key key, this.toSearch}) : super(key: key);

  @override
  _FlappySearchBarState createState() => _FlappySearchBarState();
}

class _FlappySearchBarState extends State<FlappySearchBar> {

  bool visible = true;


  @override
  void initState() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          this.visible = visible ? false : true;
        });
      },
    );  super.initState();
  }

  List results = [];
  int currentResults = 0;
  int currentIndex = -1;
  int currentModifierIndex = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Hero(
        tag: 'Search',
        child: Center(
          child: Container(
            width: MediaQuery.of(context).size.width - 32,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: grey2,
                        borderRadius: BorderRadius.all(Radius.circular(10.0))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Icon(
                              Icons.search,
                              color: grey5,
                            )
                          ),
                          Expanded(
                            flex: 5,
                            child: TextFormField(
                              onChanged: (value) async {
                                value == '' ? currentIndex = -1 : results = [];
                                results = [];
                                _updateResults(value);
                              },
                              style: grey5BoldTextStyle.copyWith(fontSize: 14),
                              decoration: InputDecoration(
                                hintText: 'Search',
                                hintStyle: grey5BoldTextStyle.copyWith(fontSize: 14),
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                counterText: '',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ),
                Visibility(
                  visible: visible,
                  child: Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Modifier(
                                  onModified: _onModified,
                                  isSelected: currentModifierIndex == 0,
                                  modifier: "Shades",
                                  index: 0,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child : Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Modifier(
                                  onModified: _onModified,
                                  isSelected: currentModifierIndex == 1,
                                  modifier: "Polls",
                                  index: 1,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Modifier(
                                  onModified: _onModified,
                                  isSelected: currentModifierIndex == 2,
                                  modifier: "Shadows",
                                  index: 2,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                  ),
                ),
                Expanded(
                    flex: 2,
                    child: ListView.builder(itemBuilder: (BuildContext context, int index){
                      if(index >= currentResults) {
                        currentResults = currentResults + 10 >= results.length
                            ? results.length
                            : currentResults + 10;
                        Future.delayed(Duration(seconds: 1));
                      }
                      if(index == results.length && results.length == 0) {

                        return Text("No Shades match your search", style: whiteNormalTextStyle,);
                      }

                      if(index >= results.length) {
                        return null;
                      }
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4.0),
                        child: ShadeCard(
                          shade: results[index][0],
                          image: results[index][1],
                          index: index,
                          onTap: _selectCard, // change to open shade page ------------------------
                          isSelected: currentIndex == index ? true : false,
                        ),
                      );
                    }),
                ),
              ]
            ),
          ),
        ),
      ),
    );
  }

  _selectCard(int index) {
    setState(() {
      currentIndex = currentIndex == index ? -1 : index;
    });
  }

  _onModified(int index) {
    if(currentModifierIndex != index) {
      setState(() {
        currentModifierIndex = index;
        currentIndex = -1;
        results = [];
      });
    }
  }

  void _updateResults(String value) async {
    if(currentModifierIndex == 0) {
      List tempList = [];
      await SearchService()
          .searchShadeByName(value)
          .then((QuerySnapshot docs) async {
        for (int i = 0; i < docs.documents.length; i++) {
          List tempUrl = await CloudStorageService().getURL(docs.documents[i]['imagePath']);
          String url = tempUrl[1];

          tempList.add(
              [
                Shade(
                  tags: docs.documents[i]['tags'],
                  uid: docs.documents[i]['uid'],
                  authorUsername: docs.documents[i]['authorUsername'],
                  access: docs.documents[i]['access'],
                  name: docs.documents[i]['name'],
                  description: docs.documents[i]['description'],
                  imagePath: docs.documents[i]['imagePath'],
                  guidelines: docs.documents[i]['guidelines'],
                  shadowList: docs.documents[i]['shadowList'],
                  pollList: docs.documents[i]['pollList'],
                  pendingShadows: docs.documents[i]['pendingShadows'],
                  adminList:  docs.documents[i]['adminList'],
                  modifiers: docs.documents[i]['modifiers'],
                  nameLowercase: docs.documents[i]['nameLowercase'],
                  numShadows: docs.documents[i]['numShadows'],
                  timestamp: docs.documents[i]['timestamp'],
                ),
                Image(image:
                NetworkToFileImage(
                    url: tempUrl[1],
                    file: File(tempUrl[0]),
                    debug: true)
                )
              ]
          );
        }
      });

      int maxIndex;
      int max;
      for(int i = 0; i < tempList.length - 1; i++) {
        maxIndex = -1;
        max = -1;
        for(int j = i; j < tempList.length; j++) {
          if(tempList[j][0].numShadows > max) {
            maxIndex = j;
            max = tempList[j][0].numShadows;
          }
        }
        var temp = tempList[maxIndex];
        tempList[maxIndex] = tempList[i];
        tempList[i] = temp;
      }

      setState(() {
        results = tempList;
        currentResults = results.length < 2 ? results.length : 2;
      });
    }
  }
}



class Modifier extends StatelessWidget {
  
  final Function onModified;
  final bool isSelected;
  final String modifier;
  final int index;

  const Modifier({Key key, this.onModified, this.isSelected, this.modifier, this.index}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onModified(index),
      child: Material(
        color: isSelected ? aquamarine : grey4,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        child: Center(
            child: Text(
                modifier,
                style: grey1BoldTextStyle
            )
        ),
      ),
    );
  }
  
}
