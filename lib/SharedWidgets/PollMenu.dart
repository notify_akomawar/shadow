import 'dart:ui';

import 'package:flutter/material.dart';

import '../Constants/ColorConstants.dart';

class PollMenu extends StatelessWidget {
  final Function closeMenu;
  PollMenu({this.closeMenu});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 200,
        width: MediaQuery.of(context).size.width - 64,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: Column(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back, color: white),
                  onPressed: () => closeMenu(),
                )
              ],
            ),
          ),
        )
      ),
    );
//    return BackdropFilter(
//      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
//      child: Center(
//        child: Container(
//          height: 100,
//          width: MediaQuery.of(context).size.width - 64,
//          child: Column(
//            children: [
//              IconButton(
//                icon: Icon(Icons.arrow_back, color: white),
//                onPressed: () => closeMenu(),
//              )
//            ],
//          ),
//        ),
//      )
//    );
  }
}
