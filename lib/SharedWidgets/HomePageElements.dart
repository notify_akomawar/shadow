import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Pages/DiscussionPage/DiscussionPage.dart';
import 'package:shadow/Pages/VotePage/VotePage.dart';
import 'package:shadow/Services/CloudStorageService.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/SharedWidgets/GraphWidgets/MiniGraph.dart';
import 'package:shadow/SharedWidgets/PollMenu.dart';

class PollCardGraph extends StatelessWidget {

  final Poll poll;
  final Function graphPressed;
  final String shadowUID;

  const PollCardGraph({this.poll, this.graphPressed, this.shadowUID});

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Container(
        width: MediaQuery.of(context).size.width - 16,
        child: Material(
            color: grey2,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            child: Column(
              children: [
                SizedBox(height: 16),
                Expanded(
                  flex: 0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      children: [
//                    Container(
//                      height: 30,
//                      width: 30,
//                      child: ClipRRect(
//                        borderRadius: BorderRadius.all(Radius.circular(5)),
//                        child: Image.network(imageUrl),
//                      ),
//                    ),
                        Expanded(
                          flex: 1,
                          child: Text(
                            poll.name,
                            style: grey5BoldTextStyle.copyWith(fontSize: 16),
                          ),
                        ),
//                        Spacer(),
                        IconButton(
                          icon: Icon(Icons.bookmark, color: aquamarine,),
                          onPressed: (){},
                        ),
                        IconButton(
                          icon: Icon(Icons.more_vert, color: aquamarine,),
                          onPressed: (){},
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 16),
                Expanded(
                    flex: 0,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Vote2(
                              rating: poll.rating,
                              voteCount: poll.voteCount,
                              poll: poll,
                              graphPressed: graphPressed,
                            ),
                          ),
                          Expanded(
                              flex: 4,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Text(
                                  poll.description,
                                  softWrap: true,
                                  maxLines: 6,
                                  overflow: TextOverflow.ellipsis,
                                  style: whiteNormalTextStyle.copyWith(fontSize: 12),
                                ),
                              )
                          )
                        ],
                      ),
                    )
                ),
                SizedBox(height: 16),
                Expanded(
                    flex: 0,
                    child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left:8.0),
                            child: Padding(
                              padding: const EdgeInsets.only(left:8.0),
                              child: Text(
                                "shade[" + poll.shadeName + "]",
                                style: grey4BoldTextStyle.copyWith(fontSize: 12),
                              ),
                            ),
                          ),
                          Spacer(),
                          Padding(
                            padding: const EdgeInsets.only(right:8.0),
                            child: Text(
                              "shadow[" + poll.authorUsername + "]",
                              style: aquamarineBoldTextStyle.copyWith(fontSize: 14),
                            ),
                          ),
                        ]
                    )
                ),
                SizedBox(height: 16),
              ],
            )
        ),
      ),
    );
  }
}

class Vote2 extends StatelessWidget {

  final int rating;
  final int voteCount;
  final Poll poll;
  final Function graphPressed;

  const Vote2({this.rating, this.voteCount, this.poll, this.graphPressed});


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text(
                    'votes: ',
                    style: grey4BoldTextStyle.copyWith(fontSize: 20),
                  ),
                  Text(
                    voteCount.toString(),
                    style: aquamarineBoldTextStyle.copyWith(fontSize: 24),
                  ),
                ],
              ),
              SizedBox(height: 16),
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      rating.toString(),
                      style: aquamarineBoldTextStyle.copyWith(fontSize: 24),
                    ),
                  ),
                  SizedBox(width: 8),
                  Expanded(
                    flex: 3,
                    child: GestureDetector(
                      onTap: () => graphPressed(poll),
                      child: MiniGraph(
                        height: 40,
                        width: 50,
                        optionsList: poll.options,
                        isColored: false,
                      ),
                    ),
                  )
                ],
              ),
            ]
        ),
      ),
    );
  }
}



class Vote extends StatelessWidget {

  final int rating;
  final int voteCount;

  const Vote({Key key, this.rating, this.voteCount,}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                this.rating.toString(),
                style: aquamarineBoldTextStyle.copyWith(fontSize: 40),
              ),
              SizedBox(height: 16),
              RichText(
                text: TextSpan(
                    children: [
                      TextSpan(
                          text: this.voteCount.toString(),
                          style:aquamarineBoldTextStyle.copyWith(fontSize: 18)
                      ),
                      TextSpan(
                        text: " votes",
                        style: whiteBoldTextStyle.copyWith(fontSize: 18),
                      ),
                    ]
                ),
              ),
            ]
        ),
      ),
    );
  }
}

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////// Cards for Search Bar Results/////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

class ShadeCard extends StatefulWidget {

  final Shade shade;
  final Image image;
  final Function onTap;
  final bool isSelected;
  final String shadowUID;
  final int index;

  const ShadeCard({Key key, this.shade, this.image, this.shadowUID, this.onTap, this.isSelected = false, this.index}) : super(key: key);


  @override
  _ShadeCardState createState() => _ShadeCardState();
}

class _ShadeCardState extends State<ShadeCard> {

  @override
  Widget build(BuildContext context) {
    return Material(
      color: !widget.isSelected ? grey2 : aquamarine,
      borderRadius: BorderRadius.all(Radius.circular(8.0)),
      child: Container(
        width: MediaQuery.of(context).size.width - 64,
        child: ListTile(
          onTap: () async {
            await widget.onTap(widget.shade, widget.shadowUID); // widget.onTap(widget.shade)
          },
          dense: true,
          leading: Container(
            height: 48,
            width: 48,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              child: widget.image,
            ),
          ),
          title: Text(
              "shade[" + widget.shade.name + "]",
              style: TextStyle(
                color: aquamarine,
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.bold,
              )
          ),
          subtitle: Text(
              "shadow[" + widget.shade.authorUsername + "]",
              style: TextStyle(
                color: grey4,
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.bold,
              )
          ),
          trailing: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
                widget.shade.numShadows.toString(),
                style: TextStyle(
                  color: aquamarine,
                  fontFamily: 'Gilroy',
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                )
            ),
          ),
        ),
      ),
    );
  }
}

