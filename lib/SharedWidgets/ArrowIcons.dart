import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';

class ArrowIcons extends StatefulWidget {
  final Function nextPage;
  final Function previousPage;

  ArrowIcons({this.nextPage, this.previousPage});

  @override
  _ArrowIconsState createState() => _ArrowIconsState();
}

class _ArrowIconsState extends State<ArrowIcons> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 8,
      bottom: 0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_upward, color: aquamarine),
            onPressed: widget.nextPage()
          ),
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: aquamarine,
            ),
            child: IconButton(
              color: grey1,
              icon: Icon(Icons.arrow_downward),
              onPressed: widget.previousPage(),
            ),
          ),
          SizedBox(height: 16)
        ],
      ),
    );
  }
}
