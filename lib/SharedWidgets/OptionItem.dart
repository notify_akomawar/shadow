import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';

class OptionItem extends StatefulWidget {
  final String answer;
  final Function onTap;

  OptionItem({this.answer, this.onTap});

  @override
  _OptionItemState createState() => _OptionItemState();
}

class _OptionItemState extends State<OptionItem> {
  List<Color> colors = [white, white];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: Row(
          children: [
            SizedBox(width: 24),
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget.answer,
                style: TextStyle(
                    color: white,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Gilroy'
                ),
              ),
            ),)
          ],
        ),
      ),
    );
  }
}
