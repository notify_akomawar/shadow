import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:network_to_file_image/network_to_file_image.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Services/CloudStorageService.dart';
import 'package:shadow/Services/SearchService.dart';

class MCQ extends StatelessWidget {
  final String optionLetter;
  final String text;
  final Color color;
  MCQ({this.color, this.text, this.optionLetter});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
        height: 40,
        width: MediaQuery.of(context).size.width - 64,
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Container(
              width: MediaQuery.of(context).size.width - 64,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                          color: grey2,
                          border: Border.all(color: aquamarine),
                          borderRadius: BorderRadius.all(Radius.circular(5))
                      ),
                      child: Center(child: Text(optionLetter, style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 24))),
                    ),
                  ),
                  Text(
                    text,
                    style: TextStyle(
                        color: color == aquamarine ? grey2 : aquamarine,
                        fontSize: 24,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            )
        ),
      ),
    );
  }
}

class InputFormField extends StatelessWidget {

  final String hintText;
  final String input;
  final Function updateString;
  final int maxLength;


  const InputFormField({Key key, this.hintText, this.updateString, this.input, this.maxLength}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
        height: 40,
        width: MediaQuery.of(context).size.width - 64,
        decoration: BoxDecoration(
            color: grey2,
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: TextFormField(
            initialValue: (input == "Enter Shade Name" || input == "Enter Poll Name") ? null : input,
            style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
            decoration: InputDecoration(
              hintText: hintText,
              hintStyle: TextStyle(color: grey3, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              counterText: '',
            ),
            maxLength: maxLength,
            validator: (value) => value.isEmpty ? '' : null,
            onChanged: (value) {updateString(value);},
          ),
        ),
      ),
    );
  }
}

class NextButton extends StatelessWidget {
  final Function onPressed;
  NextButton({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
        height: 40,
        width: 250,
        decoration: BoxDecoration(
            color: grey2,
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: Center(
          child: MaterialButton(
            elevation: 0,
            color: Colors.transparent,
            child: Text(
              'NEXT',
              style: TextStyle(
                color: aquamarine,
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: () => onPressed(),
          ),
        ),
      ),
    );
  }
}

class OptionSelector extends StatefulWidget {

  final List values;
  final Function updateOption;
  final Function updateOptionDesc;
  final Function updateOptionColor;
  final int numOptions;

  const OptionSelector({Key key, this.values, this.updateOption, this.updateOptionDesc, this.updateOptionColor, this.numOptions}) : super(key: key);

  @override
  _OptionSelectorState createState() => _OptionSelectorState();

}

class _OptionSelectorState extends State<OptionSelector> {

  int indexOfVisible = -1;
  bool colorVisible = false;
  int currentColorIndex = -1;

  int initiallyExpandedIndex = -1;

  _onExpandCard(int index) {
    setState(() {
      initiallyExpandedIndex = initiallyExpandedIndex == index ? -1 : index;
    });
  }
  _onVisibilityChanged(int index) {
    setState(() {
      indexOfVisible = indexOfVisible == index ? -1 : index;
    });
  }

  _onOptionChanged(int index, String option) {
    widget.updateOption(index, option);
  }

  _onDescChanged(int index, String desc) {
    widget.updateOptionDesc(index, desc);
  }

  _onOptionColorChanged(int index, Color color) {
    widget.updateOptionColor(index, color);
  }

  _pushColorChooser(int optionIndex) {
    setState(() {
      currentColorIndex = optionIndex == -1 ? currentColorIndex : optionIndex;
      colorVisible = !colorVisible;
    });
  }

  _popColorChooser() {
    setState(() {
      colorVisible = !colorVisible;
    });
  }

  _saveColorSelected(int colorIndex) {
    setState(() {
      widget.values[3][currentColorIndex][2] = colorIndex;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Visibility(
          visible: !colorVisible,
          child: Container(
            width: MediaQuery.of(context).size.width - 64,
            height: MediaQuery.of(context).size.height/3,
            child: Column(
              children: [
                Material(
                  color: grey1,
                  child: Container(
                    width: MediaQuery.of(context).size.width - 64,
                    height: MediaQuery.of(context).size.height/3,
                    child: ListView.builder(
                      itemCount: widget.numOptions,
                      itemBuilder:(BuildContext context, int index){
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 4.0),
                          child: OptionCardView(
                            values: widget.values,
                            optionNumber: index + 1,
                            color: ColorChooser.graphColors[widget.values[3][index][2]],
                            onOptionChanged: _onOptionChanged,
                            onDescChanged: _onDescChanged,
                            onOptionColorChanged: _onOptionColorChanged,
                            onVisibilityChanged: _onVisibilityChanged,
                            onExpandCard: _onExpandCard,
                            toggleColorVisible: _pushColorChooser,
                            cardVisible: indexOfVisible == -1 ? true : index == indexOfVisible,
                            initiallyExpanded: initiallyExpandedIndex == -1 ? false : index == initiallyExpandedIndex,
                          ),
                        );
                      }
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
        Visibility(
          visible: colorVisible,
          child: ColorChooser(
            toggleColorVisible: _popColorChooser,
            saveColorSelected: _saveColorSelected,
          ),
        ),
      ],
    );
  }
}

class OptionCardView extends StatelessWidget {

  final List values;
  final int optionNumber;
  final Color color;
  final Function onOptionChanged;
  final Function onDescChanged;
  final Function onOptionColorChanged;
  final Function onVisibilityChanged;
  final Function toggleColorVisible;
  final Function onExpandCard;
  final bool cardVisible;
  final bool initiallyExpanded;


  const OptionCardView({Key key, this.color, this.optionNumber, this.onOptionChanged, this.onDescChanged, this.onOptionColorChanged, this.onVisibilityChanged, this.cardVisible, this.values, this.toggleColorVisible, this.initiallyExpanded, this.onExpandCard}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      maintainSize: false,
      visible: cardVisible,
      child: Container(
        color: grey1,
        width: MediaQuery.of(context).size.width - 64,
        child: Row(
          children: [
            Expanded(
              flex: 5,
              child: Material(
                color: grey2,
                borderRadius: BorderRadius.all(Radius.circular(7.0)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 7,
                      child: Card(
                        color: grey2,
                        child: ExpansionTile(
                          initiallyExpanded: initiallyExpanded,
                          onExpansionChanged: (change) {
                            onVisibilityChanged(optionNumber - 1);
                            onExpandCard(optionNumber - 1);
                          },
                          backgroundColor: grey2,
                          title: Text(
                            "Option " + optionNumber.toString(),
                            style: TextStyle(
                                backgroundColor: grey2,
                              color: aquamarine,
                              fontFamily: 'Gilroy',
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),

                          ),
                          children: <Widget> [
                            Padding(
                              padding: const EdgeInsets.fromLTRB(16.0, 4.0, 8.0, 4.0),
                              child: TextFormField(
                                style: TextStyle(
                                    color: grey4,
                                    fontFamily: 'Gilroy',
                                    fontWeight: FontWeight.bold
                                ),
                                initialValue:
                                  values[3][optionNumber - 1][0] == 'Enter Option ' + (optionNumber).toString() ||
                                    values[3][optionNumber - 1][0] == ''?
                                    null : values[3][optionNumber - 1][0],
                                decoration: InputDecoration(
                                  hintText: "Enter Option " + optionNumber.toString(),
                                  hintStyle: TextStyle(color: grey3,
                                      fontFamily: 'Gilroy',
                                      fontWeight: FontWeight.bold),
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  counterStyle: TextStyle(fontFamily: 'Gilroy', color: aquamarine),

                                ),
                                maxLength: 100,
                                onChanged: (value){
                                  onOptionChanged(optionNumber - 1,value.toString());
                                },
                              ),
                            ),

                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                              child: Container(
                                height: 100,
                                width: MediaQuery.of(context).size.width - 64,
                                decoration: BoxDecoration(
                                    color: grey2,
                                    borderRadius: BorderRadius.all(Radius.circular(10))
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                                  child: TextFormField(
                                    keyboardType: TextInputType.multiline,
                                    initialValue:
                                      values[3][optionNumber - 1][1] == 'Enter Description (optional)' ||
                                        values[3][optionNumber - 1][1] == '' ?
                                        null : values[3][optionNumber - 1][1],
                                    maxLines: 3,
                                    maxLength: 200,
                                    style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                                    decoration: InputDecoration(
                                        hintText: "Enter Description for Option " + optionNumber.toString(),
                                        hintStyle: TextStyle(color: grey3, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                                        border: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        enabledBorder: InputBorder.none,
                                        errorBorder: InputBorder.none,
                                        disabledBorder: InputBorder.none,
                                        counterStyle: TextStyle(fontFamily: 'Gilroy', color: aquamarine)
                                    ),
                                    validator: (value) => value.isEmpty ? '' : null,
                                    onChanged: (value) {
                                      onDescChanged(optionNumber - 1, value);
                                    },
                                  ),
                                ),
                              ),
                            )
                          ]
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: SizedBox(
                width: 32,
                height: 32,
                child: GestureDetector(
                  onTap: () => toggleColorVisible(optionNumber - 1),
                  // (){
                  //  setState((){
                  //    colorChooserVisible = true;
                  //    colorIndex = optionNumber - 1;
                  //  })
                  // }
                  child: Material(
                    color: color,
                    borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class OptionButton extends StatelessWidget {

  final Function changeOptions;
  final bool add;
  const OptionButton({Key key, this.changeOptions, this.add =true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
        height: 40,
        width: 40,
        child: Material(
          color: grey2,
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
          child: IconButton(
            padding: EdgeInsets.all(0),
            icon: Icon(this.add ? Icons.add : Icons.remove, color: aquamarine,),
            color: grey2,
            onPressed: () {
              changeOptions();
            },
          ),
        ),
      ),
    );
  }

}

class MiniSearchBar extends StatefulWidget {
  final values;
  final Function updateForm;
  final Function nextPage;

  MiniSearchBar({this.values, this.updateForm, this.nextPage});

  @override
  _MiniSearchBarState createState() => _MiniSearchBarState();
}

class _MiniSearchBarState extends State<MiniSearchBar> {
  var searchString = '';
  var selectedIndex = -1;
  List resultList = [];

  void onTap(String name, String shadeID, int index) {
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {
      selectedIndex != index ? selectedIndex = index : selectedIndex = -1;
      widget.values[0][0] = name;
      widget.values[0][1] = shadeID;
      widget.updateForm(1);
      widget.nextPage();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 64,
      child: Column(
        children: [
          Container(
            height: 40,
            width: MediaQuery.of(context).size.width - 64,
            decoration: BoxDecoration(
                color: grey2,
                borderRadius: BorderRadius.all(Radius.circular(10))
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Row(
                children: [
                  Expanded(flex: 1, child: Icon(Icons.search, color: grey5,)),
                  Expanded(
                    flex: 7,
                    child: TextFormField(
                      initialValue: widget.values[0][0] == 'Shade Name' ? '' : widget.values[0][0],
                      style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                      decoration: InputDecoration(
                        hintText: 'Shade Name',
                        hintStyle: TextStyle(color: grey3, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                      ),
                      validator: (value) => value.isEmpty ? '' : null,
                      onChanged: (value) async {
                        resultList = [];
                        List tempList = [];
                        await SearchService()
                            .searchShadeByName(value)
                            .then((QuerySnapshot docs) async {
                          for(int i = 0; i < docs.documents.length; i++) {
                            List url = await CloudStorageService().getURL(docs.documents[i]['imagePath']);
                            tempList.add(
                                [
                                  docs.documents[i]['name'],
                                  Image(image:
                                  NetworkToFileImage(
                                      url: url[1],
                                      file: File(url[0]),
                                      debug: true)
                                  ),
                                  docs.documents[i]['authorUsername'],
                                  docs.documents[i]['shadowList'],
                                  docs.documents[i]['uid'],
                                  docs.documents[i]['numShadows'],
                                ]
                            );
                          }

                          int maxIndex;
                          int max;
                          for(int i = 0; i < tempList.length - 1; i++) {
                            maxIndex = -1;
                            max = -1;
                            for(int j = i; j < tempList.length; j++) {
                              if(tempList[j][5] > max) {
                                maxIndex = j;
                                max = tempList[j][5];
                              }
                            }
                            var temp = tempList[maxIndex];
                            tempList[maxIndex] = tempList[i];
                            tempList[i] = temp;
                          }

                        });
                        setState(() {
                          resultList = tempList;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          if(resultList != null) Container(
            height: MediaQuery.of(context).size.height / 3,
            child: ListView.builder(
              itemCount: resultList.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.only(top: 8.0),
//                  child: Material(
//                    color: grey2,
//                    borderRadius: BorderRadius.all(Radius.circular(7.0)),

                  child: MiniShadeCard(
                    index: index,
                    isSelected: index == selectedIndex ? true : false,
                    onTap: onTap,
                    shadeName: resultList[index][0],
                    shadeID: resultList[index][4],
                    image: resultList[index][1],
                    authorUsername: resultList[index][2],
                    shadowsCount: resultList[index][3].length,
                    numShadows: resultList[index][5],
//                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class MiniShadeCard extends StatefulWidget {
  final String shadeName;
  final String shadeID;
  final Image image;
  final String authorUsername;
  final int shadowsCount;
  final Function onTap;
  final bool isSelected;
  final int index;
  final int numShadows;

  MiniShadeCard({this.shadeName, this.shadeID, this.image, this.authorUsername, this.shadowsCount, this.onTap, this.isSelected, this.index, this.numShadows});

  @override
  _MiniShadeCardState createState() => _MiniShadeCardState();
}

class _MiniShadeCardState extends State<MiniShadeCard> {

  @override
  Widget build(BuildContext context) {
    return Material(
      color: !widget.isSelected ? grey2 : aquamarine,
      borderRadius: BorderRadius.all(Radius.circular(8.0)),
      child: Container(
        width: MediaQuery.of(context).size.width - 64,
        child: ListTile(
          onTap: () {
            widget.onTap(widget.shadeName, widget.shadeID, widget.index);
          },
          dense: true,
          leading: Container(
            height: 48,
            width: 48,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                child: widget.image
            ),
          ),
          title: Text(
              "shade[" + widget.shadeName + "]",
              style: TextStyle(
                color: !widget.isSelected ? aquamarine : grey1,
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.bold,
              )
          ),
          subtitle: Text(
              "shadow[" + widget.authorUsername + "]",
              style: TextStyle(
                color: !widget.isSelected ? grey4 : grey2,
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.bold,
              )
          ),
          trailing: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
                widget.numShadows.toString(),
                style: TextStyle(
                  color: !widget.isSelected ? aquamarine : grey1,
                  fontFamily: 'Gilroy',
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                )
            ),
          ),
        ),
      ),
    );
  }
}

class ColorChooser extends StatefulWidget {

  final Function toggleColorVisible;
  final Function saveColorSelected;

  static const List<Color> graphColors = [graphPurple, graphRed, graphYellow,  grey5, aquamarine, graphBlue];

  ColorChooser({Key key, this.toggleColorVisible, this.saveColorSelected}) : super(key: key);

  @override
  _ColorChooserState createState() => _ColorChooserState();
}

class _ColorChooserState extends State<ColorChooser> {

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.toggleColorVisible();
        widget.saveColorSelected(currentIndex);
      },
      child: Container(
        width: 332,
        height: 232,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          color: grey3,
          borderRadius: BorderRadius.all(Radius.circular(10.0))
        ),
        child: Column(
          children: [
            SizedBox(height: 8.0),
            Row(
              children: [
                SizedBox(width: 8.0),
                ColorTile(index: 0, onSelect: _onColorChange),
                SizedBox(width: 8.0),
                ColorTile(index: 1, onSelect: _onColorChange),
                SizedBox(width: 8.0),
                ColorTile(index: 2, onSelect: _onColorChange),
                SizedBox(width: 8.0),
                RaisedButton(
                  color: aquamarine,
                  onPressed: (){
                    widget.toggleColorVisible();
                    widget.saveColorSelected(currentIndex);
                  },
                  child: Text(
                      "Confirm",
                      style: grey1BoldTextStyle.copyWith(fontSize: 16)
                  ),
                ),
              ]
            ),
            SizedBox(height: 8.0),
            Row(
                children: [
                  SizedBox(width: 8.0),
                  ColorTile(index: 3, onSelect: _onColorChange),
                  SizedBox(width: 8.0),
                  ColorTile(index: 4, onSelect: _onColorChange),
                  SizedBox(width: 8.0),
                  ColorTile(index: 5, onSelect: _onColorChange),
                  SizedBox(width: 8.0),
                  RaisedButton(
                    color: grey2,
                    onPressed: (){
                      widget.toggleColorVisible();
                    },
                    child: Text(
                        "Cancel",
                        style: aquamarineBoldTextStyle.copyWith(fontSize: 16)
                    ),
                  ),

                ]
            ),
            Divider(color: grey1, thickness: 1),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints(
                    maxWidth: 152,
                    minWidth: 152
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                        "Color Selected: ",
                      style: grey1BoldTextStyle.copyWith(fontSize: 16),
                    ),
                  ),
                ),
//                        Spacer(),
                Center(
                  child: Container(
                    height: 64,
                    width: 64,
                    decoration: BoxDecoration(
                      color: ColorChooser.graphColors[currentIndex],
                      borderRadius: BorderRadius.all(Radius.circular(10.0))
                    ),

                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  _onColorChange(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}

class ColorTile extends StatelessWidget {

  final int index;
  final Function onSelect;

  const ColorTile({Key key, this.index, this.onSelect}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onSelect(index),
      child: Container(
        width: 64,
        height: 64,
        decoration: BoxDecoration(
          color: ColorChooser.graphColors[index],
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
      ),
    );
  }
}

class TagSearchBar extends StatefulWidget {
  final values;
  final Function updateForm;
  final Function nextPage;
  final Function updateSelected;

  TagSearchBar({this.values, this.updateForm, this.nextPage, this.updateSelected});

  @override
  _TagSearchBarState createState() => _TagSearchBarState();
}

class _TagSearchBarState extends State<TagSearchBar> {
  var searchString = '';
  List resultList = [];
  TextEditingController _textEditingController;

  bool topVisible = true;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          topVisible = visible ? false : true;
        });
      },
    );
  }


  void onTap(String name) {
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {
      searchString = '';
      _textEditingController.clear();
      for(int i = 0; i < widget.values[5].length; i++) {
        if(widget.values[5][i] == 'tag ' + (i+1).toString()) {
          widget.values[5][i] = name;
          resultList = [];
          break;
        }
        else if (widget.values[5][i] == name) {
          resultList = [];
          break;
        }
      }
      widget.updateSelected();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 64,
      child: Column(
        children: [
          Container(
            height: 40,
            width: MediaQuery.of(context).size.width - 64,
            decoration: BoxDecoration(
                color: grey2,
                borderRadius: BorderRadius.all(Radius.circular(10))
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Row(
                children: [
                  Expanded(flex: 1, child: Icon(Icons.search, color: grey5,)),
                  Expanded(
                    flex: 7,
                    child: TextFormField(
                      controller: _textEditingController,
                      style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                      decoration: InputDecoration(
                        hintText: 'Tag Name',
                        hintStyle: TextStyle(color: grey3, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                      ),
                      validator: (value) => value.isEmpty ? '' : null,
                      onChanged: (value) async {
                        searchString = value;
                        resultList = [];
                        List tempList = [];
                        await SearchService()
                            .searchTagByName(value)
                            .then((QuerySnapshot docs) async {
                          for(int i = 0; i < docs.documents.length; i++) {
                            tempList.add(
                              [
                                docs.documents[i]['name'],
                                docs.documents[i]['shadeCount'],
                              ]
                            );
                          }
//                          int maxIndex;
//                          int max;
//                          for(int i = 0; i < tempList.length - 1; i++) {
//                            maxIndex = -1;
//                            max = -1;
//                            for(int j = i; j < tempList.length; j++) {
//                              if(tempList[j][1] > max) {
//                                maxIndex = j;
//                                max = tempList[j][1];
//                              }
//                            }
//                            var temp = tempList[maxIndex];
//                            tempList[maxIndex] = tempList[i];
//                            tempList[i] = temp;
//                          }
//
                        });

                        setState(() {
                          resultList = tempList;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          if(resultList != null && resultList.length == 0 && searchString != '') Center(
            child: Column(
              children: [
                Text("this tag doesn't exist, be the first to make it.", style: TextStyle(
                  color: graphRed,
                  fontSize: 12
                )),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Container(
                          height: 35,
                          decoration: BoxDecoration(
                            color: grey2,
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              searchString,
                              style: aquamarineBoldTextStyle,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              onTap(searchString);
                            });
//                            ShadowProfile shadow = await DatabaseService().getShadow(widget.uid);
//                            await TagCreationService().createCustomTag(searchString, widget.uid, shadow.username);
                          },
                          child: Container(
                            height: 35,
                            decoration: BoxDecoration(
                              color: aquamarine,
                              borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Icon(
                                Icons.add,
                                color: grey1,
                              ),
//                          child: Text(
//                            "Create Custom Tag",
//                            style: grey2BoldTextStyle.copyWith(fontSize: 12),
//                          ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          if(resultList != null) Container(
            height: MediaQuery.of(context).size.height / 5,
            child: ListView.builder(
              itemCount: resultList.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: MiniTagCard(
                    index: index,
                    onTap: onTap,
                    tagName: resultList[index][0],
                    shadeCount: resultList[index][1],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class MiniTagCard extends StatefulWidget {
  final String tagName;
  final int shadeCount;
  final Function onTap;
  final int index;

  MiniTagCard({this.tagName, this.shadeCount, this.onTap, this.index});

  @override
  _MiniTagCardState createState() => _MiniTagCardState();
}

class _MiniTagCardState extends State<MiniTagCard> {

  @override
  Widget build(BuildContext context) {
    return Material(
      color: grey2,
      borderRadius: BorderRadius.all(Radius.circular(8.0)),
      child: Container(
        width: MediaQuery.of(context).size.width - 64,
        child: ListTile(
          onTap: () {
            widget.onTap(widget.tagName);
          },
          dense: true,
          title: Text(
              "tag[" + widget.tagName + "]",
              style: TextStyle(
                color: aquamarine,
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.bold,
              )
          ),
          trailing: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
                widget.shadeCount.toString(),
                style: TextStyle(
                  color: aquamarine,
                  fontFamily: 'Gilroy',
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                )
            ),
          ),
        ),
      ),
    );
  }
}


