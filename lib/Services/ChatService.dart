import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shadow/Models/MessageModel.dart';
import 'package:shadow/Models/ThreadModel.dart';
class ChatService {
  final shadeUID;
  final pollUID;

  ChatService({this.shadeUID, this.pollUID});

  List messageList(DocumentSnapshot snapshot) {
    return snapshot.data['messages'];
  }

  Stream<List> get pollMessages {
    return Firestore.instance
      .collection('shades').document(shadeUID)
      .collection('polls').document(pollUID)
      .collection('chats').document('main')
      .snapshots().map(messageList);
  }

  List<Thread> getThread(QuerySnapshot snapshot) {
    return snapshot.documents.map((document) {
      return Thread(
        threadMessages: List<Message>.from(document.data['threadMessages'].map((item) {
          return Message(
            message: item['message'],
            option: item['option'],
            selected: item['selected'],
            timestamp: DateTime.fromMillisecondsSinceEpoch(item['timestamp']),
            authorUID: item['authorUID'],
            username: item['username']
          );
        })),
        childMessages: List<Message>.from(document.data['childMessages'].map((item) {
          return Message(
              message: item['message'],
              option: item['option'],
              selected: item['selected'],
              timestamp: DateTime.fromMillisecondsSinceEpoch(item['timestamp']),
              authorUID: item['authorUID'],
              username: item['username']
          );
        })),
        chatName: document.data['chatName'],
        threadName: document.data['threadName'],
        creatorUID: document.data['creatorUID'],
        creatorUsername: document.data['creatorUsername'],
        pollUID: document.data['pollUID'],
        shadeUID: document.data['shadeUID'],
        threadUID: document.data['threadUID'],
        timestamp: DateTime.fromMillisecondsSinceEpoch(document.data['timestamp']),
      );
    }).toList();
  }

  Stream<List> get threads {
    return Firestore.instance
      .collection('shades').document(shadeUID)
      .collection('polls').document(pollUID)
      .collection('threads').snapshots().map(getThread);
  }

  Future createThread(String creatorUsername, String shadeUID, String pollUID, String threadUID, String threadName, String chatName, String creatorUID, List<Map<String, dynamic>> messages) async {
    return await Firestore.instance.collection('shades')
      .document(shadeUID).collection('polls').document(pollUID)
      .collection('threads').document(threadUID).setData({
        'creatorUsername' : creatorUsername,
        'threadUID' : threadUID,
        'threadName' : threadName,
        'shadeUID' : shadeUID,
        'pollUID' : pollUID,
        'chatName' : chatName,
        'creatorUID' : creatorUID,
        'threadMessages' : messages,
        'childMessages' : [],
        'timestamp' : DateTime.now().toUtc().millisecondsSinceEpoch,
    });
  }

  Future createThreadMessage(String shadeUID, String pollUID, String threadUID, String message, String option, bool selected, String authorUID, String username) async {
    return await Firestore.instance.collection('shades')
      .document(shadeUID).collection('polls').document(pollUID)
      .collection('threads').document(threadUID).updateData({
      'childMessages': FieldValue.arrayUnion(
        [
          {
          'message' : message,
          'option' : option,
          'selected' : selected,
          'timestamp' : DateTime.now().toUtc().millisecondsSinceEpoch,
          'authorUID' : authorUID,
          'username' : username
          }
        ]
      )
    });
  }

  Future createMessage(String shadeUID, String pollUID,
      String authorUID, String username, String message, String option,
      {String chatName = "main", bool selected = false}) async {


    return await Firestore.instance.collection('shades')
        .document(shadeUID).collection('polls').document(pollUID)
        .collection('chats').document(chatName).updateData({
      'messages' : FieldValue.arrayUnion(
          [
            {
              'message' : message,
              'option' : option,
              'selected' : selected,
              'timestamp' : DateTime.now().toUtc().millisecondsSinceEpoch,
              'authorUID' : authorUID,
              'username' : username
            }
          ]
      ),
    });
  }
}
