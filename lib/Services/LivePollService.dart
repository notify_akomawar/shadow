import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';

class LivePollService {

  final String uid;

  LivePollService({this.uid});

  List pollsVotedFromSnapshot(DocumentSnapshot snapshot) {
    return [snapshot.data['pollsVoted'], snapshot.data['createdPolls'], snapshot.data['createdShades']];
  }

  Stream<List> get livePollPage {
    return Firestore.instance.collection('users').document(uid).snapshots().map(pollsVotedFromSnapshot);
  }
  
  Future<Poll> getPollVoted(String shadeUID, String pollUID) async {
    print(pollUID + " " + shadeUID);
    return await Firestore.instance.collection('shades').document(shadeUID).collection('polls').document(pollUID).get().then((value) {
      if(value == null) return null;
      Map<String, bool> temp = value.data['shadowsRated'] != null ? new Map<String, bool>.from(value.data['shadowsRated']) : {};
      Map<String, List> temp1 = value.data['shadowsVoted'] != null ? new Map<String, List>.from(value.data['shadowsVoted']) : {};
      List tempList = [
        value.data['option1'],
        value.data['option2'],
        value.data['option3'],
        value.data['option4'],
        value.data['option5'],
      ];
      
      return Poll(
          options: tempList,
          uid: value.data['uid'],
          live: value.data['live'],
          authorUID: value.data['authorUID'],
          shadeName: value.data['shadeName'],
          shadeUID: value.data['shadeID'],
          imagePath: value.data['imagePath'],
          rating: value.data['rating'],
          name: value.data['name'],
          description: value.data['description'],
          modifiers: value.data['modifiers'],
          voteCount: value.data['voteCount'],
          authorUsername: value.data['authorUsername'],
          timestamp: DateTime.fromMillisecondsSinceEpoch(value.data['timestamp']),
          shadowsRated: temp,
          shadowsVoted: temp1
      );
    });
  }

  Future<Shade> getCreatedShades(String shadeUID) async {
    return await Firestore.instance.collection('shades').document(shadeUID).get().then((doc) {
      return Shade(
        tags: doc.data['tags'],
        uid: doc.data['uid'],
        authorUsername: doc.data['authorUsername'],
        access: doc.data['access'],
        name: doc.data['name'],
        description: doc.data['description'],
        imagePath: doc.data['imagePath'],
        guidelines: doc.data['guidelines'],
        shadowList: doc.data['shadowList'],
        pollList: doc.data['pollList'],
        pendingShadows: doc.data['pendingShadows'],
        adminList: doc.data['adminList'],
        modifiers: doc.data['modifiers'],
        nameLowercase: doc.data['nameLowercase'],
        numShadows: doc.data['numShadows'],
        timestamp: DateTime.fromMillisecondsSinceEpoch(doc.data['timestamp']),
        numPolls: doc.data['numPolls'],
      );
    });
  }

  Future<List<Poll>> getPolls(List uidList) async {
    List<Poll> polls = [];
    for(var inputUID in uidList) {
      polls.addAll([await getPollVoted(inputUID['shadeUID'], inputUID['pollUID'])]);
    }

    for(int i = 0; i < polls.length/2; i++) {
      var temp = polls[i];
      polls[i] = polls[polls.length - 1];
      polls[polls.length - 1 - i] = temp;
    }

//    votedPolls.sort((a, b) => b.timestamp.compareTo(a.timestamp));
    return polls;
  }

  Future<List<Shade>> getShades(List uidList) async {
    List<Shade> createdShades = [];
    for(var inputUID in uidList) {
      createdShades.addAll([await getCreatedShades(inputUID)]);
    }

    for(int i = 0; i < createdShades.length/2; i++) {
      var temp = createdShades[i];
      createdShades[i] = createdShades[createdShades.length - 1];
      createdShades[createdShades.length - 1 - i] = temp;
    }

    return createdShades;
  }
}