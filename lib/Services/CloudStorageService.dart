import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class CloudStorageService {
  Future getURL(String imagePath) async {
    Directory cacheDir = await getTemporaryDirectory();
    String cachePath = cacheDir.path;

    StorageReference storageReference = FirebaseStorage.instance.ref().child(imagePath);
    String downloadURL = await storageReference.getDownloadURL();

    return ['$cachePath/$imagePath', downloadURL];
  }

  Future getImage(String imagePath) async {
    Directory cacheDir = await getTemporaryDirectory();
    String cachePath = cacheDir.path;

    bool exists = await File('$cachePath/$imagePath').exists();
    if(exists) {
      return Image.file(File('$cachePath/$imagePath'));
    } else {
      StorageReference storageReference = FirebaseStorage.instance.ref().child(imagePath);
      String downloadURL = await storageReference.getDownloadURL();
      return Image.network('$cachePath/$imagePath');
    }
  }
}