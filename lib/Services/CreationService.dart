import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shadow/Services/DatabaseService.dart';
import 'package:shadow/Services/FirestoreService.dart';

class ShadeCreationService {
  final uid;
  final shadowID;
  ShadeCreationService({this.uid, this.shadowID});

  Future createShade(List values) async {
    final author = await FirestoreService().getShadow(shadowID);
    if(values[2].toString() =='public')
      return await Firestore.instance.collection('shades').document(uid).setData({
        'uid' : uid,
        'authorUsername' : author.username,
        'access' : values[2].toString(),
        'name' : values[1].toString(),
        'description' : values[3].toString(),
        'imagePath' : 'shades/${uid.toString().trim()}.png',
        'guidelines' : values[6].toString(),
        'shadowList' : [shadowID.toString()],
        'pollList' : [],
        'adminList' : [shadowID.toString()],
        'modifiers' : [],
        'nameLowercase' : values[1].toString().toLowerCase(),
        'numShadows' : 1,
        'numPolls' : 0,
        'timestamp' : DateTime.now().toUtc().millisecondsSinceEpoch,
        'tags': [values[5][0], values[5][1], values[5][2], values[5][3], values[5][4],],
      });
    else
      return await Firestore.instance.collection('shades').document(uid).setData({
        'uid' : uid,
        'authorUsername' : author.username,
        'access' : values[2].toString(),
        'name' : values[1].toString(),
        'description' : values[3].toString(),
        'imagePath' : 'shades/${uid.toString().trim()}.png',
        'guidelines' : values[6].toString(),
        'shadowList' : [shadowID.toString()],
        'pollList' : [],
        'adminList' : [shadowID.toString()],
        'modifiers' : [],
        'nameLowercase' : values[1].toString().toLowerCase(),
        'numShadows' : 1,
        'numPolls' : 0,
        'timestamp' : DateTime.now().toUtc().millisecondsSinceEpoch,
        'tags': [values[5][0], values[5][1], values[5][2], values[5][3], values[5][4],],
        'pendingShadows' : [],
        'acceptedShadows': [shadowID],
      });
  }

//  Future addShadeToShadow(List values) async {
//    final author = await FirestoreService().getShadow(shadowID);
//    return await Firestore.instance.collection('users').document(shadowID.toString().trim()).collection('createdShades').document(uid).setData({
//      'uid' : uid,
//      'authorUsername' : author.username,
//      'access' : values[2].toString(),
//      'name' : values[1].toString(),
//      'description' : values[3].toString(),
//      'imagePath' : 'shades/${uid.toString().trim()}.png',
//      'guidelines' : values[6].toString(),
//      'shadowList' : [shadowID.toString()],
//      'pollList' : [],
//      'pendingShadows' : [],
//      'adminList' : [shadowID.toString()],
//      'modifiers' : [],
//      'nameLowercase' : values[1].toString().toLowerCase(),
//      'numShadows' : 1,
//      'numPolls' : 0,
//      'timestamp' : DateTime.now().toUtc().millisecondsSinceEpoch,
//      'tags': [values[5][0], values[5][1], values[5][2], values[5][3], values[5][4],],
//    });
//  }

  Future addFollowedShadeToShadow() async {
    return await Firestore.instance.collection('users').document(shadowID.toString().trim()).updateData({
      'joinedShades' : FieldValue.arrayUnion([uid]),
    });
  }

  Future addShadeUIDToTag(String tagName) async {
    return await Firestore.instance.collection('tags').document(tagName.trim()).updateData({
      "shades" : FieldValue.arrayUnion([uid]),
      'shadeCount' : FieldValue.increment(1),
    });
  }

  Future addToUserCreatedShades(String shadowUID, String shadeUID) async {
    return await Firestore.instance.collection('users').document(shadowUID).updateData({
      'createdShades' : FieldValue.arrayUnion([shadeUID])
    });
  }
}

class PollCreationService {
  final uid;
  final shadeID;
  final shadowID;
  PollCreationService({this.uid, this.shadeID, this.shadowID});

  Future addToUserPolls(String shadowUID, String shadeUID, String pollUID) async {
    return await Firestore.instance.collection('users').document(shadowUID).updateData({
      'createdPolls' : FieldValue.arrayUnion([{
        'shadeUID' : shadeUID,
        'pollUID' : pollUID
      }])
    });
  }

  Future updatePollsInShade() async {
    return await Firestore.instance.collection('shades').document(shadeID).updateData({
      'numPolls' : FieldValue.increment(1)
    });
  }

  Future createPoll(List values) async {
    final authorUsername = await FirestoreService().getShadow(shadowID);
    return await Firestore.instance.collection('shades').document(shadeID).collection('polls').document(uid).setData({
      'uid': uid,
      'live': true,
      'authorUsername': authorUsername.username,
      'authorUID': authorUsername.uid,
      'name': values[1].toString(),
      'description': values[2].toString(),
      'shadeName': values[0][0].toString(),
      'shadeID': values[0][1].toString(),
      'option1': {
        'option' : values[3][0][0].toString(),
        'description' : values[3][0][1].toString(),
        'color' : values[3][0][2].toString(),
        'votes' : 0
      },
      'option2': {
        'option' : values[3][1][0].toString(),
        'description' : values[3][1][1].toString(),
        'color' : values[3][1][2].toString(),
        'votes' : 0
      },
      'option3': {
        'option' : values[3][2][0].toString(),
        'description' : values[3][2][1].toString(),
        'color' : values[3][2][2].toString(),
        'votes' : 0
      },
      'option4' : {
        'option' : values[3][3][0].toString(),
        'description' : values[3][3][1].toString(),
        'color' : values[3][3][2].toString(),
        'votes' : 0
      },
      'option5' : {
        'option' : values[3][4][0].toString(),
        'description' : values[3][4][1].toString(),
        'color' : values[3][4][2].toString(),
        'votes' : 0
      },
      'imagePath': 'shades/${values[0][1].toString().trim()}.png',
      'voteCount': 0,
      'voteCountLimit': 5,
      'rating': 0,
      'modifiers': [values[5][0], values[5][1]],
      'timestamp': DateTime.now().toUtc().millisecondsSinceEpoch,
      'shadowsRated': {},
      'shadowsVoted': {},
    });
  }
  
  Future addInitialDiscussionToPoll() async {
    return await Firestore.instance.collection('shades').document(shadeID)
        .collection('polls').document(uid)
        .collection('chats').document('main').setData({
      'messages' : [],
    });
  }

  Future addPollToShadow(List values) async {
    final authorUsername = await FirestoreService().getShadow(shadowID);
    return await Firestore.instance.collection('users').document(shadowID).collection('createdPolls').document(uid).setData({
      'uid': uid,
      'live': true,
      'authorUsername': authorUsername.username,
      'authorUID': authorUsername.uid,
      'name': values[1].toString(),
      'description': values[2].toString(),
      'shadeName': values[0][0].toString(),
      'shadeID': values[0][1].toString(),
      'option1': {
        'option' : values[3][0][0].toString(),
        'description' : values[3][0][1].toString(),
        'color' : values[3][0][2].toString(),
        'votes' : 0
      },
      'option2': {
        'option' : values[3][1][0].toString(),
        'description' : values[3][1][1].toString(),
        'color' : values[3][1][2].toString(),
        'votes' : 0
      },
      'option3': {
        'option' : values[3][2][0].toString(),
        'description' : values[3][2][1].toString(),
        'color' : values[3][2][2].toString(),
        'votes' : 0
      },
      'option4' : {
        'option' : values[3][3][0].toString(),
        'description' : values[3][3][1].toString(),
        'color' : values[3][3][2].toString(),
        'votes' : 0
      },
      'option5' : {
        'option' : values[3][4][0].toString(),
        'description' : values[3][4][1].toString(),
        'color' : values[3][4][2].toString(),
        'votes' : 0
      },
      'imagePath': 'shades/${values[0][1].toString().trim()}.png',
      'voteCount': 0,
      'voteCountLimit': 5,
      'rating': 0,
      'modifiers': [values[5][0], values[5][1]],
      'timestamp' : DateTime.now().toUtc().millisecondsSinceEpoch,
      'shadowsRated' : {},
      'shadowsVoted': {},
    });
  }
}

class TagCreationService {
  Future createCustomTag(String tagName, String authorUID, String authorName) async {
    return await Firestore.instance.collection('tags').document(tagName).setData({
      'name' : tagName,
      'shadeCount' : 0,
      'shades' : [],
      'authorUID' : authorUID,
      'authorName' : authorName,
    });
  }
}