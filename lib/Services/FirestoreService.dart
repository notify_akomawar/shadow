import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';

class FirestoreService {
  
  final String shadowID;

  FirestoreService({this.shadowID});
  
  final CollectionReference shadesCollection = Firestore.instance.collection('shades');

  Future<List<Shade>> getShades() async {
    return await Firestore.instance.collection('shades').getDocuments().then((snapshot) {
      List<Shade> documentList = [];
      for(int i = 0; i < snapshot.documents.length; i++){
        documentList.add(Shade(
            uid: snapshot.documents[i]['uid'],
            authorUsername: snapshot.documents[i]['authorUsername'],
            access: snapshot.documents[i]['access'],
            name: snapshot.documents[i]['name'],
            description: snapshot.documents[i]['description'],
            imagePath: snapshot.documents[i]['imagePath'],
            guidelines: snapshot.documents[i]['guidelines'],
            shadowList: snapshot.documents[i]['shadowList'],
            pollList: snapshot.documents[i]['pollList'],
            pendingShadows: snapshot.documents[i]['pendingShadows'],
            adminList:  snapshot.documents[i]['adminList'],
            modifiers: snapshot.documents[i]['modifiers'],
            nameLowercase: snapshot.documents[i]['nameLowercase'],
            numShadows: snapshot.documents[i]['numShadows'],
            timestamp: snapshot.documents[i]['timestamp'],
        ));
      }
      return documentList;
    });
  }

  List<Shade> shadeList(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Shade(
        tags: doc.data['tags'],
        uid: doc.data['uid'],
        authorUsername: doc.data['authorUsername'],
        access: doc.data['access'],
        name: doc.data['name'],
        description: doc.data['description'],
        imagePath: doc.data['imagePath'],
        guidelines: doc.data['guidelines'],
        shadowList: doc.data['shadowList'],
        pollList: doc.data['pollList'],
        pendingShadows: doc.data['pendingShadows'],
        adminList: doc.data['adminList'],
        modifiers: doc.data['modifiers'],
        nameLowercase: doc.data['nameLowercase'],
        numShadows: doc.data['numShadows'],
        timestamp: doc.data['timestamp'],
        numPolls: doc.data['numPolls'],
      );
    }).toList();
  }

  Stream<List<Shade>> get shades {
    return shadesCollection.snapshots().map(shadeList);
  }

  List<Shade> votedPollsList(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Shade(
        tags: doc.data['tags'],
        uid: doc.data['uid'],
        authorUsername: doc.data['authorUsername'],
        access: doc.data['access'],
        name: doc.data['name'],
        description: doc.data['description'],
        imagePath: doc.data['imagePath'],
        guidelines: doc.data['guidelines'],
        shadowList: doc.data['shadowList'],
        pollList: doc.data['pollList'],
        pendingShadows: doc.data['pendingShadows'],
        adminList: doc.data['adminList'],
        modifiers: doc.data['modifiers'],
        nameLowercase: doc.data['nameLowercase'],
        numShadows: doc.data['numShadows'],
        timestamp: doc.data['timestamp'],
        numPolls: doc.data['numPolls'],
      );
    }).toList();
  }

  Future<void> deletePoll(String shadeUID, String pollUID){
    return Firestore.instance.collection('shades').document(shadeUID).collection('polls').document(pollUID).delete();
  }

  Future decrementNumPolls(String shadeUID) async {
    return await Firestore.instance.collection('shades').document(shadeUID).updateData({
      'numPolls' : FieldValue.increment(-1),
    });
  }

  final CollectionReference usersCollection = Firestore.instance.collection('users');

  ShadowProfile userProfileFromSnapshot(DocumentSnapshot snapshot) {
    return ShadowProfile(
      uid: snapshot.documentID.toString(),
      username: snapshot.data['username'],
      email: snapshot.data['email'],
      joinedShades: snapshot.data['joinedShades'],
      light: snapshot.data['light'],
      description: snapshot.data['description'],
      profileImage: snapshot.data['profileImage'],
      shadowers: snapshot.data['shadowers']
    );
  }

  Stream<ShadowProfile> get shadowProfile {
    return usersCollection.document(shadowID).snapshots().map(userProfileFromSnapshot);
  }

  Future<ShadowProfile> getShadow(String uid) async {
    return await usersCollection.getDocuments().then((snapshot) {
      ShadowProfile shadowProfile;
//      ShadowProfile({this.light, this.uid, this.username, this.description, this.email, this.profileImage});
      for(var document in snapshot.documents) {
        if (document.documentID == uid) {
          shadowProfile = ShadowProfile(
            light: document['light'],
            uid: document.documentID.toString(),
            username: document['username'].toString(),
            description: document['description'],
            email: document['email'].toString(),
            profileImage: document['profileImage'].toString(),
            joinedShades: document['joinedShades'],
            shadowers: document['shadowers']
          );
        }
      }
      return shadowProfile;
    });
  }

  Future addUserToPending(String uid, String name, String shadeID) async {
    List addedShadeID = List();
    addedShadeID.add(uid);

    return await Firestore.instance.collection('shades').document(shadeID.trim()).updateData({
      'pendingShadows' : FieldValue.arrayUnion(addedShadeID)
    });
  }

  Future removeUsersPending(String uid, String name, String shadeID) async {
    List addedShadeID = List();
    addedShadeID.add(uid);

    print(addedShadeID);
    return await Firestore.instance.collection('shades').document(shadeID.trim()).updateData({
      'pendingShadows' : FieldValue.arrayRemove(addedShadeID),
    });
  }

  Future addUsersJoinedShades(String uid, String name, String shadeID) async {
    List addedShadeID = List();
    addedShadeID.add(shadeID);

    return await Firestore.instance.collection('users').document(uid.trim()).updateData({
      'joinedShades' : FieldValue.arrayUnion(addedShadeID)
    });
  }

  Future removeUsersJoinedShades(String uid, String name, String shadeID) async {
    List addedShadeID = List();
    addedShadeID.add(shadeID);

    return await Firestore.instance.collection('users').document(uid.trim()).updateData({
      'joinedShades' : FieldValue.arrayRemove(addedShadeID),
//      'createdShades' : FieldValue.arrayRemove(addedShadeID),
    });
  }

  Future addShadowToShade(String uid, String name, String shadeID) async {
    List addedShadeID = List();
    addedShadeID.add(uid);

    return await Firestore.instance.collection('shades').document(shadeID.trim()).updateData({
      'shadowList' : FieldValue.arrayUnion(addedShadeID)
    });
  }


  Future removeShadowFromShade(String uid, String name, String shadeID) async {
    List addedShadeID = List();
    addedShadeID.add(uid);

    return await Firestore.instance.collection('shades').document(shadeID.trim()).updateData({
      'shadowList' : FieldValue.arrayRemove(addedShadeID)
    });
  }

  Future updateRating(DocumentReference firestoreReference, String shadowUID, int currentRating, int change, {bool reset = false}) async {
    if(!reset) {
      return await firestoreReference.updateData({
        'rating': FieldValue.increment(change),
        'shadowsRated.$shadowUID': change > 0,
      });
    }
    else {
      return await firestoreReference.updateData({
        'rating': FieldValue.increment(change),
        'shadowsRated.$shadowUID': FieldValue.delete()
      });
    }
  }

  Future updateVote(DocumentReference firestoreReference, String shadowUID, int optionIndex, int change, String option, String justification) async {
    return await firestoreReference.updateData({
      'option${optionIndex + 1}.votes': FieldValue.increment(change),
      'voteCount' : FieldValue.increment(change),
      'shadowsVoted.$shadowUID' : [option, justification],
    });
  }

  Future addToUserVoted(String shadowUID, String shadeUID, String pollUID) async {
    return await Firestore.instance.collection('users').document(shadowUID).updateData({
      'pollsVoted' : FieldValue.arrayUnion([{
        'shadeUID' : shadeUID,
        'pollUID' : pollUID
      }])
    });
  }

  Future editUserDescription(String shadowUID, String description) async {
    return await Firestore.instance.collection('users').document(shadowUID).updateData({
      'description' : description,
    });
  }

  Future editShadeInfo(String shadeUID, String description, String guidelines) async {
    return await Firestore.instance.collection('shades').document(shadeUID).updateData({
      'description' : description,
      'guidelines' : guidelines,
    });
  }
}