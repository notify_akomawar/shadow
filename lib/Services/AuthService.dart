
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Services/DatabaseService.dart';

class AuthService {
  
  final FirebaseAuth _auth = FirebaseAuth.instance;
  
  // create user object using firebase user
  FirebaseShadow _userFromFirebaseUser(FirebaseUser user) {
   return user != null ? FirebaseShadow(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<FirebaseShadow> get firebaseShadow {
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future registerWithEmailAndPassword(String username, String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;

      //create document in database for user
      await DatabaseService(uid: user.uid).createUser(username, email, password);

      return _userFromFirebaseUser(user);
    } catch(e) {
      return null;
    }
  }

  Future signWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      DatabaseService.setUID(user.uid);
      return _userFromFirebaseUser(user);
    } catch(error) {
      return error;
    }
  }

  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch(e) {
      print(e.toString());
      return null;
    }
  }
}