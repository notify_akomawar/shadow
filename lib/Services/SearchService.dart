
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ProfileModel.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';

import 'DatabaseService.dart';

class SearchService {
  List<ShadowProfile> getUsers(String username, List<ShadowProfile> toSearch) {
    toSearch.sort((a, b) =>
        a.username.toString().compareTo(b.username.toString()));
    List<ShadowProfile> match = new List<ShadowProfile>();
    for (ShadowProfile profile in toSearch) {
      var tempString = profile.username.substring(0, username.length)
          .toLowerCase()
          .trim();
      if (tempString == username) match.add(profile);
    }
    return match;
  }

  bool isUsernameCreated(String username, List<Profile> users) {
    for (Profile profile in users) {
      if (profile.username.toLowerCase() == username.toLowerCase())
        return false;
    }
    return true;
  }

  searchShadeByName(String toSearch) {
    toSearch = toSearch.toLowerCase();

    var documents = Firestore.instance
        .collection('shades')
        .where('nameLowercase',
        isGreaterThanOrEqualTo: toSearch,
        isLessThan: toSearch.length != 0 ? String.fromCharCode(
            toSearch.codeUnitAt(0) + 1) : "")
        .orderBy('nameLowercase')
        .limit(50)
        .getDocuments();
    return documents;
  }

  searchTagByName(String toSearch) {
    var documents = Firestore.instance
        .collection('tags')
        .where('name',
        isGreaterThanOrEqualTo: toSearch,
        isLessThan: toSearch.length != 0 ? String.fromCharCode(
            toSearch.codeUnitAt(0) + 1) : "")
        .orderBy('name', descending: false)
        .limit(10)
        .getDocuments();
    return documents;
  }

  Future<bool> morePollsAvailable(int lastRefresh, String shadeID) async {
    return await Firestore.instance
        .collection('shades').document(shadeID.trim()).collection('polls')
        .where('timestamp', isGreaterThan: lastRefresh)
        .limit(1)
        .getDocuments().then((snapshot) async {
          if(snapshot.documents.length > 0)
            return true;
          return false;
    });
  }

  // ignore: missing_return
  Future<List<Poll>> getPollsByRecent(int pastTime, String shadeID, bool latestFirst) async {

    List<Poll> documentList = [];

    return await Firestore.instance
        .collection('shades')
        .document(shadeID.trim())
        .collection('polls')
        .where('timestamp', isGreaterThan: pastTime)
        .orderBy('timestamp', descending: true)
        .getDocuments()
        .then((snapshot) async {
      for(int i = 0; i < snapshot.documents.length; i++){
        Map<String, bool> temp = snapshot.documents[i]['shadowsRated'] != null ? new Map<String, bool>.from(snapshot.documents[i]['shadowsRated']) : {};
        Map<String, List> temp1 = snapshot.documents[i]['shadowsVoted'] != null ? new Map<String, List>.from(snapshot.documents[i]['shadowsVoted']) : {};
        List tempList = [
          snapshot.documents[i]['option1'],
          snapshot.documents[i]['option2'],
          snapshot.documents[i]['option3'],
          snapshot.documents[i]['option4'],
          snapshot.documents[i]['option5'],
        ];
        documentList.add(
            Poll(
              options: tempList,
              uid: snapshot.documents[i]['uid'],
              live: snapshot.documents[i]['live'],
              authorUID: snapshot.documents[i]['authorUID'],
              shadeName: snapshot.documents[i]['shadeName'],
              shadeUID: snapshot.documents[i]['shadeID'],
              imagePath: snapshot.documents[i]['imagePath'],
              rating: snapshot.documents[i]['rating'],
              name: snapshot.documents[i]['name'],
              description: snapshot.documents[i]['description'],
              modifiers: snapshot.documents[i]['modifiers'],
              voteCount: snapshot.documents[i]['voteCount'],
              authorUsername: snapshot.documents[i]['authorUsername'],
              timestamp: DateTime.fromMillisecondsSinceEpoch(snapshot.documents[i]['timestamp']),
              shadowsRated: temp,
              shadowsVoted: temp1,
              voteCountLimit: snapshot.documents[i]['voteCountLimit']
            )
        );
      }
      return documentList;
    });
  }
}