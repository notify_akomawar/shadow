import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadeModel.dart';

class DiscoverService {
  Future<List<Shade>> getTrendingShades(int pastTime) async {
    List<Shade> trendingShadeList = [];
    List<List<String>> shadeUIDList = [];

    //get lists of shadeUIDs based on top 10 highest tags
    await Firestore.instance
        .collection('tags')
        .orderBy('shadeCount')
        .limit(10)
        .getDocuments()
        .then((snapshot) async {
      for (var document in snapshot.documents) {
        shadeUIDList.add(document['shades']);
      }
    });
  }

  Future<List> getTagShadeUIDs(String tag) async {
    List shadeUIDList = [];

    return await Firestore.instance.collection('tags').document(tag)
        .get()
        .then((doc) {
      return (doc.data['shades']);
    }).catchError((error) {
      print('Error' + error.toString());
    });
  }

  Future<List<Shade>> getShadesFromTag(String tag) async {
    List<Shade> shadeList = [];

    List shadeUIDlist = await getTagShadeUIDs(tag);

    for (String shadeUID in shadeUIDlist) {
      shadeList.add(await Firestore.instance
          .collection('shades')
          .document(shadeUID)
          .get()
          .then((document) {
        return Shade(
            uid: document['uid'],
            authorUsername: document['authorUsername'],
            access: document['access'],
            name: document['name'],
            description: document['description'],
            imagePath: document['imagePath'],
            guidelines: document['guidelines'],
            shadowList: document['shadowList'],
            pollList: document['pollList'],
            pendingShadows: document['pendingShadows'],
            adminList: document['adminList'],
            modifiers: document['modifiers'],
            numShadows: document['numShadows'],
            nameLowercase: document['nameLowercase'],
            timestamp:
            DateTime.fromMillisecondsSinceEpoch(document['timestamp']),
            tags: document['tags'],
            numPolls: document['numPolls']);
      }));
    }

    return shadeList;
  }

  Future<List<Shade>> getTopShades(String orderBy) async {
    List<Shade> topShadesList = [];
    var orderKey = '';

    switch (orderBy) {
      case 'Shade':
        orderKey = 'numShadows';
        break;
      case 'Polls':
        orderKey = 'numPolls';
        break;
    }

    return await Firestore.instance
        .collection('shades')
        .orderBy(orderKey, descending: true)
        .limit(10)
        .getDocuments()
        .then((snapshot) async {
      for (int i = 0; i < snapshot.documents.length; i++) {
        topShadesList.add(Shade(
            uid: snapshot.documents[i]['uid'],
            authorUsername: snapshot.documents[i]['authorUsername'],
            access: snapshot.documents[i]['access'],
            name: snapshot.documents[i]['name'],
            description: snapshot.documents[i]['description'],
            imagePath: snapshot.documents[i]['imagePath'],
            guidelines: snapshot.documents[i]['guidelines'],
            shadowList: snapshot.documents[i]['shadowList'],
            pollList: snapshot.documents[i]['pollList'],
            pendingShadows: snapshot.documents[i]['pendingShadows'],
            adminList: snapshot.documents[i]['adminList'],
            modifiers: snapshot.documents[i]['modifiers'],
            numShadows: snapshot.documents[i]['numShadows'],
            nameLowercase: snapshot.documents[i]['nameLowercase'],
            timestamp: DateTime.fromMillisecondsSinceEpoch(
                snapshot.documents[i]['timestamp']),
            tags: snapshot.documents[i]['tags'],
            numPolls: snapshot.documents[i]['numPolls']));
      }
      return topShadesList;
    });
  }
}