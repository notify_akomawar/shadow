import 'dart:io';

import 'package:flutter/material.dart';
import 'package:network_to_file_image/network_to_file_image.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Constants/Widgets/Loading.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/PollCards/PollCardDead.dart';
import 'package:shadow/Services/CloudStorageService.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/Services/SearchService.dart';

class ShadeProfilePage extends StatefulWidget {

  final Shade shade;
  final String shadowUID;

  const ShadeProfilePage({Key key, this.shade, this.shadowUID}) : super(key: key);

  @override
  _ShadeProfilePageState createState() => _ShadeProfilePageState();
}

class _ShadeProfilePageState extends State<ShadeProfilePage> {
  bool searchVisible = false;
  double opacity = 1.0;
  double yTranslate = 0.0;
  double animatedWidth = 0.0;

  List<Poll> shadePolls = List();
  String joined = 'Join Shade';

  TextEditingController _textEditingController;
  PageController pageController =  PageController(
      initialPage: 2
  );

  String dropdownValue = 'Latest';
  List<String> dropdownList = ['Trending', 'Latest', 'Oldest', 'Most Votes'];
  var timeAgo = (DateTime.now()).subtract(Duration(hours: 24)).toUtc().millisecondsSinceEpoch;
  List url;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();

    CloudStorageService().getURL(widget.shade.imagePath).then((value) {
      setState(() {
        url = value;
      });
    });

    SearchService().getPollsByRecent(timeAgo, widget.shade.uid, true).then((result) {
      setState(() {
       shadePolls = result;
      });
    });

    FirestoreService().getShadow(widget.shadowUID).then((result) {
      if(!result.joinedShades.contains(widget.shade.uid) && widget.shade.pendingShadows.contains(widget.shadowUID))
        setState(() {
          joined = 'Pending';
        });
      else
        setState(() {
          joined = 'Leave Shade';
        });
    });
  }

  void refreshShadePolls() {
    SearchService().getPollsByRecent(timeAgo, widget.shade.uid, true).then((result) {
      setState(() {
        shadePolls = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: grey1,
        body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width - 32,
            height: MediaQuery.of(context).size.height - 32,
            child: Column(
              children: [
                Expanded(
                  flex: 0,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 0,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: InkWell(
                            child: Icon(Icons.arrow_back, color: grey5,),
                            onTap: () => Navigator.pop(context),
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 1,
                          child: Text(
                            "shade[" + widget.shade.name + "]",
                            style: grey5BoldTextStyle.copyWith(fontSize: 24),
                          )
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: grey2,
                          borderRadius: BorderRadius.all(Radius.circular(5))
                      ),
                      child: Row(
                        children: [
                          Expanded(flex: 1,
                              child: IconButton(
                                  icon: Icon(Icons.search),
                                  color: grey4,
                                  splashColor: Colors.transparent,
                                  highlightColor: Colors.transparent,
                                  onPressed: () {
                                    setState(() {
                                      yTranslate = MediaQuery.of(context).size.height - 250;
                                      opacity = 0.0;
                                      animatedWidth = MediaQuery.of(context).size.width / 5;
                                      searchVisible = true;
                                    });
                                  }
                              )
                          ),
                          Expanded(
                            flex: 5,
                            child: TextFormField(
                              controller: _textEditingController,
                              onTap: () {
                                setState(() {
                                  yTranslate = MediaQuery.of(context).size.height - 250;
                                  opacity = 0.0;
                                  animatedWidth = MediaQuery.of(context).size.width / 5;
                                  searchVisible = true;
                                });
                              },
                              onChanged: (value) {

                              },
                              style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                              decoration: InputDecoration(
                                hintText: 'Search',
                                hintStyle: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: AnimatedOpacity(
                                duration: Duration(milliseconds: 250),
                                opacity: 1.0 - opacity,
                                child: IconButton(
                                  splashColor: Colors.transparent,
                                  highlightColor: Colors.transparent,
                                  icon: Icon(Icons.close),
                                  color: aquamarine,
                                  onPressed: () {
                                    setState(() {
                                      _textEditingController.clear();
                                      searchVisible = false;
                                      yTranslate = 0.0;
                                      opacity = 1.0;
                                      animatedWidth = 0.0;
                                    });
                                    FocusScope.of(context).requestFocus(FocusNode());
                                  },
                                )
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: grey2,
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(height: 16),
                              Expanded(
                                flex: 5,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(5)),
                                  child: url != null ? Image(image:
                                  NetworkToFileImage(
                                    url: url[1],
                                    file: File(url[0]),
                                    debug: true)
                                  ) : Loading(color: grey2),
                                ),
                              ),
                              Spacer(),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 32.0),
                                  child: Text(
                                    widget.shade.description,
                                    style: grey3BoldTextStyle.copyWith(fontSize: 18),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: SingleChildScrollView(
                                  child: Text(
                                    widget.shade.guidelines,
                                    style: grey3BoldTextStyle.copyWith(fontSize: 14),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
//                              Spacer(),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Text(
                                      "Polls",
                                      style: whiteBoldTextStyle.copyWith(fontSize: 18),
                                    ),
                                    Spacer(),
                                    Text(
                                      widget.shade.pollList.length.toString(),
                                      style: aquamarineBoldTextStyle.copyWith(fontSize: 18),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                                      child: Text(
                                        "Shadows",
                                        style: whiteBoldTextStyle.copyWith(fontSize: 18),
                                      ),
                                    ),
                                    Spacer(),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                                      child: Text(
                                        widget.shade.numShadows.toString(),
                                        style: aquamarineBoldTextStyle.copyWith(fontSize: 18),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: InkWell(
                    onTap: () async {
                      if(joined == 'Join Shade') {
                        if(widget.shade.access == 'public') {
                          await FirestoreService().addUsersJoinedShades(
                              widget.shadowUID, widget.shade.name,
                              widget.shade.uid);
                          await FirestoreService().addShadowToShade(
                              widget.shadowUID, widget.shade.name,
                              widget.shade.uid);
                        }
                        else
                          await FirestoreService().addUserToPending(
                              widget.shadowUID, widget.shade.name,
                              widget.shade.uid);
                      }
                      else if(joined == 'Pending') {
                        await FirestoreService().removeUsersPending(widget.shadowUID, widget.shade.name, widget.shade.uid);
                      }
                      else {
                        await FirestoreService().removeUsersJoinedShades(widget.shadowUID, widget.shade.name, widget.shade.uid);
                        await FirestoreService().removeShadowFromShade(widget.shadowUID, widget.shade.name, widget.shade.uid);
                      }
                      setState(() {
                        if(joined == 'Join Shade' && widget.shade.access == 'private') joined = 'Pending';
                        else if(joined == 'Join Shade' && widget.shade.access == 'public') joined = 'Leave Shade';
                        else joined = 'Join Shade';
                        refreshShadePolls();
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: aquamarine,
                          borderRadius: BorderRadius.all(Radius.circular(3))
                      ),
                      width: double.infinity,
                      height: 25,
                      child: Center(child: Text(joined, style: grey1BoldTextStyle.copyWith(fontSize: 16))),
                    ),
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal:8.0),
                            child: DropdownButton(
                              dropdownColor: grey2,
                              value: dropdownValue,
                              underline: SizedBox(),
                              items: dropdownList.map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(
                                    dropDownStringItem,
                                    style: grey5BoldTextStyle.copyWith(fontSize: 12),
                                  ),
                                );
                              }).toList(),
                              onChanged: (String value) {
                                setState(() {
                                  dropdownValue = value;
                                  refreshShadePolls();
                                });
                              },
                            ),
                          ),
                        ),
                        Spacer(),
                        Expanded(
                          flex: 0,
                          child: IconButton(
                            icon: Icon(Icons.arrow_left, color: aquamarine),
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onPressed: () {
                              if(pageController.page.toInt() > 0) {
                                pageController.previousPage(duration: Duration(milliseconds: 200), curve: Curves.easeInCubic);
                              }
                            },
                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              height: 20,
                              width: 52,
                              child: Center(
                                child: PageView(
                                  controller: pageController,
                                  children: [
                                    Text('10 mins', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                    Text('1 hour', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                    Text('24 hours', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                    Text('1 week', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                    Text('1 month', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                    Text('6 months', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                    Text('1 year', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                  ],
                                  scrollDirection: Axis.horizontal,
                                  onPageChanged: (index) {
                                    switch(index) {
                                      case 0: {
                                        setState(() {
                                          timeAgo = (DateTime.now()).subtract(Duration(minutes: 10)).toUtc().millisecondsSinceEpoch;
                                        });
                                        break;
                                      }
                                      case 1: {
                                        setState(() {
                                          timeAgo = (DateTime.now()).subtract(Duration(minutes: 60)).toUtc().millisecondsSinceEpoch;
                                        });
                                        break;
                                      }
                                      case 2: {
                                        setState(() {
                                          timeAgo = (DateTime.now()).subtract(Duration(days: 1)).toUtc().millisecondsSinceEpoch;
                                        });
                                        break;
                                      }
                                      case 3: {
                                        setState(() {
                                          timeAgo = (DateTime.now()).subtract(Duration(days: 7)).toUtc().millisecondsSinceEpoch;
                                        });
                                        break;
                                      }
                                      case 4: {
                                        setState(() {
                                          timeAgo = (DateTime.now()).subtract(Duration(days: 31)).toUtc().millisecondsSinceEpoch;
                                        });
                                        break;
                                      }
                                      case 5: {
                                        setState(() {
                                          timeAgo = (DateTime.now()).subtract(Duration(days: 126)).toUtc().millisecondsSinceEpoch;
                                        });
                                        break;
                                      }
                                      case 6: {
                                        setState(() {
                                          timeAgo = (DateTime.now()).subtract(Duration(days: 365)).toUtc().millisecondsSinceEpoch;
                                        });
                                        break;
                                      }
                                    }
                                    setState(() {
                                      refreshShadePolls();
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: IconButton(
                            icon: Icon(Icons.arrow_right, color: aquamarine),
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onPressed: () {
                              if(pageController.page.toInt() < 5) {
                                pageController.nextPage(duration: Duration(milliseconds: 200), curve: Curves.easeInCubic);
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ),
                Expanded(
                  flex: 4,
                  child: ListView.builder(
                    itemCount: shadePolls.length,
                    itemBuilder: (context, index) {
                      if(joined == 'Leave Shade')
                        return Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: PollCardDead(
                            poll: shadePolls[index],
                            shadowUID: widget.shadowUID,
                          ),
                        );
                      else
                        return Text(
                        "Join shade to view polls!",
                        textAlign: TextAlign.center,
                        style: grey4BoldTextStyle.copyWith(fontSize: 12),
                      );
                    },
                  )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

