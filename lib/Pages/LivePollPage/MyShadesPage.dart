import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Pages/HomePage/Discover.dart';
import 'package:shadow/Pages/ShadeAdminPage.dart';
import 'package:shadow/Services/LivePollService.dart';

class MyShadesPage extends StatefulWidget {

  final List<Shade> shades;
  final Function updateShades;
  final FirebaseShadow firebaseShadow;
  final List pollsVoted;

  const MyShadesPage({Key key, this.firebaseShadow, this.pollsVoted, this.shades, this.updateShades}) : super(key: key);

  @override
  _MyShadesPageState createState() => _MyShadesPageState();
}

class _MyShadesPageState extends State<MyShadesPage> {
  List<Shade> createdShades = [];

  void initState() {
    setState(() {
      createdShades = widget.shades;
    });
    super.initState();
    LivePollService().getShades(widget.pollsVoted).then((value) {
      setState(() {
        createdShades = value;
        widget.updateShades(value);
      });
    });
  }

  @override
  void didUpdateWidget(MyShadesPage oldWidget) {
    if(widget.pollsVoted != createdShades) {
      LivePollService().getShades(widget.pollsVoted).then((value) {
        setState(() {
          createdShades = value;
        });
      });
    }
    super.didUpdateWidget(oldWidget);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: grey1,
      child: createdShades.length != 0 ? ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
          scrollDirection: Axis.horizontal,
          itemCount: createdShades.length,
          itemBuilder: (BuildContext context, int i){
            return Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 16, 8, 0),
              child: VerticalShadeCard(shade: createdShades[i], widget: ShadeAdminPage(shade: createdShades[i], firebaseShadow: widget.firebaseShadow))
            );
          }
      ) : Padding(
        padding: const EdgeInsets.all(32.0),
        child: Container(
          child: Text(
            "No created shades, create a shade to get started.",
            textAlign: TextAlign.center,
            style: grey4BoldTextStyle.copyWith(fontSize: 12),
          ),
        ),
      )
    );
  }
}
