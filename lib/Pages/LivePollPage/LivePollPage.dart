import 'dart:ffi';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Constants/Widgets/Loading.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Pages/LivePollPage/CreatedPollsPage.dart';
import 'package:shadow/Pages/LivePollPage/MyShadesPage.dart';
import 'package:shadow/Services/LivePollService.dart';
import 'package:shadow/SharedWidgets/GraphWidgets/MiniGraph.dart';
import 'package:shadow/SharedWidgets/HomePageElements.dart';
import 'package:shadow/main.dart';

import 'DeadPollsPage.dart';
import 'VotedPollsPage.dart';

class LivePollPage extends StatefulWidget {
  @override
  _LivePollPageState createState() => _LivePollPageState();
}

class _LivePollPageState extends State<LivePollPage> with SingleTickerProviderStateMixin {
  double opacity = 1.0;

  TextEditingController _textEditingController;
  TabController _tabController;
  List<Poll> livePolls = [];
  List<Poll> myPolls = [];
  List<Shade> myShades = [];

  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
    _tabController = TabController(length: 3, vsync: this);
  }

  void dispose() {
    super.dispose();
    _textEditingController.dispose();
    _tabController.dispose();
  }

  void _updateLivePolls(List polls) {
    setState((){
      livePolls = polls;
    });
  }

  void _updateMyPolls(List polls) {
    setState((){
      myPolls = polls;
    });
  }

  void _updateMyShades(List shades) {
    setState((){
      myShades = shades;
    });
  }

  @override
  Widget build(BuildContext context) {
    final firebaseShadow = Provider.of<FirebaseShadow>(context);

    if (firebaseShadow != null) return SafeArea(
      child: Scaffold(
        backgroundColor: grey1,
        appBar: AppBar(
          backgroundColor: grey1,
          automaticallyImplyLeading: false,
          title: Padding(
            padding: EdgeInsets.all(5.0),
            child: Container(
              decoration: BoxDecoration(
                color: grey2,
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              child: Row(
                children: [
                  Expanded(flex: 1,
                      child: IconButton(
                          icon: Icon(Icons.search),
                          color: grey4,
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onPressed: () {

                          }
                      )
                  ),
                  Expanded(
                    flex: 5,
                    child: TextFormField(
                      controller: _textEditingController,
//                    onTap: () {
//                      setState(() {
//                        yTranslate = MediaQuery.of(context).size.height - 250;
//                        opacity = 0.0;
//                        animatedWidth = MediaQuery.of(context).size.width / 5;
//                        searchVisible = true;
//                      });
//                    },
//                    onChanged: (value) {
//                      value == '' ? currentIndex = -1 : results = [];
//                      results = [];
//                      _updateResults(value);
//                    },
                      style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                      decoration: InputDecoration(
                        hintText: 'Search',
                        hintStyle: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: AnimatedOpacity(
                        duration: Duration(milliseconds: 250),
                        opacity: 1.0 - opacity,
                        child: IconButton(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          icon: Icon(Icons.close),
                          color: aquamarine,
                          onPressed: () => print("pressed1"),
                        )
                    ),
                  )
                ],
              ),
            ),
          ),
          bottom: TabBar(
            tabs: <Tab>[
              Tab(text: "Live Polls"),
              Tab(text: "My Polls"),
              Tab(text: "My Shades"),
            ],
            indicatorColor: aquamarine,
            labelColor: aquamarine,
            labelStyle: aquamarineBoldTextStyle,
            unselectedLabelColor: white,
            unselectedLabelStyle: whiteBoldTextStyle,
            controller: _tabController,
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            StreamBuilder(
              stream: LivePollService(uid: firebaseShadow.uid).livePollPage,
              builder: (context, snapshot) {
                if(snapshot.hasData) return VotedPollsPage(polls: livePolls, updatePolls: _updateLivePolls, pollsVoted: snapshot.data[0], firebaseShadow: firebaseShadow);
                else return Loading();
              }
            ),
            StreamBuilder(
              stream: LivePollService(uid: firebaseShadow.uid).livePollPage,
              builder: (context, snapshot) {
                if(snapshot.hasData) return CreatedPollsPage(polls: myPolls, updatePolls: _updateMyPolls, pollsVoted: snapshot.data[1], firebaseShadow: firebaseShadow);
                else return Loading();
              }
            ),
            StreamBuilder(
              stream: LivePollService(uid: firebaseShadow.uid).livePollPage,
              builder: (context, snapshot) {
                if(snapshot.hasData) return MyShadesPage(shades: myShades, updateShades: _updateMyShades, pollsVoted: snapshot.data[2], firebaseShadow: firebaseShadow);
                else return Loading();
              }
            ),
          ],
          controller: _tabController,
        ),
      ),
    );
    else
      return Loading();
  }
}

