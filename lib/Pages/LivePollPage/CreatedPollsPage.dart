import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/PollCards/PollCardAlive.dart';
import 'package:shadow/PollCards/PollCardAliveTemp.dart';
import 'package:shadow/Services/LivePollService.dart';
import 'package:shadow/SharedWidgets/HomePageElements.dart';

class CreatedPollsPage extends StatefulWidget {
  final List<Poll> polls;
  final Function updatePolls;
  final FirebaseShadow firebaseShadow;
  final List pollsVoted;

  const CreatedPollsPage({Key key, this.firebaseShadow, this.pollsVoted, this.polls, this.updatePolls}) : super(key: key);

  @override
  _CreatedPollsPageState createState() => _CreatedPollsPageState();
}

class _CreatedPollsPageState extends State<CreatedPollsPage> {
  List<Poll> createdPolls = [];

  void initState() {
    setState((){
      createdPolls = widget.polls;
    });
    super.initState();
    LivePollService().getPolls(widget.pollsVoted).then((value) {
      setState(() {
        createdPolls = value;
        widget.updatePolls(value);
      });
    });
  }

  @override
  void didUpdateWidget(CreatedPollsPage oldWidget) {
    if(widget.pollsVoted != createdPolls) {
      LivePollService().getPolls(widget.pollsVoted).then((value) {
        setState(() {
          createdPolls = value;
        });
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: grey1,
        child: createdPolls.length != 0  && createdPolls != null ? ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
            itemCount: createdPolls.length,
            itemBuilder: (BuildContext context, int i){
              return Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 16, 8, 0),
                child: PollCardAlive(poll: createdPolls[i], shadowUID: widget.firebaseShadow.uid),
              );
            }
        ) : Padding(
          padding: const EdgeInsets.all(32.0),
          child: Container(
            child: Text(
              "No created polls, make a poll to get started.",
              textAlign: TextAlign.center,
              style: grey4BoldTextStyle.copyWith(fontSize: 12),
            ),
          ),
        )
    );
  }
}
