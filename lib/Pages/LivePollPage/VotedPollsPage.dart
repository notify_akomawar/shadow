import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/PollCards/PollCardAlive.dart';
import 'package:shadow/PollCards/PollCardAliveTemp.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/Services/LivePollService.dart';
import 'package:shadow/Services/SearchService.dart';

class VotedPollsPage extends StatefulWidget {

  final List<Poll> polls;
  final Function updatePolls;
  final FirebaseShadow firebaseShadow;
  final List pollsVoted;

  const VotedPollsPage({Key key, this.firebaseShadow, this.pollsVoted, this.polls, this.updatePolls}) : super(key: key);

  @override
  _VotedPollsPageState createState() => _VotedPollsPageState();
}

class _VotedPollsPageState extends State<VotedPollsPage> {
  List<Poll> livePolls = [];
  
  void initState() {
    setState((){
      livePolls = widget.polls;
    });
    super.initState();
    LivePollService().getPolls(widget.pollsVoted).then((value) {
      setState(() {
        livePolls = value;
        widget.updatePolls(value);
      });
    });
  }

  @override
  void didUpdateWidget(VotedPollsPage oldWidget) {
    if(widget.pollsVoted != livePolls) {
      LivePollService().getPolls(widget.pollsVoted).then((value) {
        setState(() {
          livePolls = value;
        });
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: grey1,
      child: livePolls.length != 0  && livePolls != null ? ListView.builder(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
        itemCount: livePolls.length,
        itemBuilder: (BuildContext context, int i){
          return Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 16, 8, 0),
            child: PollCardAlive(poll: livePolls[i], shadowUID: widget.firebaseShadow.uid),
          );
        }
      ) : Padding(
        padding: const EdgeInsets.all(32.0),
        child: Container(
          child: Text(
            "No current live polls, vote on a poll to get started.",
            textAlign: TextAlign.center,
            style: grey4BoldTextStyle.copyWith(fontSize: 12),
          ),
        ),
      )
    );
  }
}