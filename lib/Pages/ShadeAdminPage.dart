import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:network_to_file_image/network_to_file_image.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/PollCards/PollCardAdmin.dart';
import 'package:shadow/Services/CloudStorageService.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/Services/SearchService.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class ShadeAdminPage extends StatefulWidget {
  final Shade shade;
  final FirebaseShadow firebaseShadow;

  ShadeAdminPage({this.shade, this.firebaseShadow});

  @override
  _ShadeAdminPageState createState() => _ShadeAdminPageState();
}

class _ShadeAdminPageState extends State<ShadeAdminPage> {
  List url;
  var image;
  List<Poll> shadePolls = [];

  _openEditPage() {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => EditShadeProfilePage(
          currentShade: widget.shade,
          currentDescription: widget.shade.description,
          currentGuidelines: widget.shade.guidelines
      )),
    );
  }

  _openMembersPage() {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => ShadeMembersPage(
        currentShade: widget.shade,
//          currentShade: widget.shade,
//          currentDescription: widget.shade.description,
//          currentGuidelines: widget.shade.guidelines)
      )),
    );
  }

  @override
  void initState() {
    super.initState();
    CloudStorageService().getURL(widget.shade.imagePath).then((value) {
      setState(() {
        url = value;
      });
    });

    CloudStorageService().getImage(widget.shade.imagePath).then((value) {
      setState(() {
        image = value;
      });
    });

    var timeAgo = (DateTime.now()).subtract(Duration(days: 365)).toUtc().millisecondsSinceEpoch;
    SearchService().getPollsByRecent(timeAgo, widget.shade.uid, true).then((result) {
      setState(() {
        shadePolls = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
//                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      flex: 0,
                      child: InkWell(
                        child: Icon(Icons.arrow_back, color: white),
                        onTap: () => Navigator.of(context).pop(context),
                      ),
                    ),
                    Flexible(
                      child: Text('shade[${widget.shade.name}]', textAlign: TextAlign.right, style: whiteBoldTextStyle.copyWith(fontSize: 24)),
                    )
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Access', style: aquamarineBoldTextStyle.copyWith(fontSize: 16)),
                        Text(widget.shade.access, style: grey5NormalTextStyle.copyWith(fontSize: 14)),
//                        Text('Shade Description', style: aquamarineBoldTextStyle.copyWith(fontSize: 16)),
//                        Text(widget.shade.description, style: grey5NormalTextStyle.copyWith(fontSize: 14)),
                        SizedBox(height: 16),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () => {
                                print("tapped 1"),
                                _openEditPage(),
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: grey2,
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(4.0),
                                  child: Icon(
                                      Icons.edit,
                                      color: grey4
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 8.0),
                            InkWell(
                              onTap: () => {
                                print("tapped 2"),
                                _openMembersPage(),
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: grey2,
                                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(4.0),
                                  child: Icon(
                                    Icons.person,
                                    color: grey4
                                  ),
                                ),
                              ),
                            ),
                          ]
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.width/3,
                    width: MediaQuery.of(context).size.width/3,
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        child: url != null ? Image(image:
                        NetworkToFileImage(
                          url: url[1],
                          file: File(url[0]),
                          debug: true)
                        ) : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(aquamarine)),
                        ),
                    ),
                  ),
                ],
              ),
              Expanded(
                flex: 9,
                child: ListView.builder(
                  itemCount: shadePolls != null ? shadePolls.length : 1,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: PollCardAdmin(
                        poll: shadePolls[index],
                        shadowUID: widget.firebaseShadow.uid,
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class EditShadeProfilePage extends StatefulWidget {

  final String currentDescription;
  final String currentGuidelines;
  final Shade currentShade;

  const EditShadeProfilePage({Key key, this.currentDescription, this.currentGuidelines, this.currentShade}) : super(key: key);

  @override
  _EditShadeProfilePageState createState() => _EditShadeProfilePageState();
}

class _EditShadeProfilePageState extends State<EditShadeProfilePage> {

  String description = "";
  String guidelines = "";

//  _updateDescription(String description) {
//    this.description = description;
//  }
//
//  _updateGuidelines(String guidelines) {
//    this.guidelines = guidelines;
//  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(left: 32.0),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(padding: EdgeInsets.only(right: 8.0), child: InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      child: Padding(padding: EdgeInsets.only(top: 8.0, right: 8.0, bottom: 8.0), child: Icon(Icons.arrow_back, color: white)))
                  ),
                  Text("Edit Shade", style: whiteBoldTextStyle.copyWith(fontSize: 24)),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 8.0), child: Text("Change Description", style: grey5BoldTextStyle.copyWith(fontSize: 12))),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                child: Container(
                  height: 140,
                  width: MediaQuery.of(context).size.width - 64,
                  decoration: BoxDecoration(
                      color: grey2,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                    child: TextFormField(
                      initialValue: widget.currentDescription,
                      keyboardType: TextInputType.multiline,
                      maxLines: 6,
                      maxLength: 250,
                      style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                      decoration: InputDecoration(
                          hintText: "Enter New Description Here",
                          hintStyle: TextStyle(color: grey3, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          counterStyle: TextStyle(color: aquamarine)
                      ),
                      validator: (value) => value.isEmpty ? '' : null,
                      onChanged: (value) {
                        setState(() {
                          description = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 8.0), child: Text("Change Guidelines", style: grey5BoldTextStyle.copyWith(fontSize: 12))),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                child: Container(
                  height: 140,
                  width: MediaQuery.of(context).size.width - 64,
                  decoration: BoxDecoration(
                      color: grey2,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                    child: TextFormField(
                      initialValue: widget.currentGuidelines,
                      keyboardType: TextInputType.multiline,
                      maxLines: 6,
                      maxLength: 250,
                      style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                      decoration: InputDecoration(
                          hintText: "Enter New Guidelines Here",
                          hintStyle: TextStyle(color: grey3, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          counterStyle: TextStyle(color: aquamarine)
                      ),
                      validator: (value) => value.isEmpty ? '' : null,
                      onChanged: (value) {
                        setState(() {
                          guidelines = value;
                        });
                      },
                    ),
                  ),
                ),
              ),
              InkWell(
                  onTap: () async {
                    setState((){
                      widget.currentShade.description = description == "" ? widget.currentShade.description : description;
                      widget.currentShade.guidelines = guidelines == "" ? widget.currentShade.guidelines : guidelines;
                    });
                    await FirestoreService().editShadeInfo(widget.currentShade.uid, widget.currentShade.description, widget.currentShade.guidelines);
                    Navigator.of(context).pop();
                  },
                  child: Container(height: 32, decoration: BoxDecoration(color: aquamarine, borderRadius: BorderRadius.all(Radius.circular(10.0))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Confirm", style: grey1BoldTextStyle.copyWith(fontSize: 12)),
                      ))
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ShadeMembersPage extends StatefulWidget {

  final Shade currentShade;

  const  ShadeMembersPage({Key key, this.currentShade}) : super(key: key);

  @override
  _ShadeMembersPageState createState() => _ShadeMembersPageState();
}

class _ShadeMembersPageState extends State<ShadeMembersPage> with SingleTickerProviderStateMixin{

  ScrollController _scrollViewController;
  TabController _tabController;
  TextEditingController _textEditingController;

  bool searchVisible = false;
  double opacity = 1.0;
  double yTranslate = 0.0;
  double animatedWidth = 0.0;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
    _scrollViewController = ScrollController();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("yay");
//    return SafeArea(child: CurrentMembersPage());
    return SafeArea(
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.transparent,
            body: Column(
              children: [
                Expanded(
                  flex: 0,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () => () {
                          Navigator.of(context).pop();
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.arrow_back, color: white),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text("Members", style: grey5BoldTextStyle.copyWith(fontSize: 24)),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: grey2,
                          borderRadius: BorderRadius.all(Radius.circular(5))
                      ),
//                      child: Row(
//                        children: [
//                          Expanded(flex: 1,
//                              child: IconButton(
//                                  icon: Icon(Icons.search),
//                                  color: grey4,
//                                  splashColor: Colors.transparent,
//                                  highlightColor: Colors.transparent,
//                                  onPressed: () {
//                                    setState(() {
//                                      yTranslate = MediaQuery.of(context).size.height - 250;
//                                      opacity = 0.0;
//                                      animatedWidth = MediaQuery.of(context).size.width / 5;
//                                      searchVisible = true;
//                                    });
//                                  }
//                              )
//                          ),
//                          Expanded(
//                            flex: 5,
//                            child: TextFormField(
//                              controller: _textEditingController,
//                              onTap: () {
//                                setState(() {
//                                  yTranslate = MediaQuery.of(context).size.height - 250;
//                                  opacity = 0.0;
//                                  animatedWidth = MediaQuery.of(context).size.width / 5;
//                                  searchVisible = true;
//                                });
//                              },
//                              onChanged: (value) {
////                                value == '' ? currentIndex = -1 : results = [];
////                                results = [];
////                                _updateResults(value);
//                                print(value);
//                              },
//                              style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
//                              decoration: InputDecoration(
//                                hintText: 'Search',
//                                hintStyle: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
//                                border: InputBorder.none,
//                                focusedBorder: InputBorder.none,
//                                enabledBorder: InputBorder.none,
//                                errorBorder: InputBorder.none,
//                                disabledBorder: InputBorder.none,
//                              ),
//                            ),
//                          ),
//                          Expanded(
//                            flex: 1,
//                            child: AnimatedOpacity(
//                                duration: Duration(milliseconds: 250),
//                                opacity: 1.0 - opacity,
//                                child: IconButton(
//                                  splashColor: Colors.transparent,
//                                  highlightColor: Colors.transparent,
//                                  icon: Icon(Icons.close),
//                                  color: aquamarine,
//                                  onPressed: () {
//                                    setState(() {
//                                      _textEditingController.clear();
//                                      searchVisible = false;
//                                      yTranslate = 0.0;
//                                      opacity = 1.0;
//                                      animatedWidth = 0.0;
//                                    });
//                                    FocusScope.of(context).requestFocus(FocusNode());
//                                  },
//                                )
//                            ),
//                          )
//                        ],
//                      ),
                    ),
                  ),
                ),
//                Visibility(
//                  visible: searchVisible,
//                  child: Expanded(
//                    flex: 1,
//                    child: Padding(
//                      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
//                      child: ListView.builder(itemBuilder: (BuildContext context, int index){
//                        if(index >= currentResults) {
//                          currentResults = currentResults + 10 >= results.length
//                              ? results.length
//                              : currentResults + 10;
//                          Future.delayed(Duration(seconds: 1));
//                        }
//                        if(index == results.length && results.length == 0) {
//                          return Text("No members match your search", style: whiteNormalTextStyle,);
//                        }
//                        if(index >= results.length) {
//                          return null;
//                        }
//                        return Padding(
//                          padding: const EdgeInsets.symmetric(vertical: 4.0),
//                          child: ShadeCard(
//                            shade: results[index][0],
//                            shadowUID: firebaseShadow.uid,
//                            image: results[index][1],
//                            index: index,
//                            onTap: _openShadeProfilePage, // _openShadeProfilePage // change to open shade page ------------------------
//                            isSelected: currentIndex == index ? true : false,
//                          ),
//                        );
//                      }),
//                    ),
//                  ),
//                ),
                Visibility(
                  visible: !searchVisible,
                  child: Container(
                    height: MediaQuery.of(context).size.height - 208,
                    child: AnimatedOpacity(
                      duration: Duration(milliseconds: 500),
                      opacity: opacity,
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        transform: Matrix4.identity()
                          ..translate(0.0, yTranslate),
                        child: Column(
                          children: [
                            Expanded(
                              flex: 0,
                              child: SizedBox(
                                height: 50,
                                child: TabBar(
                                    tabs: <Tab>[
                                      Tab(text: "Members"),
                                      Tab(text: "Pending"),
                                    ],
                                    indicatorColor: aquamarine,
                                    labelColor: aquamarine,
                                    labelStyle: aquamarineBoldTextStyle,
                                    unselectedLabelColor: white,
                                    unselectedLabelStyle: whiteBoldTextStyle,
                                    controller: _tabController,
                                ),
                              ),
                            ),
//                            Spacer(),
                            Container(
                              height: MediaQuery.of(context).size.height - 264,
                              color: grey1,
                              child: TabBarView(
                                physics: NeverScrollableScrollPhysics(),
                                children: <Widget>[
                                  CurrentMembersPage(),
                                  PendingMembersPage(),
                                ],
                                controller: _tabController,
                              ),
                            ),
                          ]
                        ),
//                        child: NestedScrollView(
//                          controller: _scrollViewController,
//                          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
//                            return <Widget>[
//                              AppBar(
//                                backgroundColor: grey1,
//                                automaticallyImplyLeading: true,
//                                leading: Padding(
//                                  padding: EdgeInsets.all(8.0),
//                                  child: Icon(Icons.arrow_back, color: white),
//                                ),
//                                title: TabBar(
//                                  tabs: <Tab>[
//                                    Tab(text: "Members"),
//                                    Tab(text: "Pending"),
//                                  ],
//                                  indicatorColor: aquamarine,
//                                  labelColor: aquamarine,
//                                  labelStyle: aquamarineBoldTextStyle,
//                                  unselectedLabelColor: white,
//                                  unselectedLabelStyle: whiteBoldTextStyle,
//                                  controller: _tabController,
//                                ),
//                              ),
//                            ];
//                          },
//                          body: TabBarView(
//                            physics: NeverScrollableScrollPhysics(),
//                            children: <Widget>[
//                              CurrentMembersPage(),
//                              PendingMembersPage(),
//                            ],
//                            controller: _tabController,
//                          ),
//                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
//          if(previewPoll != null) Visibility(
//              visible: isPreviewPoll,
//              child: PollPreview(previewPoll: previewPoll, closePreview: closePreview)
//          ),
//          Visibility(
//            visible: isMenuVisible,
//            child: PollMenu(closeMenu: closeMenu),
//          )
        ],
      ),
    );
  }
}

class CurrentMembersPage extends StatelessWidget {

  final Shade currentShade;

  const CurrentMembersPage({Key key, this.currentShade}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: grey1,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        child: ListView.builder(

          itemBuilder: (BuildContext context, int i){
            return Text("yay", style: whiteBoldTextStyle);
//          return MemberCard(currentShade.shadowList, i);
          },
        ),
      )
    );
  }
}


class PendingMembersPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

