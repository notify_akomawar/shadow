import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Constants/Widgets/Loading.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';
import 'package:shadow/Pages/HomePage/Discover.dart';
import 'package:shadow/Pages/ProfilePages/ShadeProfilePage.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/Services/LivePollService.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  List<Shade> joinedShades = [];

  _openEditPage(ShadowProfile shadowProfile, String currentDescription,) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => EditProfilePage(shadowProfile: shadowProfile, currentDescription: currentDescription)),
    );
  }

  @override
  Widget build(BuildContext context) {
    final firebaseShadow = Provider.of<FirebaseShadow>(context);

    if(firebaseShadow != null) return StreamBuilder<ShadowProfile>(
      stream: FirestoreService(shadowID: firebaseShadow.uid).shadowProfile,
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          ShadowProfile shadowProfile = snapshot.data;
          Future getJoinedShades() async {
            List<Shade> temp = [];
            temp = await LivePollService().getShades(shadowProfile.joinedShades);
            setState(() {
              joinedShades = temp;
            });
          }

          getJoinedShades();


          return SafeArea(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                child: Column(
                  children: [
                    Flexible(
                      child: Row(
                        children: [
                          Text('shadow[${shadowProfile.username}]', style: whiteBoldTextStyle.copyWith(fontSize: 24)),
                          InkWell(
                            onTap: () => _openEditPage(shadowProfile, shadowProfile.description),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Icon(Icons.edit, color: aquamarine, ),
                            )
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 48),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 3,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Align(alignment: Alignment.centerLeft, child: Text('Description', style: aquamarineBoldTextStyle.copyWith(fontSize: 18))),
                              SizedBox(height: 8),
                              Text(shadowProfile.description ?? 'Who are you? Tell the community.', style: grey3BoldTextStyle.copyWith(fontSize: 14))
                            ],
                          ),
                        ),
                        SizedBox(width: 16.0),
                        Expanded(
                          flex: 0,
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              color: grey2,
                              borderRadius: BorderRadius.all(Radius.circular(10))
                            ),
                            child: Center(
                              child: Container(
                                height: 80,
                                width: 80,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: aquamarine
                                ),
                                child: Text(''),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 64),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('light', style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
                        Text(shadowProfile.light.toString(), style: whiteBoldTextStyle.copyWith(fontSize: 24))
                      ],
                    ),
                    SizedBox(height: 4),
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: [
//                        Text('shadowers', style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
//                        Text(shadowProfile.shadowers.length.toString(), style: whiteBoldTextStyle.copyWith(fontSize: 24))
//                      ],
//                    ),
                    SizedBox(height: 4),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('polls created', style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
                        Text(23.toString(), style: whiteBoldTextStyle.copyWith(fontSize: 24))
                      ],
                    ),
                    SizedBox(height: 4),
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: [
//                        Text('shadows reached', style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
//                        Text(34.2.toString() + 'k', style: whiteBoldTextStyle.copyWith(fontSize: 24))
//                      ],
//                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(alignment: Alignment.centerLeft, child: Text('Shades', style: aquamarineBoldTextStyle.copyWith(fontSize: 20))),
                    ),
                    SizedBox(height: 8.0),
                    SizedBox(
                      width: MediaQuery.of(context).size.width - 32,
                      height: MediaQuery.of(context).size.height / 3,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: false,
                        itemCount: joinedShades.length,
                        itemBuilder: (context, index) {
                          return joinedShades.length != 0 ? ProfilePageShadeCard(
                            shade: joinedShades[index],
                            widget: ShadeProfilePage(
                              shadowUID: firebaseShadow.uid,
                              shade: joinedShades[index],
                            )
                          ) : Text("Join a Shade to view them here", style: aquamarineBoldTextStyle.copyWith(fontSize: 12));
//                          return ShadowMiniCard(shadowUsername: shadowProfile.shadowers[index]['username'].toString(), light: shadowProfile.shadowers[index]['light']);
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        } else return Loading();
      }
    );
    else return Loading();
  }
}

class EditProfilePage extends StatefulWidget {

  final ShadowProfile shadowProfile;
  final String currentDescription;

  const EditProfilePage({Key key, this.shadowProfile, this.currentDescription}) : super(key: key);

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {

  String description = "";

  _updateDescription(String description) {
    setState(() {
      this.description = description;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(padding: EdgeInsets.only(right: 8.0), child: InkWell(
                      onTap: () => Navigator.of(context).pop(),
                        child: Padding(padding: EdgeInsets.all(8.0), child: Icon(Icons.arrow_back, color: white)))
                    ),
                    Text("Edit Profile", style: aquamarineBoldTextStyle.copyWith(fontSize: 18)),
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 8.0), child: Text("Change Description", style: grey5BoldTextStyle.copyWith(fontSize: 12))),
                Padding(padding: EdgeInsets.symmetric(vertical: 16.0), child: InputFormField(
                  hintText: widget.currentDescription,
                  updateString: _updateDescription,
                  input: widget.currentDescription == "" ? "Who are you? Tell the community." : widget.currentDescription,
                  maxLength: 100,
                )),
//                Spacer(),
                InkWell(
                  onTap: () async {
                    setState((){
                      widget.shadowProfile.description = description == "" ? "Who are you? Tell the community." : description;
                    });
                    await FirestoreService().editUserDescription(widget.shadowProfile.uid, widget.shadowProfile.description);
                    Navigator.of(context).pop();
                  },
                    child: Container(height: 32, decoration: BoxDecoration(color: aquamarine, borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Confirm", style: grey1BoldTextStyle.copyWith(fontSize: 12)),
                    ))
                ),
              ]
            ),
          ),
        ),
      ),
    );
  }
}


class ShadowMiniCard extends StatelessWidget {
  final String shadowUsername;
  final int light;

  ShadowMiniCard({this.shadowUsername, this.light});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      decoration: BoxDecoration(
        color: grey2,
        borderRadius: BorderRadius.all(Radius.circular((5)))
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(text: 'shadow[', style: whiteBoldTextStyle.copyWith(fontSize: 14)),
                  TextSpan(text: shadowUsername, style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
                  TextSpan(text: ']', style: whiteBoldTextStyle.copyWith(fontSize: 14))
                ]
              ),
            ),
            Spacer(),
            Text('light', style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
            SizedBox(width: 8.0),
            Text(light.toString(), style: whiteBoldTextStyle.copyWith(fontSize: 18)),
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 8.0, 0.0, 8.0),
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    color: aquamarine,
                    borderRadius: BorderRadius.all(Radius.circular(5))
                ),
                child: Icon(Icons.add, color: grey1),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class ProfilePageShadeCard extends StatelessWidget {
  final Shade shade;
  final Widget widget;
  ProfilePageShadeCard({this.shade, this.widget});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
//        Navigator.push(
//          context,
//          MaterialPageRoute(
//              builder: (context) => ShadeProfilePage(
//                shadowUID: shadowUID,
//                shade: shade,
//              )
//          ),
//        );
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => widget
          ),
        );
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
              color: grey2,
              borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          width: 150,
          height: 140,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(width: 64, height: 64, child: Placeholder()),
                Flexible(
                  child: Text(
                    shade.name.toUpperCase(),
                    style: aquamarineBoldTextStyle.copyWith(fontSize: 18),
                  ),
                ),
                SizedBox(height: 32),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'polls',
                          style: whiteTitleTextStyle.copyWith(fontSize: 12),
                        ),
                        Text(
                          'shadows',
                          style: whiteTitleTextStyle.copyWith(fontSize: 12),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          shade.numPolls.toString() ?? 0.toString(),
                          style: aquamarineBoldTextStyle.copyWith(fontSize: 16),
                        ),
                        Text(
                          shade.numShadows.toString(),
                          style: aquamarineBoldTextStyle.copyWith(fontSize: 16),
                        )
                      ],
                    )
                  ],
                ),
//                SizedBox(height: 16),
                RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(text: 'view top ', style: whiteBoldTextStyle.copyWith(fontSize: 8)),
                        TextSpan(text: 'polls', style: aquamarineBoldTextStyle.copyWith(fontSize: 8))
                      ]
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

