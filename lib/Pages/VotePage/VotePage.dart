import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';
import 'package:shadow/SharedWidgets/GraphWidgets/MiniGraph.dart';

import '../../SharedWidgets/FormElements.dart';

class VotePage extends StatefulWidget {

  final Poll poll;
  final int numOptions;
  final String shadowUID;
  final List<Poll> currentPolls;
  final int index;

  const VotePage({Key key, this.poll, this.numOptions, this.shadowUID, this.currentPolls, this.index}) : super(key: key);

  @override
  _VotePageState createState() => _VotePageState();
}

class _VotePageState extends State<VotePage> {

  bool topVisible = true;
  int currentIndex = 0;
  String justification = "Enter justification here (optional)";

  void initState() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          topVisible = visible ? false : true;
        });
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Container(
          width: MediaQuery.of(context).size.width - 16,
          color: grey1,
          child: Column(
            children: [
              Expanded(
                  flex: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 0,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 12.0),
                          child: InkWell(
                            onTap: (){Navigator.of(context).pop();},
                            child: Icon(
                              Icons.arrow_back,
                              color: white,
                              size: 32,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.poll.name,
                              textAlign: TextAlign.left,
                              style: whiteBoldTextStyle.copyWith(fontSize: 24),
                            ),
                            Visibility(
                              visible: topVisible,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0),
                                child: Text(
                                  widget.poll.description,
                                  textAlign: TextAlign.left,
                                  style: whiteNormalTextStyle.copyWith(fontSize: 14),
                                ),
                              ),
                            ),
                            Visibility(
                              visible: topVisible,
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Row(
                                  children: [
                                    Text(
                                      "votes",
                                      style: grey5BoldTextStyle.copyWith(fontSize: 16),
                                    ),
                                    Spacer(),
                                    Text(
                                      widget.poll.voteCount.toString(),
                                      style: aquamarineBoldTextStyle.copyWith(fontSize: 24),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 16),
                    ],
                  ),
              ),
              Visibility(
                visible: topVisible,
                child: SizedBox(
                  height: 32,
                ),
              ),
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Row(
                      children: [
                        SizedBox(width: 16),
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Visibility(
                                    visible: widget.numOptions >= 1,
                                    child: OptionSquare(
                                      color: ColorChooser.graphColors[int.parse(widget.poll.options[0]['color'])],
                                      isSelected: currentIndex == 0 ? true : false,
                                      updateSelected: _updateCurrent,
                                      index: 0,
                                    )
                                  ),
                                  Visibility(
                                      visible: widget.numOptions >= 2,
                                      child: OptionSquare(
                                        color: ColorChooser.graphColors[int.parse(widget.poll.options[1]['color'])],
                                        isSelected: currentIndex == 1 ? true : false,
                                        updateSelected: _updateCurrent,
                                        index: 1,
                                      )
                                  ),
                                  Visibility(
                                      visible: widget.numOptions >= 3,
                                      child: OptionSquare(
                                        color: ColorChooser.graphColors[int.parse(widget.poll.options[2]['color'])],
                                        isSelected: currentIndex == 2 ? true : false,
                                        updateSelected: _updateCurrent,
                                        index: 2,
                                      )
                                  ),
                                  Visibility(
                                      visible: widget.numOptions >= 4,
                                      child: OptionSquare(
                                        color: ColorChooser.graphColors[int.parse(widget.poll.options[3]['color'])],
                                        isSelected: currentIndex == 3 ? true : false,
                                        updateSelected: _updateCurrent,
                                        index: 3,
                                      )
                                  ),
                                  Visibility(
                                      visible: widget.numOptions >= 5,
                                      child: OptionSquare(
                                        color: ColorChooser.graphColors[int.parse(widget.poll.options[4]['color'])],
                                        isSelected: currentIndex == 4 ? true : false,
                                        updateSelected: _updateCurrent,
                                        index: 4,
                                      )
                                  ),
                              ],
                              ),
                              SizedBox(height: 16),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0),
                                child: SizedBox(
                                  height: 50,
                                  child: Text(
                                    widget.poll.options[currentIndex]['option'].toString(),
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: ColorChooser.graphColors[int.parse(widget.poll.options[currentIndex]['color'])],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: SizedBox(
                                  height: 80,
                                  child: Text(
                                    widget.poll.options[currentIndex]['description'].toString() == 'Enter Description (Optional)' ? '' : widget.poll.options[currentIndex]['description'].toString(),
                                    textAlign: TextAlign.left,
                                    style: grey4NormalTextStyle.copyWith(fontSize: 18),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: !widget.poll.shadowsVoted.containsKey('${widget.shadowUID}') && widget.poll.voteCount != widget.poll.voteCountLimit,
                          child: Expanded(
                            flex: 0,
                            child: InkWell(
                              onTap: () async {
                                await FirestoreService().updateVote(
                                  Firestore.instance.collection('shades').document(widget.poll.shadeUID).collection('polls').document(widget.poll.uid),
                                  widget.shadowUID,
                                  currentIndex,
                                  1,
                                  widget.poll.options[currentIndex]['option'],
                                  justification,
                                );
                                await FirestoreService().addToUserVoted(widget.shadowUID, widget.poll.shadeUID, widget.poll.uid);
                                setState(() {
                                  widget.currentPolls.removeAt(widget.index);
                                  widget.poll.options[currentIndex]['votes'] += 1;
                                  widget.poll.voteCount += 1;
                                  widget.poll.shadowsVoted['${widget.shadowUID}'] = [widget.poll.options[currentIndex]['option'], justification];
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                ),
                                height: MediaQuery.of(context).size.width / 5,
                                child: Image(
                                  image: AssetImage('lib/Assets/Icons/illustration/' + (int.parse(widget.poll.options[currentIndex]['color']) + 1).toString().trim() + '.png'),
                                  colorBlendMode: BlendMode.multiply,
                                  fit: BoxFit.cover,
                                ),
                              )
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
              ),
              if(widget.poll.shadowsVoted.containsKey('${widget.shadowUID}')) Expanded(
                flex: 1,
                child: MiniGraph(
                  width: MediaQuery.of(context).size.width * 0.75,
                  height: (MediaQuery.of(context).size.width * 0.75) - 50,
                  optionsList: widget.poll.options,
                ),
              ),
             if(!widget.poll.shadowsVoted.containsKey('${widget.shadowUID}')) Expanded(
               flex: 1,
               child: Padding(
                 padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                 child: Container(
                   decoration: BoxDecoration(
                     color: grey2,
                     borderRadius: BorderRadius.all(Radius.circular(10.0)),
                   ),
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: TextFormField(
                       style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                       decoration: InputDecoration(
                         hintText: "Enter justification here (optional)",
                         hintStyle: TextStyle(color: grey3, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                         border: InputBorder.none,
                         focusedBorder: InputBorder.none,
                         enabledBorder: InputBorder.none,
                         errorBorder: InputBorder.none,
                         disabledBorder: InputBorder.none,
                         counterText: '',
                       ),
                       maxLength: 150,
                       maxLines: 6,
                       onChanged: (value) {
                         setState(() {
                           justification = value;
                         });
                       },
                     ),
                   ),
                 ),
               ),
             ),
            ],
          ),
        ),
      ),
    );
  }

  _updateCurrent(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}

///////////////////////////// VOTE PAGE WIDGETS /////////////////////////////
class OptionSquare extends StatelessWidget {

  final Color color;
  final bool isSelected;
  final Function updateSelected;
  final int index;

  const OptionSquare({Key key, this.color, this.isSelected, this.updateSelected, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 16.0),
      child: InkWell(
        onTap: () => updateSelected(index),
        child: Center(
          widthFactor: 1.0,
          heightFactor: 1.0,
          child: Container(
              height: 32,
              width: 32,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                color: isSelected ? white : grey1,
              ),
              child: Center(
                child: Container(
                    width: 28,
                    height: 28,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      color: color,
                    )
                ),
              )
          ),
        ),
      ),
    );
  }
}

