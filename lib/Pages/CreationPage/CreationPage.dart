import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/FirebaseWidgets/StorageUploader.dart';
import 'package:shadow/Models/OptionListModel.dart';
import 'package:shadow/Models/OptionModel.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Pages/CreationPage/PollCreationWidgets/PollConfirmationPage.dart';
import 'package:shadow/Pages/CreationPage/PollCreationWidgets/PollNamePage.dart';
import 'package:shadow/Pages/CreationPage/PollCreationWidgets/PollSettingsPage.dart';
import 'package:shadow/Pages/CreationPage/PollCreationWidgets/SelectOptionsPage.dart';
import 'package:shadow/Pages/CreationPage/PollCreationWidgets/ShadeSelectionPage.dart';
import 'package:shadow/Pages/CreationPage/ShadeCreationWidgets/ChooseShadeAccessPage.dart';
import 'package:shadow/Pages/CreationPage/ShadeCreationWidgets/NameYourShadePage.dart';
import 'package:shadow/Pages/CreationPage/ShadeCreationWidgets/ShadeConfirmationPage.dart';
import 'package:shadow/Pages/CreationPage/ShadeCreationWidgets/ShadeDescriptionPage.dart';
import 'package:shadow/Pages/CreationPage/ShadeCreationWidgets/ShadeGuidelinePage.dart';
import 'package:shadow/Pages/CreationPage/ShadeCreationWidgets/ShadeIconPicker.dart';
import 'package:shadow/Pages/CreationPage/QuestionPage.dart';
import 'package:shadow/Services/AuthService.dart';

import 'PollCreationWidgets/PollDescriptionPage.dart';
import 'ShadeCreationWidgets/ShadeTagPage.dart';

class CreationPage extends StatefulWidget {

  final Function updateColors;

  const CreationPage({Key key, this.updateColors}) : super(key: key);

  @override
  _CreationPageState createState() => _CreationPageState();
}

class _CreationPageState extends State<CreationPage> {
  PageController pageController = PageController(initialPage: 0);

  int pageCount = 0;
  int pageControlIndex = 0;
  List<Color> mcqColors = [grey2, grey2];
  List<Color> mcqColorsPP = [grey2, grey2];

  nextPage() {
    if(pageController.page < pageCount) pageController.animateToPage(pageController.page.toInt() + 1, duration: Duration(seconds: 1), curve: Curves.easeInOut);
  }
  previousPage() {
    if(pageController.page > 0) pageController.animateToPage(pageController.page.toInt() - 1, duration: Duration(seconds: 1), curve: Curves.easeInOut);
  }

  void updateForm(int pageCountInput) {
    setState(() {
      pageCount = pageCountInput;
    });
  }

  void updateColorOfMCQ(Color color, int index) {
    setState(() {
      mcqColors[index] = color == grey2 ? aquamarine : grey2;
      if(index == 0 && mcqColors[0] == aquamarine && mcqColors[0] == mcqColors[1]) {
        mcqColors[1] = grey2;
      }
      else if(index ==  1 && mcqColors[0] == aquamarine && mcqColors[0] == mcqColors[1]) {
        mcqColors[0] = grey2;
      }
    });
  }

  void updateColorOfMCQPrivatePublic(Color color, int index) {
    setState(() {
      mcqColorsPP[index] = color == grey2 ? aquamarine : grey2;
      if(index == 0 && mcqColorsPP[0] == aquamarine && mcqColorsPP[0] == mcqColorsPP[1]) {
        mcqColorsPP[1] = grey2;
      }
      else if(index ==  1 && mcqColorsPP[0] == aquamarine && mcqColorsPP[0] == mcqColorsPP[1]) {
        mcqColorsPP[0] = grey2;
      }
    });
  }

  List values = ['not selected', 'Enter Shade Name', 'access', 'Type Description Here', 'image', ['tag 1', 'tag 2', 'tag 3', 'tag 4', 'tag 5',], 'Type Guidelines Here', 'not confirmed'];

  List pollValues = [['Shade Name', 'Shade UID'], 'Enter Poll Name', 'Enter Poll Description',
      [
        ['Enter Option 1', 'Enter Description (optional)', 4],
        ['Enter Option 2', 'Enter Description (optional)', 4],
        ['Enter Option 3', 'Enter Description (optional)', 4],
        ['Enter Option 4', 'Enter Description (optional)', 4],
        ['Enter Option 5', 'Enter Description (optional)', 4],
      ],
    'Enter Poll Settings',
    [false, false, 'not seen'],
    'not confirmed'
  ];

  File image;
  Poll poll = new Poll();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(color: grey1),
        child: SafeArea(
          child: Stack(
            children: <Widget>[
              PageView(
                controller: pageController,
                physics: values[7] != 'not confirmed' || pollValues[6] != 'not confirmed' ? NeverScrollableScrollPhysics() : null,
                onPageChanged: (int index) {
                  setState(() {
                    pageControlIndex = index;
                  });
                },
                children: [
                  QuestionPage(
                    values: values,
                    question: 'Choose Creation Type',
                    questionNumber: '01',
                    answers: ['shade', 'poll'],
                    mcqColors: mcqColors,
                    updateForm: updateForm,
                    updateColorOfMCQ: updateColorOfMCQ,
                    nextPage: nextPage,
                  ),
                  if(values[0] == 'shade') NameYourShadePage(
                    values: values,
                    question: 'Assign Shade Name',
                    questionNumber: '02',
                    updateForm: updateForm,
                    nextPage: nextPage
                  ),
                  if(values[0] == 'shade' && values[1] != 'Enter Shade Name') ChooseShadeAccessPage(
                    values: values,
                    question: 'Choose Shade Access Type',
                    questionNumber: '03',
                    answers: ['public', 'private'],
                    mcqColors: mcqColorsPP,
                    updateForm: updateForm,
                    updateColorOfMCQ: updateColorOfMCQPrivatePublic,
                    nextPage: nextPage,
                  ),
                  if(values[0] == 'shade' && values[2] != 'access') ShadeDescriptionPage(
                    values: values,
                    question: 'Write A Shade Description',
                    questionNumber: '04',
                    updateForm: updateForm,
                    nextPage: nextPage
                  ),
                  if(values[0] == 'shade' && values[3] != 'Type Description Here') ShadeIconPicker(
                    values: values,
                    question: 'Pick A Shade Icon',
                    questionNumber: '05',
                    updateForm: updateForm,
                    nextPage: nextPage,
                  ),
                  if(values[0] == 'shade' && values[4] != 'image')
                    ShadeTagPage(
                      values: values,
                      question: 'Choose Up To 5 Tags For Your Shade',
                      questionNumber: '06',
                      updateForm: updateForm,
                      nextPage: nextPage,
                    ),
                  if(values[0] == 'shade' && values[5][0] != 'tag 1') ShadeGuidelinePage(
                    values: values,
                    question: 'Add Shade Guidelines',
                    questionNumber: '07',
                    updateForm: updateForm,
                    nextPage: nextPage,
                  ),
                  if(values[0] == 'shade' && values[6] != 'Type Guidelines Here')
                    StreamProvider<FirebaseShadow>.value(
                    value: AuthService().firebaseShadow,
                    child: ShadeConfirmationPage(
                      values: values,
                      question: 'Shade Confirmation',
                      questionNumber: '08',
                      updateForm: updateForm,
                      nextPage: nextPage
                    ),
                  ),
                  if(values[0] == 'shade' && values[7] != 'not confirmed') StorageUploader(updateColors: widget.updateColors, file: values[4], values: values),

                  //poll creation pages
                  if(values[0] == 'poll') ShadeSelectionPage(
                      values: pollValues,
                      question: 'Choose Your Shade',
                      questionNumber: '02',
                      updateForm: updateForm,
                      nextPage: nextPage,
                  ),
                  if(values[0] == 'poll' && pollValues[0][0] != 'Shade Name') PollNamePage(
                    values: pollValues,
                    question: 'Name Your Poll',
                    questionNumber: '03',
                    updateForm: updateForm,
                    nextPage: nextPage,
                  ),
                  if(values[0] == 'poll' && pollValues[1] != 'Enter Poll Name') PollDescriptionPage(
                    values: pollValues,
                    question: 'Write A Poll Description',
                    questionNumber: '04',
                    updateForm: updateForm,
                    nextPage: nextPage,
                  ),
                  if(values[0] == 'poll' && pollValues[2] != 'Enter Poll Description') SelectOptionsPage(
                    values: pollValues,
                    question: 'Add Poll Options',
                    questionNumber: '05',
                    updateForm: updateForm,
                    nextPage: nextPage,
                  ),
                  if(values[0] == 'poll' && pollValues[3][0][0] != 'Enter Option 1' && pollValues[3][1][0] != 'Enter Option 2') PollSettingsPage(
                    values: pollValues,
                    questionNumber: '06',
                    question: 'Poll Settings',
                    updateForm: updateForm,
                    nextPage: nextPage,
                  ),
                  if(values[0] == 'poll' && pollValues[5][2] == 'seen')
                    StreamProvider<FirebaseShadow>.value(
                      value: AuthService().firebaseShadow,
                      child: PollConfirmationPage(
                        updateColors: widget.updateColors,
                          values: pollValues,
                          question: 'Poll Confirmation',
                          questionNumber: '07',
                          updateForm: updateForm,
                          nextPage: nextPage
                      ),
                    ),
                ],
              ),
              Positioned(
                bottom: 32,
                left: 32,
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 1.0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: grey3,
                        ),
                        height: 3,
                        width: MediaQuery.of(context).size.width - 64,
                      ),
                    ),
                    AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      transform: Matrix4.identity()
                        ..translate(((MediaQuery.of(context).size.width - 64)/8) * (pageControlIndex)),
                      color: aquamarine,
                      height: 5,
                      width: (MediaQuery.of(context).size.width - 64)/8,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class StepNumber extends StatelessWidget {
  final String questionNumber;
  StepNumber({this.questionNumber});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        questionNumber,
        style: TextStyle(
            color: aquamarine.withOpacity(0.75),
            fontSize: 72,
            fontWeight: FontWeight.bold,
            fontFamily: 'Gilroy'
        ),
      ),
    );
  }
}

class Question extends StatelessWidget {
  final String question;
  Question({this.question});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        question,
        style: TextStyle(
            color: white,
            fontSize: 18,
            fontFamily: 'Gilroy',
            fontWeight: FontWeight.bold
        ),
      ),
    );
  }
}