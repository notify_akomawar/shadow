import 'package:flutter/material.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';

import '../../SharedWidgets/FormElements.dart';

class QuestionPage extends StatelessWidget {
  final List values;
  final String questionNumber;
  final String question;
  final List<String> answers;
  final VoidCallback onOptionSelected;
  final List<Color> mcqColors;
  final Function updateForm;
  final Function updateColorOfMCQ;
  final Function nextPage;

  QuestionPage({this.values, this.questionNumber, this.question, this.answers, this.onOptionSelected, this.mcqColors, this.updateForm, this.updateColorOfMCQ, this.nextPage});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 32),
          StepNumber(questionNumber: questionNumber),
          Question(question: question),
          Spacer(),
          InkWell(
            onTap: () {
              answers[0] == 'shade' ? values[0] = 'shade' : values[0] = 'poll';
              updateForm(answers[0] == 'shade' ? 1 : 0);
              updateColorOfMCQ(mcqColors[0], 0);
            },
            child: MCQ(text: "Shade", color: mcqColors[0], optionLetter: "A"),
          ),
          SizedBox(height: 16,),
          InkWell(
            onTap: () {
              answers[1] == 'poll' ? values[0] = 'poll' : values[0] = 'shade';
              updateForm(answers[1] == 'poll' ? 1 : 0);
              updateColorOfMCQ(mcqColors[1], 1);
            },
            child: MCQ(text: "Poll", color: mcqColors[1], optionLetter: "B",),
          ),
          SizedBox(height: 16),
          NextButton(onPressed: nextPage),
//              ...answers.map((String answer) {
//                return InkWell(
//                  onTap: () {
//                    answer == 'shade' ? values[0] = 'shade' : values[0] = 'poll';
//                    updateForm(answer == 'shade' ? 1 : 0);
//                  },
//                  child: OptionItem(
//                      answer: answer,
//                      onTap: onOptionSelected
//                  ),
//                );
//              }),
          Spacer()
        ],
      ),
    );
  }
}