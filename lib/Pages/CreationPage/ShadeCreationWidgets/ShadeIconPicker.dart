import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class ShadeIconPicker extends StatefulWidget {
  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;

  ShadeIconPicker({this.values, this.question, this.questionNumber, this.updateForm, this.nextPage});

  @override
  _ShadeIconPickerState createState() => _ShadeIconPickerState();
}

class _ShadeIconPickerState extends State<ShadeIconPicker> {
  File _image;

  Future getImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source, maxWidth: 500, maxHeight: 500);

    setState(() {
      widget.values[4] = image;
      _image = image;
    });
  }

  Future<void> cropImage() async {
    if(_image != null) {
      var image = await ImageCropper.cropImage(
        sourcePath: _image.path,
        aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1)
      );

      setState(() {
        widget.values[4] = image;
        _image = image;
      });
    }
  }

  void onPressed() {
    if(_image != null) {
      widget.values[4] = _image;
      widget.updateForm(5);
      widget.nextPage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 32),
          StepNumber(questionNumber: widget.questionNumber),
          Question(question: widget.question),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text('Add an image to your shade', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 12)),
          ),
          Spacer(),
          Container(
            width: MediaQuery.of(context).size.width - 64,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Container(
                    height: 250,
                    width: 250,
                    decoration: BoxDecoration(
                        color: grey2,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: widget.values[4] == 'image' ? Container() : Image.file(widget.values[4]),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height/4,
                  child: Column(
                    children: [
                      IconButton(
                        icon: Icon(Icons.photo_camera, color: aquamarine),
                        onPressed: () => getImage(ImageSource.camera),
                      ),
                      IconButton(
                        icon: Icon(Icons.photo_library, color: aquamarine),
                        onPressed: () => getImage(ImageSource.gallery),
                      ),
                      IconButton(
                        icon: Icon(Icons.crop, color: aquamarine),
                        onPressed: () => cropImage(),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 16),
          NextButton(onPressed: onPressed),
          Spacer()
        ],
      ),
    );
  }
}
