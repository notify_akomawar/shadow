import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class ChooseShadeAccessPage extends StatelessWidget {
  final List values;
  final String questionNumber;
  final String question;
  final List<String> answers;
  final VoidCallback onOptionSelected;
  final List<Color> mcqColors;
  final Function updateForm;
  final Function updateColorOfMCQ;
  final Function nextPage;

  ChooseShadeAccessPage({this.values, this.questionNumber, this.question, this.answers, this.onOptionSelected, this.updateForm, this.updateColorOfMCQ, this.mcqColors, this.nextPage});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 32),
          StepNumber(questionNumber: questionNumber),
          Question(question: question),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text('recommended type public, allows access to all users', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 12)),
          ),
          Spacer(),
          InkWell(
            onTap: () {
              values[2] = 'public';
              updateForm(3);
              updateColorOfMCQ(mcqColors[0], 0);
            },
            child: MCQ(
              text: "Public",
              color: mcqColors[0],
              optionLetter: "A",
            )
          ),
          SizedBox(height: 16,),
          InkWell(
            onTap: () {
              values[2] = 'private';
              updateForm(3);
              updateColorOfMCQ(mcqColors[1], 1);
            },
            child: MCQ(
              text: "Private",
              color: mcqColors[1],
              optionLetter: "B",
            )
          ),
          SizedBox(height: 16),
          NextButton(onPressed: nextPage),
          Spacer()
        ],
      ),
    );
  }
}
