import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class ShadeDescriptionPage extends StatefulWidget {
  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;

  ShadeDescriptionPage({this.values, this.question, this.questionNumber, this.updateForm, this.nextPage});

  @override
  _ShadeDescriptionPageState createState() => _ShadeDescriptionPageState();
}

class _ShadeDescriptionPageState extends State<ShadeDescriptionPage> {
  var description = '';

  void initState() {
    super.initState();
    if(widget.values[3] != null && widget.values[3] != 'Type Description Here')
      description = widget.values[3];
  }

  void onPressed() {
    FocusScope.of(context).requestFocus(FocusNode());
    if(description != null) {
      widget.values[3] = description;
      widget.updateForm(4);
      widget.nextPage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 32),
          StepNumber(questionNumber: widget.questionNumber),
          Question(question: widget.question),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text('ensure your shade description is informative', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 12)),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Container(
              height: 140,
              width: MediaQuery.of(context).size.width - 64,
              decoration: BoxDecoration(
                  color: grey2,
                  borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                child: TextFormField(
                  initialValue: widget.values[3] == 'Type Description Here' ? null : widget.values[3],
                  keyboardType: TextInputType.multiline,
                  maxLines: 6,
                  maxLength: 250,
                  style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                  decoration: InputDecoration(
                    hintText: widget.values[3],
                    hintStyle: TextStyle(color: grey3, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    counterStyle: TextStyle(fontFamily: 'Gilroy', color: aquamarine)
                  ),
                  validator: (value) => value.isEmpty ? '' : null,
                  onChanged: (value) {
                    description = value;
                  },
                ),
              ),
            ),
          ),
          SizedBox(height: 16),
          NextButton(onPressed: onPressed),
          Spacer()
        ],
      ),
    );
  }
}
