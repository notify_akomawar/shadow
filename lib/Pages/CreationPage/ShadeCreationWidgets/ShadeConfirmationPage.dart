import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/Services/DatabaseService.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';
import 'package:shadow/Services/CreationService.dart';

class ShadeConfirmationPage extends StatefulWidget {
  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;

  ShadeConfirmationPage({this.values, this.question, this.questionNumber, this.updateForm, this.nextPage});

  @override
  _ShadeConfirmationPageState createState() => _ShadeConfirmationPageState();
}

class _ShadeConfirmationPageState extends State<ShadeConfirmationPage> {
  final CollectionReference shadeCollection = Firestore.instance.collection('shades');
  final isButtonPressed = false;

  @override
  Widget build(BuildContext context) {
    final shadow = Provider.of<FirebaseShadow>(context);

    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width - 64,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 32),
            StepNumber(questionNumber: widget.questionNumber),
            Question(question: widget.question),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text('swipe left to make changes to any of the fields', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 12)),
            ),
            Spacer(),
            Padding(
              padding: const EdgeInsets.only(left: 2.0),
              child: Row(
                children: [
                  RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(text: 'shade[', style: TextStyle(
                              color: white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold
                          )),
                          TextSpan(text: widget.values[1], style: TextStyle(
                              color: aquamarine,
                              fontSize: 16,
                              fontWeight: FontWeight.bold
                          )),
                          TextSpan(text: ']', style: TextStyle(
                              color: white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold
                          )),
                        ]
                    ),
                  ),
                  Spacer(),
                  Text(
                    widget.values[2].toString().toUpperCase(),
                    style: TextStyle(
                        color: aquamarine,
                        fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 8),
            Container(
              width: MediaQuery.of(context).size.width - 64,
              decoration: BoxDecoration(
                color: grey2,
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Text(
                            widget.values[3],
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: grey4,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(width: 16),
                        Container(
                          height: 100,
                          width: 100,
                          child: ClipRRect(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              child: Image.file(widget.values[4])
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 16),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Guidelines',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          color: aquamarine,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Text(
                      widget.values[6],
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        color: grey3,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Container(
                height: 40,
                width: 250,
                decoration: BoxDecoration(
                    color: grey2,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Center(
                  child: FlatButton.icon(
                    color: Colors.transparent,
                    icon: Icon(Icons.cloud_upload, color: white),
                    label: Text(
                      'Confirm Shade Creation',
                      style: TextStyle(
                        color: aquamarine,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onPressed: () async {
                      if(!isButtonPressed && shadow != null) {
                        var uid = Firestore.instance.collection('shades').document().documentID;
                        await ShadeCreationService(uid: uid, shadowID: shadow.uid.toString()).createShade(widget.values);
                        await ShadeCreationService(uid: uid, shadowID: shadow.uid.toString()).addToUserCreatedShades(shadow.uid.toString(), uid);
                        await ShadeCreationService(uid: uid, shadowID: shadow.uid.toString()).addFollowedShadeToShadow();

                        for(int i = 0; i < widget.values[5].length; i++) {
                          ShadowProfile author = await FirestoreService().getShadow(shadow.uid);
                          if(widget.values[5][i] != "tag " + (i+1).toString())
                            await TagCreationService().createCustomTag(widget.values[5][i], shadow.uid.toString(), author.username);
                        }

                        for(int i = 0; i < widget.values[5].length; i++)
                          if(widget.values[5][i] != "tag " + (i+1).toString())
                            await ShadeCreationService(uid: uid, shadowID: shadow.uid.toString()).addShadeUIDToTag(widget.values[5][i]);
                        setState(() {
                          widget.values[7] = uid.toString();
                          widget.updateForm(8);
                          widget.nextPage();
                        });
                      }
                    },
                  ),
                ),
              ),
            ),
            Spacer()
          ],
        ),
      ),
    );
  }
}

class PreviewObject extends StatelessWidget {
  final title;
  final content;

  PreviewObject({this.title, this.content});

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: title + '\n', style: TextStyle(
            color: aquamarine,
            fontSize: 20,
            fontWeight: FontWeight.bold
          )),
          TextSpan(text: content, style: TextStyle(
              color: grey4,
              fontSize: 18,
              fontWeight: FontWeight.bold
          )),
        ]
      ),
    );
  }
}
