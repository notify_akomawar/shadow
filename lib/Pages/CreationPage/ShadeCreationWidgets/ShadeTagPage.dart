import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

import '../CreationPage.dart';

class ShadeTagPage extends StatefulWidget {

  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;

  const ShadeTagPage({Key key, this.values, this.question, this.questionNumber, this.updateForm, this.nextPage}) : super(key: key);



  @override
  _ShadeTagPageState createState() => _ShadeTagPageState();
}

class _ShadeTagPageState extends State<ShadeTagPage> {

  int tagCount = 0;

  bool topVisible = true;
  bool tagsVisible = false;

  void onPressed() {
    widget.updateForm(6);
    widget.nextPage();
  }

  @override
  void initState() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          topVisible = visible ? false : true;
        });
      },
    );
    _updateSelectedTags();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Visibility(visible: topVisible, child: SizedBox(height: 32)),
          Visibility(visible: topVisible, child: StepNumber(questionNumber: widget.questionNumber)),
          Visibility(visible: topVisible, child: Question(question: widget.question)),
          Visibility(
            visible: topVisible,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text('Choose a tag', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 12)),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height/2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Visibility(
                    visible: tagsVisible,
                    child: Flexible(
                      flex: 1,
                      child: TagDisplay(
                        values: widget.values,
                        tagCount: tagCount,
                        updateSelected: _updateSelectedTags,
                      ),
                    )
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0, top: 8.0),
                  child: TagSearchBar(
                    values: widget.values,
                    updateForm: widget.updateForm,
                    nextPage: widget.nextPage,
                    updateSelected: _updateSelectedTags,
                  ),
//            child: SizedBox(width: MediaQuery.of(context).size.width - 64, child: Placeholder()),
                ),
              ],
            ),
          ),
          Spacer(),
          Visibility(visible: topVisible, child: NextButton(onPressed: onPressed)),
          Spacer(),
        ],
      ),
    );
  }

  _updateSelectedTags() {
      setState(() {
        tagCount = 0;
        for (int i = 0; i < widget.values[5].length; i++) {
          if(widget.values[5][i] != 'tag ' + (i+1).toString()) {
            tagCount++;
          }
        }
        if(tagCount == 0) tagsVisible = false;
        else tagsVisible = true;
      });
  }
}

class TagDisplay extends StatelessWidget {

  final List values;
  final int tagCount;
  final Function updateSelected;

  const TagDisplay({Key key, this.values, this.tagCount, this.updateSelected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: tagCount > 3 ? 100 : 56,
      width: MediaQuery.of(context).size.width - 64,
//      child: Placeholder(),
      child: Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Visibility(
                    visible: tagCount > 0,
                    child: Tag(
                      values[5][0],
                      removeTag: (){
                        _removeTag(0);
                        updateSelected();
                      },
                    ),
                  ),
                  Visibility(
                    visible: tagCount > 1,
                    child: Tag(
                      values[5][1],
                      removeTag: (){
                        _removeTag(1);
                        updateSelected();
                      },
                    ),
                  ),
                  Visibility(
                    visible: tagCount > 2,
                    child: Tag(
                      values[5][2],
                      removeTag: (){
                        _removeTag(2);
                        updateSelected();
                      },
                    ),
                  ),
                ]
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 4.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Visibility(
                      visible: tagCount > 3,
                      child: Tag(
                        values[5][3],
                        removeTag: (){
                          _removeTag(3);
                          updateSelected();
                        },
                      ),
                    ),
                    Visibility(
                      visible: tagCount > 4,
                      child: Tag(
                        values[5][4],
                        removeTag: (){
                          _removeTag(4);
                          updateSelected();
                        },
                      ),
                    ),
                  ]
              ),
            ),
          ],
        ),
      )
    );
  }

  _removeTag(int index) {
    values[5][index] = 'tag ' + (index + 1).toString();
    if(index + 1 != tagCount) {
      for(int i = index; i < tagCount - 1; i++) {
        values[5][i] = values[5][i + 1];
        values[5][i + 1] = 'tag ' + (i + 2).toString();
      }
    }
  }
}

class Tag extends StatelessWidget {

  final String name;
  final Function removeTag;

  const Tag(this.name, {this.removeTag});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Container(
        decoration: BoxDecoration(
          color: grey2,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Row(
          children: [
            Expanded(
              flex: 0,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  name,
                  style: aquamarineBoldTextStyle.copyWith(fontSize: 16),
                ),
              ),
            ),
            Expanded(
              flex: 0,
              child: InkWell(
                onTap: () => removeTag(),
                child: Icon(
                  Icons.close,
                  color: Colors.redAccent,
                ),
              )
            ),
            SizedBox(width: 4)
          ],
        )
      ),
    );
  }
}

