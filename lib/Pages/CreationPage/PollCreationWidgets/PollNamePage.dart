import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

import '../CreationPage.dart';

class PollNamePage extends StatefulWidget {

  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;

  const PollNamePage({Key key, this.values, this.question, this.questionNumber, this.updateForm, this.nextPage}) : super(key: key);

  @override
  _PollNamePageState createState() => _PollNamePageState();
}

class _PollNamePageState extends State<PollNamePage> {
  var tempString = '';
  var confirmedString = '';

  void updateTempString(String input) {
    setState(() {
      tempString = input;
    });
  }

  void updateConfirmedString(String input) {
    setState(() {
      confirmedString = input;
    });
  }

  void onPressed() {
    FocusScope.of(context).requestFocus(FocusNode());
    if (tempString == confirmedString && tempString != '') {
      widget.values[1] = confirmedString;
      widget.updateForm(3);
      widget.nextPage();
    }
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 32),
          StepNumber(questionNumber: widget.questionNumber),
          Question(question: widget.question),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
                'Ensure your poll name is specific and precise',
                style: TextStyle(
                    color: aquamarine,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.bold,
                    fontSize: 12
                )
            ),
          ),
          Spacer(),
          InputFormField(
            hintText: ("Enter Your Poll Name"),
            input: widget.values[1],
            updateString: updateTempString,
            maxLength: 100,
          ),
          SizedBox(height: 16),
          InputFormField(
            hintText: "Confirm Your Poll Name",
            input: widget.values[1],
            updateString: updateConfirmedString,
            maxLength: 100,
          ),
          SizedBox(height: 16),
          NextButton(onPressed: onPressed),
          Spacer()
        ],
      ),
    );
  }
}
