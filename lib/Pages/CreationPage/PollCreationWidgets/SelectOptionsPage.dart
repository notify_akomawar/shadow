import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class SelectOptionsPage extends StatefulWidget {

  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;
  final int maxOptions = 5;
  final int minOptions = 2;

  SelectOptionsPage({Key key, this.values, this.question, this.questionNumber, this.updateForm, this.nextPage}) : super(key: key);

  @override
  _SelectOptionsPageState createState() => _SelectOptionsPageState();
}

class _SelectOptionsPageState extends State<SelectOptionsPage> {

  int numOptions;
  bool topVisible = true;

  @override
  void initState() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          topVisible = visible ? false : true;
        });
      },
    );  super.initState();

    int current = 2;
    for(int i = 2; i < 5; i++) {
      if(widget.values[3][i][0] != 'Enter Option ' + (i+1).toString()) {
        current++;
      }
    }
    setState(() {
      numOptions = current;
    });
  }

  void onPressed() {
    FocusScope.of(context).requestFocus(FocusNode());
    bool valid = true;
    for(int i = 0; i < numOptions; i++)
    if (widget.values[3][i][0] == 'Enter Option ' + (i+1).toString()) {
      valid = false;
    }
    if(valid) {
      widget.updateForm(5);
      widget.nextPage();
    }
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 32),
          Visibility(visible: topVisible, child: StepNumber(questionNumber: widget.questionNumber,)),
          Visibility(visible: topVisible, child: Question(question: widget.question,)),
          Visibility(
            visible: topVisible,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text('Fill out options and descriptions for your poll', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 12)),
            ),
          ),
          Spacer(),
          OptionSelector(
            numOptions: numOptions,
            values: widget.values,
            updateOption: _updateOption,
            updateOptionDesc: _updateOptionDesc,
            updateOptionColor: _updateOptionColor,

          ),
          Spacer(),
          Container(
            width: MediaQuery.of(context).size.width - 64,
            child: Row(
              children: <Widget> [
                Expanded(flex: 1, child: NextButton(onPressed: onPressed)),
                Expanded(
                  flex: 0,
                  child: Visibility(
                      visible: numOptions < widget.maxOptions,
                      child: OptionButton(
                        changeOptions: incNumOptions,
                      )
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: Visibility(
                      visible: numOptions > widget.minOptions,
                      child: OptionButton(
                        add: false,
                        changeOptions: decNumOptions,
                      )
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }

  _updateOption(int index, String value) {
    setState(() {
      widget.values[3][index][0] = value;
    });
  }

  _updateOptionDesc(int index, String value) {
    setState(() {
      widget.values[3][index][1] = value;
    });
  }

  _updateOptionColor(int index, int value) {
    setState(() {
      widget.values[3][index][2] = value;
    });
  }

  incNumOptions() {
    setState(() {
      if(numOptions < widget.maxOptions) {
        widget.values[3][numOptions][0] = 'Enter Option ' + (numOptions + 1).toString();
        widget.values[3][numOptions][1] = 'Enter Description (optional)';
        widget.values[3][numOptions][2] = 4;
        numOptions++;
      }
    });
  }

  decNumOptions() {
    setState(() {
      if(numOptions > widget.minOptions) {
        numOptions--;
        widget.values[3][numOptions][0] = 'Enter Option ' + (numOptions+1).toString();
        widget.values[3][numOptions][1] = 'Enter Description (optional)';
        widget.values[3][numOptions][2] = 4;
      }
    });
  }
}
