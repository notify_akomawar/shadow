import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class ShadeSelectionPage extends StatefulWidget {
  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;

  ShadeSelectionPage({this.values, this.question, this.questionNumber, this.updateForm, this.nextPage});

  @override
  _ShadeSelectionPageState createState() => _ShadeSelectionPageState();
}

class _ShadeSelectionPageState extends State<ShadeSelectionPage> {
  var searchString;
  bool topVisible = true;

  void onPressed() {
    widget.updateForm(2);
    widget.nextPage();
  }

  @override
  void initState() {
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          topVisible = visible ? false : true;
        });
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Visibility(visible: topVisible, child: SizedBox(height: 32)),
          Visibility(visible: topVisible, child: StepNumber(questionNumber: widget.questionNumber)),
          Visibility(visible: topVisible, child: Question(question: widget.question)),
          Visibility(
            visible: topVisible,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text('Select the shade you wish to upload the poll to', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 12)),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 8.0, top: 8.0),
            child: MiniSearchBar(values: widget.values, updateForm: widget.updateForm, nextPage: widget.nextPage),
          ),
          Spacer(),
          Visibility(visible: topVisible, child: NextButton(onPressed: onPressed)),
          Spacer()
        ],
      ),
    );
  }
}
