import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/Services/CreationService.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/SharedWidgets/Confetti.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';
import 'package:shadow/main.dart';

class PollConfirmationPage extends StatefulWidget {
  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;
  final Function updateColors;

  PollConfirmationPage({this.values, this.question, this.questionNumber, this.updateForm, this.nextPage, this.updateColors});

  @override
  _PollConfirmationPageState createState() => _PollConfirmationPageState();
}

class _PollConfirmationPageState extends State<PollConfirmationPage> {
  final isButtonPressed = false;
  String dropdownValue = 'Option 1';
  bool visible = false;

  @override
  Widget build(BuildContext context) {
    final shadow = Provider.of<FirebaseShadow>(context);
    final CollectionReference pollCollection = Firestore.instance.collection('shades').document(widget.values[0].toString().trim()).collection('polls');
    final List optionList = widget.values[3];
    final List optionColorsList = [];
    List<String> items = [];

    List empty = ['Enter Option 3', 'Enter Option 4', 'Enter Option 5'];

    var index = 0;
    for(List option in optionList) {
      index += 1;
      if(!empty.contains(option[0])) {
        items.add('Option ' + index.toString());
        optionColorsList.add(option[2]);
      }
    }

    return Stack(
      children: [
        Center(
          child: Container(
            width: MediaQuery.of(context).size.width - 64,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 32),
                StepNumber(questionNumber: widget.questionNumber),
                Question(question: widget.question),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text('swipe left to make changes to any of the fields', style: TextStyle(color: aquamarine, fontFamily: 'Gilroy', fontWeight: FontWeight.bold, fontSize: 12)),
                ),
                Spacer(),
                Padding(
                  padding: const EdgeInsets.only(left: 2.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: RichText(
                          text: TextSpan(
                              children: [
                                TextSpan(text: 'poll[', style: TextStyle(
                                    color: white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold
                                )),
                                TextSpan(text: widget.values[1], style: TextStyle(
                                    color: aquamarine,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold
                                )),
                                TextSpan(text: ']', style: TextStyle(
                                    color: white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold
                                )),
                              ]
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  width: MediaQuery.of(context).size.width - 64,
                  decoration: BoxDecoration(
                      color: grey2,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                widget.values[2],
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: grey4,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 16),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: DropdownButton(
                            dropdownColor: grey1,
                            value: dropdownValue,
                            underline: Container(
                              height: 2,
                              color: ColorChooser.graphColors[optionColorsList[items.indexOf(dropdownValue)]],
                            ),
                            items: items.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                value: dropDownStringItem,
                                child: Text(
                                  dropDownStringItem,
                                  style: TextStyle(
                                      color: ColorChooser.graphColors[optionColorsList[items.indexOf(dropdownValue)]],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14
                                  ),
                                ),
                              );
                            }).toList(),
                            onChanged: (String value) {
                              setState(() {
                                dropdownValue = value;
                              });
                            },
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                optionList[items.indexOf(dropdownValue)][1],
                                style: TextStyle(
                                    color: grey4,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 16),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Container(
                    height: 40,
                    width: 250,
                    decoration: BoxDecoration(
                        color: grey2,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: Center(
                      child: FlatButton.icon(
                        color: Colors.transparent,
                        icon: Icon(Icons.cloud_upload, color: white),
                        label: Text(
                          'Confirm Poll Creation',
                          style: TextStyle(
                            color: aquamarine,
                            fontFamily: 'Gilroy',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () async {
                          if(!isButtonPressed && shadow != null) {
                            setState(() {
                              widget.values[6] = 'confirmed';
                              visible = !visible;
                            });
                            var uid = Firestore.instance.collection('shades').document().documentID;
                            await PollCreationService(uid: uid, shadeID: widget.values[0][1].toString().trim(), shadowID: shadow.uid.toString()).createPoll(widget.values);
//                            await PollCreationService(uid: uid, shadeID: widget.values[0][1].toString().trim(), shadowID: shadow.uid.toString()).addPollToShadow(widget.values);
                            await PollCreationService(uid: uid, shadeID: widget.values[0][1].toString().trim(), shadowID: shadow.uid.toString()).addToUserPolls(shadow.uid.toString(), widget.values[0][1].toString().trim(), uid);
                            await PollCreationService(uid: uid, shadeID: widget.values[0][1].toString().trim(), shadowID: shadow.uid.toString()).updatePollsInShade();
                            await PollCreationService(uid: uid, shadeID: widget.values[0][1].toString().trim(), shadowID: shadow.uid.toString()).addInitialDiscussionToPoll();
                            await FirestoreService().addUsersJoinedShades(shadow.uid.toString(), widget.values[0][0].toString().trim(), widget.values[0][1].toString().trim());
                          }
                        },
                      ),
                    ),
                  ),
                ),
                Spacer()
              ],
            ),
          ),
        ),
        Visibility(
          visible: visible,
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Center(
                child: Container(
                  width: MediaQuery.of(context).size.width - 64,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 2.0),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: RichText(
                                text: TextSpan(
                                    children: [
                                      TextSpan(text: 'poll[', style: TextStyle(
                                          color: white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold
                                      )),
                                      TextSpan(text: widget.values[1], style: TextStyle(
                                          color: aquamarine,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold
                                      )),
                                      TextSpan(text: ']', style: TextStyle(
                                          color: white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold
                                      )),
                                    ]
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 16),
                      Container(
                        width: MediaQuery.of(context).size.width - 64,
                        decoration: BoxDecoration(
                            color: grey2,
                            borderRadius: BorderRadius.all(Radius.circular(10))
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      widget.values[2],
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        color: grey4,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 16),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: DropdownButton(
                                  dropdownColor: grey1,
                                  value: dropdownValue,
                                  underline: Container(
                                    height: 2,
                                    color: ColorChooser.graphColors[optionColorsList[items.indexOf(dropdownValue)]],
                                  ),
                                  items: items.map((String dropDownStringItem) {
                                    return DropdownMenuItem<String>(
                                      value: dropDownStringItem,
                                      child: Text(
                                        dropDownStringItem,
                                        style: TextStyle(
                                            color: ColorChooser.graphColors[optionColorsList[items.indexOf(dropdownValue)]],
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14
                                        ),
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (String value) {
                                    setState(() {
                                      dropdownValue = value;
                                    });
                                  },
                                ),
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      optionList[items.indexOf(dropdownValue)][1],
                                      style: TextStyle(
                                          color: grey4,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 16),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all((Radius.circular(10))),
                          color: grey4,
                        ),
                        width: MediaQuery.of(context).size.width - 64,
                        child: Row(
                          children: [
                            FlatButton.icon(
                              icon: Icon(Icons.add, color: grey1, size: 35),
                              label: Text(
                                'Create Another Poll',
                                style: TextStyle(
                                    color: grey1,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                              onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil('/creationPage', (route) => false),
                            ),
                            Spacer(),
                            IconButton(
                              icon: Icon(Icons.home, color: grey1),
                              onPressed: () {
                                widget.updateColors([aquamarine, white, white, white, white]);
                                Navigator.of(context).pushNamedAndRemoveUntil(
                                    '/', (route) => false);
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        if(context != null && visible) Confetti()
      ]
    );
  }
}
