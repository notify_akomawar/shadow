import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';

class PollSettingsPage extends StatefulWidget {
  final List values;
  final String question;
  final String questionNumber;
  final Function updateForm;
  final Function nextPage;

  const PollSettingsPage({Key key, this.values, this.question, this.questionNumber, this.updateForm, this.nextPage}) : super(key: key);

  @override
  _PollSettingsPageState createState() => _PollSettingsPageState();
}

class _PollSettingsPageState extends State<PollSettingsPage> {
  List<bool> toggle = [false, false];
  List<String> settingNames = ['view statistics', 'poll life'];
  List<String> settingDescriptions = [
    'This toggle signifies that the a user  may not view information such as the poll statistics, and graphical representations of the poll until the user has voted for the poll. ',
    'Poll Life Description'
  ];
  int index;
  bool isShowOverlay = false;

  void toggleState(int index) {
    setState(() {
      toggle[index] = !toggle[index];
      widget.values[5][index] = toggle[index];
    });
  }

  void showOverlay(int indexInput) {
    setState(() {
      index = indexInput;
      isShowOverlay = !isShowOverlay;
    });
  }

  void onPressed() {
    widget.values[5][2] = 'seen';
    widget.updateForm(6);
    widget.nextPage();
  }

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Stack(
      children: [
        Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 32),
              StepNumber(questionNumber: widget.questionNumber),
              Question(question: widget.question),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                    'Shade Settings',
                    style: TextStyle(
                        color: aquamarine,
                        fontFamily: 'Gilroy',
                        fontWeight: FontWeight.bold,
                        fontSize: 12
                    )
                ),
              ),
              Spacer(),
              Container(
                width: MediaQuery.of(context).size.width - 64,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: SettingsRow(
                    settingName: settingNames[0],
                    settingDescription: settingDescriptions[0],
                    index: 0,
                    isToggled: toggle[0],
                    toggleState: toggleState,
                    showOverlay: showOverlay,
                  ),
                ),
              ),
              Spacer(),
              NextButton(onPressed: onPressed),
              Spacer()
            ],
          ),
        ),
        if(isShowOverlay) BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Center(
            child: Container(
              width: MediaQuery.of(context).size.width - 64,
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.arrow_back, color: white),
                        onPressed: () => showOverlay(index),
                      ),
                      Spacer(),
                      Text(
                        settingNames[index],
                        style: TextStyle(
                          color: aquamarine,
                          fontSize: 32,
                          fontWeight: FontWeight.bold
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      settingDescriptions[index],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  SizedBox(height: 16),
                  Align(
                    alignment: Alignment.center,
                    child: Toggle(
                      index: index,
                      isToggled: toggle[index],
                      toggleState: toggleState,
                    ),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class SettingsRow extends StatelessWidget {
  final String settingName;
  final String settingDescription;
  final int index;
  final bool isToggled;
  final Function toggleState;
  final Function showOverlay;

  SettingsRow({this.settingName, this.settingDescription, this.index, this.isToggled, this.toggleState, this.showOverlay});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 64,
      child: Row(
        children: [
          Text(
            settingName,
            style: TextStyle(
              color: grey5,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
          IconButton(
            icon: Icon(Icons.help, color: Colors.white),
            onPressed: () => showOverlay(index),
          ),
          Spacer(),
          Toggle(index: index, isToggled: isToggled, toggleState: toggleState)
        ],
      ),
    );
  }
}


class Toggle extends StatelessWidget {
  final int index;
  final bool isToggled;
  final Function toggleState;

  Toggle({this.index, this.isToggled, this.toggleState});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => toggleState(index),
      child: Stack(
        children: [
          Container(
            height: 50,
            width: 100,
            decoration: BoxDecoration(
              color: grey2,
              borderRadius: BorderRadius.all(Radius.circular(10))
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 4.0),
            child: AnimatedContainer(
              transform: Matrix4.identity()
                ..translate(isToggled ? 50.0 : 0.0),
              duration: Duration(milliseconds: 250),
              height: 42,
              width: 42,
              decoration: BoxDecoration(
                  color: isToggled ? aquamarine : grey4,
                  borderRadius: BorderRadius.all(Radius.circular(10))
              ),
            ),
          )
        ],
      ),
    );
  }
}

