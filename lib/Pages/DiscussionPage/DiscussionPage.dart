import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Constants/Widgets/Loading.dart';
import 'package:shadow/Models/MessageModel.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';
import 'package:shadow/Pages/DiscussionPage/MessageCard.dart';
import 'package:shadow/Pages/DiscussionPage/ThreadCreationPage.dart';
import 'package:shadow/Services/ChatService.dart';
import 'package:shadow/Services/FirestoreService.dart';

class DiscussionPage extends StatefulWidget {

  final Poll poll;
  final String shadowUID;

  const DiscussionPage({Key key, this.poll, this.shadowUID}) : super(key: key);

  @override
  _DiscussionPageState createState() => _DiscussionPageState();
}

class _DiscussionPageState extends State<DiscussionPage> {
  List<Message> selectedMessages = [];
  bool chatInputVisible = true;

  void selectMessage(Message message) {
    setState(() {
      if(Message().contains(selectedMessages, message)) {
        Message().remove(selectedMessages, message);
      } else {
        selectedMessages.add(message);
      }
    });
  }

  void _addMessage(message) async {
    ShadowProfile shadow = await FirestoreService().getShadow(widget.shadowUID);
    String option = widget.poll.shadowsVoted[widget.shadowUID][0];
    ChatService().createMessage(widget.poll.shadeUID, widget.poll.uid, widget.shadowUID,
        shadow.username, message, option);
  }

  @override
  Widget build(BuildContext context) {
    final messages = Provider.of<List>(context);

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0),
        child: Column(
          children: [
            Visibility(
              visible: selectedMessages.length != 0,
              child: Expanded(
                child: ListView.builder(
                  itemCount: selectedMessages.length + 1,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    if(index == 0) return Padding(
                      padding: const EdgeInsets.only(left: 8.0, top: 4.0, right: 8.0),
                      child: Center(
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            color: aquamarine,
                            borderRadius: BorderRadius.all(Radius.circular(5))
                          ),
                          child: InkWell(
                            child: Icon(Icons.create, color: grey1, size: 32),
                            onTap: () {
                              Navigator.push(context, PageRouteBuilder(
                                pageBuilder: (context, animation, secondaryAnimation) {
                                  return ThreadCreationPage(poll: widget.poll, shadowUID: widget.shadowUID, threadMessages: selectedMessages);
                                },
                                transitionDuration: Duration(milliseconds: 50),
                                transitionsBuilder: (context, animation, secondaryAnimations, child) => FadeTransition(opacity: animation, child: child),
                              ));
                            },
                          ),
                        ),
                      ),
                    );
                    return ConstrainedBox(
                        constraints: BoxConstraints(
                            maxHeight: 100,
                            maxWidth: MediaQuery.of(context).size.width - 128
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: MessageCard(
                            message: selectedMessages[index - 1],
                            selectMessage: selectMessage,
                            icon: Icon(Icons.cancel, color: Colors.redAccent, size: 18)
                          ),
                        )
                    );
                  },
                ),
              ),
            ),
            Visibility(
              visible: selectedMessages.length != 0,
              child: Divider(color: aquamarine, thickness: 2),
            ),
            Expanded(
              flex: 10,
              child: messages != null ? Container(
                child: ListView.builder(
                  padding: EdgeInsets.all(0.0),
                  itemCount: messages.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: MessageCard(
                        message: Message(
                          username: messages[index]['username'],
                          option: messages[index]['option'],
                          timestamp: DateTime.fromMillisecondsSinceEpoch(messages[index]['timestamp']),
                          selected: messages[index]['selected'],
                          message: messages[index]['message'],
                          authorUID: messages[index]['authorUID'],
                        ),
                        selectMessage: selectMessage,
                        icon: Icon(Icons.add, color: white, size: 16),
                      ),
                    );
                  }
                ),
              ) : Loading(),
            ),
            Expanded(
              flex: 0,
              child: MessageField(
                addMessage: _addMessage,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessageField extends StatefulWidget {

  final Function addMessage;

  const MessageField({Key key, this.addMessage}) : super(key: key);

  @override
  _MessageFieldState createState() => _MessageFieldState();
}

class _MessageFieldState extends State<MessageField> {

  TextEditingController _textEditingController;

  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
  }

  void dispose() {
    super.dispose();
    _textEditingController.dispose();
  }

  String message = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 10,
              child: TextFormField(
                controller: _textEditingController,
                style: grey5BoldTextStyle,
                decoration: InputDecoration(
                  hintText: "Enter a message...",
                  hintStyle: grey4BoldTextStyle.copyWith(fontSize: 14),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  counterText: '',
                ),
                onChanged: (value) {
                  setState(() {
                    message = value;
                  });
                },
              ),
            ),
            Spacer(),
            VerticalDivider(
              color: grey1,
              thickness: 4,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () {
                  widget.addMessage(message);
                  _textEditingController.clear();
                },
                child: Icon(
                  Icons.send,
                  color: aquamarine,
                )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
