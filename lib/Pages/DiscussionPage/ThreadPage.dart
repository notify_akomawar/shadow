import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/MessageModel.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';
import 'package:shadow/Models/ThreadModel.dart';
import 'package:shadow/Services/ChatService.dart';
import 'package:shadow/Services/FirestoreService.dart';

import 'DiscussionPage.dart';
import 'MessageCard.dart';

class ThreadPage extends StatefulWidget {
  final Thread thread;
  final String shadowUID;
  final Poll poll;
  ThreadPage({this.thread, this.shadowUID, this.poll});

  @override
  _ThreadPageState createState() => _ThreadPageState();
}

class _ThreadPageState extends State<ThreadPage> with SingleTickerProviderStateMixin {

  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  void _addMessage(String inputMessage) async {
    ShadowProfile shadow = await FirestoreService().getShadow(widget.shadowUID);
    String option = widget.poll.shadowsVoted[widget.shadowUID][0];
    await ChatService().createThreadMessage(widget.thread.shadeUID, widget.thread.pollUID, widget.thread.threadUID, inputMessage, option, false, widget.shadowUID, shadow.username);
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0),
        child: Scaffold(
          backgroundColor: grey1,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: grey1,
            elevation: 0.0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: (){Navigator.of(context).pop();},
                  child: Icon(
                    Icons.arrow_back,
                    color: white,
                    size: 24,
                  ),
                ),
                Spacer(),
                Text(widget.poll.name, style: whiteBoldTextStyle.copyWith(fontSize: 24))
              ],
            ),
            bottom: TabBar(
              tabs: [
                Tab(child: Text('Thread')),
                Tab(child: Text('Discussion'))
              ],
              controller: _tabController,
              indicatorColor: aquamarine,
              labelColor: aquamarine,
              labelStyle: aquamarineBoldTextStyle,
              unselectedLabelColor: white,
              unselectedLabelStyle: whiteBoldTextStyle,
            ),
            bottomOpacity: 1,
          ),
          body: TabBarView(
            children: [
              ListView.builder(
                  padding: EdgeInsets.all(0.0),
                  itemCount: widget.thread.threadMessages.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: MessageCard(
                        message: widget.thread.threadMessages[index],
                        icon: Icon(Icons.add, color: white, size: 16),
                      ),
                    );
                  }
              ),
              Column(
                children: [
                  Expanded(
                    flex: 10,
                    child: ListView.builder(
                        padding: EdgeInsets.all(0.0),
                        itemCount: widget.thread.childMessages.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: MessageCard(
                              message: widget.thread.childMessages[index],
                              icon: Icon(Icons.add, color: white, size: 16),
                            ),
                          );
                        }
                    ),
                  ),
                  Expanded(
                    flex: 0,
                    child: MessageField(
                      addMessage: _addMessage,
                    ),
                  ),
                ],
              )
            ],
            controller: _tabController,
          ),
        ),
      ),
    );
//    return SafeArea(
//      child: Padding(
//        padding: const EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0),
//        child: Column(
//          children: [
//            Expanded(
//              flex: 0,
//              child: Row(
//                children: [
//                  InkWell(
//                    child: Icon(Icons.arrow_back, color: white, size: 24),
//                    onTap: () => Navigator.of(context).pop(context),
//                  )
//                ],
//              )
//            ),
//            Expanded(
//              flex: 5,
//              child: ListView.builder(
//                padding: EdgeInsets.all(0.0),
//                itemCount: widget.thread.threadMessages.length,
//                shrinkWrap: true,
//                itemBuilder: (BuildContext context, int index) {
//                  return Padding(
//                    padding: const EdgeInsets.symmetric(vertical: 8.0),
//                    child: MessageCard(
//                      message: widget.thread.threadMessages[index],
//                      icon: Icon(Icons.add, color: white, size: 16),
//                    ),
//                  );
//                }
//              ),
//            ),
//            Expanded(
//              flex: 3,
//              child: ListView.builder(
//                padding: EdgeInsets.all(0.0),
//                itemCount: widget.thread.childMessages.length,
//                itemBuilder: (BuildContext context, int index) {
//                  return Padding(
//                    padding: const EdgeInsets.symmetric(vertical: 8.0),
//                    child: MessageCard(
//                      message: widget.thread.childMessages[index],
//                      icon: Icon(Icons.add, color: white, size: 16),
//                    ),
//                  );
//                }
//              )
//            ),
//            Expanded(
//              flex: 0,
//              child: MessageField(
//                addMessage: _addMessage,
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
  }
}