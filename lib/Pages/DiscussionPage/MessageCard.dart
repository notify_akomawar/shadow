import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/MessageModel.dart';
import 'package:intl/intl.dart';

class MessageCard extends StatelessWidget {
  final Message message;
  final Function selectMessage;
  final Icon icon;
  MessageCard({this.message, this.selectMessage, this.icon});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: grey3,
        borderRadius: BorderRadius.all(Radius.circular(10))
      ),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minHeight: 80,
          maxHeight: 80
        ),
        child: Column(
          children: [
            Container(
              height: 30,
              decoration: BoxDecoration(
                color: grey2,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('shadow[${message.username}]', style: whiteBoldTextStyle.copyWith(fontSize: 12)),
                    SizedBox(width: 8),
                    Text(DateFormat.jm().format(message.timestamp), style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
                    Spacer(),
                    Text(message.option, style: grey3BoldTextStyle.copyWith(fontSize: 12), softWrap: true),
                    SizedBox(width: 8),
                    InkWell(
                      onTap: () => selectMessage(message),
                      child: icon
                    )
                  ],
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0, bottom: 8.0, right: 4.0),
                child: Align(alignment: Alignment.centerLeft, child: Text(message.message, style: whiteBoldTextStyle.copyWith(fontSize: 12), textAlign: TextAlign.left)),
              ),
            )
          ],
        ),
      ),
    );
  }
}