import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Constants/Widgets/Loading.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ThreadModel.dart';
import 'package:shadow/Pages/DiscussionPage/ThreadPage.dart';
import 'package:shadow/Services/ChatService.dart';

class ThreadsList extends StatelessWidget {
  final String shadowUID;
  final Poll poll;
  final List threadList;
  ThreadsList({this.shadowUID, this.poll, this.threadList});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: ListView.builder(
          itemCount: threadList.length,
          itemBuilder: (context, index) {
            return ThreadCard(thread: threadList[index], shadowUID: shadowUID, poll: poll, index: index);
          },
        ),
      ),
    );
  }
}

class ThreadCard extends StatelessWidget {
  final Thread thread;
  final String shadowUID;
  final Poll poll;
  final int index;
  ThreadCard({this.thread, this.shadowUID, this.poll, this.index});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, CupertinoPageRoute(builder: (BuildContext context) =>
          StreamBuilder<List>(
            stream: ChatService(shadeUID: poll.shadeUID, pollUID: poll.uid).threads,
            builder: (context, snapshot) {
              if(snapshot.hasData) return ThreadPage(thread: snapshot.data[index], shadowUID: shadowUID, poll: poll);
              else return Loading();
            }
          ))
        );
      },
      child: Card(
        color: grey2,
        child: ListTile(
          title: Text(thread.threadName.toString(), style: grey5BoldTextStyle),
          subtitle: Text(thread.creatorUsername ?? 'username', style: grey4BoldTextStyle),
          trailing: Text(thread.childMessages.length.toString(), style: aquamarineBoldTextStyle.copyWith(fontSize: 48)),
        ),
      ),
    );
  }
}
