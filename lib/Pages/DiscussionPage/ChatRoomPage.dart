import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Constants/Widgets/Loading.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ThreadModel.dart';
import 'package:shadow/Pages/DiscussionPage/ThreadsList.dart';
import 'package:shadow/Services/ChatService.dart';
import 'DiscussionPage.dart';

class ChatRoomPage extends StatefulWidget {
  final Poll poll;
  final String shadowUID;

  const ChatRoomPage({Key key, this.poll, this.shadowUID}) : super(key: key);

  @override
  _ChatRoomPageState createState() => _ChatRoomPageState();
}

class _ChatRoomPageState extends State<ChatRoomPage> with SingleTickerProviderStateMixin {

  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Scaffold(
          backgroundColor: grey1,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: grey1,
            elevation: 0.0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: (){Navigator.of(context).pop();},
                  child: Icon(
                    Icons.arrow_back,
                    color: white,
                    size: 24,
                  ),
                ),
                Spacer(),
                Text(widget.poll.name, style: whiteBoldTextStyle.copyWith(fontSize: 24))
              ],
            ),
            bottom: TabBar(
              tabs: [
                Tab(child: Text('Main')),
                Tab(child: Text('Threads'))
              ],
              controller: _tabController,
              indicatorColor: aquamarine,
              labelColor: aquamarine,
              labelStyle: aquamarineBoldTextStyle,
              unselectedLabelColor: white,
              unselectedLabelStyle: whiteBoldTextStyle,
            ),
            bottomOpacity: 1,
          ),
          body: TabBarView(
            children: [
              StreamProvider<List>.value(
                value: ChatService(shadeUID: widget.poll.shadeUID, pollUID: widget.poll.uid).pollMessages,
                child: DiscussionPage(
                  poll: widget.poll,
                  shadowUID: widget.shadowUID,
                ),
              ),
              StreamBuilder<List>(
                stream: ChatService(shadeUID: widget.poll.shadeUID, pollUID: widget.poll.uid).threads,
                builder: (context, snapshot) {
                  if(snapshot.hasData) return ThreadsList(poll: widget.poll, shadowUID: widget.shadowUID, threadList: snapshot.data);
                  else return Loading();
                },
              )
            ],
            controller: _tabController,
          ),
        ),
      ),
    );
  }
}
