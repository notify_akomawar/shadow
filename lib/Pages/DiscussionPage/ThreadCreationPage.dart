import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/MessageModel.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';
import 'package:shadow/Pages/DiscussionPage/MessageCard.dart';
import 'package:shadow/Services/ChatService.dart';
import 'package:shadow/Services/FirestoreService.dart';

class ThreadCreationPage extends StatefulWidget {
  final Poll poll;
  final String shadowUID;
  final List<Message> threadMessages;

  ThreadCreationPage({this.poll, this.shadowUID, this.threadMessages});

  @override
  _ThreadCreationPageState createState() => _ThreadCreationPageState();
}

class _ThreadCreationPageState extends State<ThreadCreationPage> {
  String threadName = '';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap:() => FocusScope.of(context).requestFocus(FocusNode()),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop(context);
                      },
                      child: Icon(Icons.arrow_back, color: white, size: 24),
                    ),
                    Spacer(),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        'Confirm Thread',
                        style: whiteBoldTextStyle.copyWith(fontSize: 24),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 32),
                TextFormField(
                  style: grey5BoldTextStyle,
                  decoration: emailInputDecoration.copyWith(hintText: 'enter thread title', labelText: 'Enter Thread Title', labelStyle: TextStyle(
                  color: myFocusNode.hasFocus ? white : white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                  ),),
                  onChanged: (value) {
                    setState(() {
                      threadName = value;
                    });
                  },
                ),
                SizedBox(height: 16),
                Align(alignment: Alignment.centerLeft, child: Text('poll[${widget.poll.name}]', style: aquamarineBoldTextStyle.copyWith(fontSize: 18))),
                Align(alignment: Alignment.centerLeft, child: Text('shade[${widget.poll.shadeName}]', style: aquamarineBoldTextStyle.copyWith(fontSize: 18))),
                SizedBox(height: 16),
                Align(alignment: Alignment.centerLeft, child: Text('Thread Message Preview', style: grey3BoldTextStyle.copyWith(fontSize: 20))),
                SizedBox(height: 8.0),
                Expanded(
                  flex: 10,
                  child: ListView.builder(
                    itemCount: widget.threadMessages.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4.0),
                        child: MessageCard(
                            message: widget.threadMessages[index],
                            icon: Icon(Icons.cancel, color: Colors.transparent, size: 18)
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(height: 8.0),
                InkWell(
                  child: Container(
                    width: double.infinity,
                    height: 50,
                    decoration: BoxDecoration(
                      color: aquamarine,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: Center(
                      child: Text(
                        'Confirm',
                        style: grey1BoldTextStyle.copyWith(fontSize: 20),
                      ),
                    ),
                  ),

                  onTap:() async {
                    if(threadName.length != 0) {
                      var uid = Firestore.instance.collection('shades').document().documentID;
                      List<Map<String, dynamic>> messagesMap = [];

                      messagesMap = widget.threadMessages.map((message) {
                        Map<String,dynamic> messageMap = new Map<String,dynamic>();
                        messageMap['message'] = message.message;
                        messageMap['option'] = message.option;
                        messageMap['selected'] = message.selected;
                        messageMap['timestamp'] = message.timestamp.toUtc().millisecondsSinceEpoch;
                        messageMap['authorUID'] = message.authorUID;
                        messageMap['username'] = message.username;
                        return messageMap;
                      }).toList();

                      ShadowProfile shadow = await FirestoreService().getShadow(widget.shadowUID);
                      ChatService().createThread(shadow.username, widget.poll.shadeUID, widget.poll.uid, uid, threadName, widget.poll.name, widget.shadowUID, messagesMap);
                      setState(() {
                        widget.threadMessages.clear();
                      });
                      Navigator.of(context).pop(context);
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
