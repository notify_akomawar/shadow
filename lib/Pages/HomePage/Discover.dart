import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:network_to_file_image/network_to_file_image.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Constants/Widgets/Loading.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Services/CloudStorageService.dart';
import 'package:shadow/Services/DiscoverService.dart';

import '../../Constants/ColorConstants.dart';
import '../../Constants/DesignConstants.dart';
import '../../Services/DiscoverService.dart';
import '../ProfilePages/ShadeProfilePage.dart';

class Discover extends StatefulWidget {
  final List topShades;
  final List sportsShades;
  final List politicsShades;
  final List techShades;
  final String shadowUID;

  Discover({this.shadowUID, this.topShades, this.sportsShades, this.politicsShades, this.techShades});

  @override
  _DiscoverState createState() => _DiscoverState();
}

class _DiscoverState extends State<Discover> {
  List<String> dropdownList = ['Shade', 'Polls'];
  String dropdownValue = 'Shade';

  @override
  void initState() {
    super.initState();
    if(widget.topShades.length == 0) {
      DiscoverService().getTopShades(dropdownValue).then((result) {
        setState(() {
          widget.topShades.clear();
          widget.topShades.addAll(result);
        });
      });
    }

      DiscoverService().getShadesFromTag('sports').then((result) {
        setState(() {
          widget.sportsShades.clear();
          widget.sportsShades.addAll(result);
        });
      });

      DiscoverService().getShadesFromTag('politics').then((result) {
        setState(() {
          widget.politicsShades.clear();
          widget.politicsShades.addAll(result);
        });
      });

      DiscoverService().getShadesFromTag('tech').then((result) {
        setState(() {
          widget.techShades.clear();
          widget.techShades.addAll(result);
        });
      });
  }

  Future refresh() async {
    List<Shade> topShades = await DiscoverService().getTopShades(dropdownValue);
    List<Shade> sportsTag = await DiscoverService().getShadesFromTag('sports');
    List<Shade> politicsTag = await DiscoverService().getShadesFromTag('politics');
    List<Shade> techTag = await DiscoverService().getShadesFromTag('tech');

    setState(() {
      widget.topShades.clear();
      widget.topShades.addAll(topShades);
      widget.sportsShades.clear();
      widget.sportsShades.addAll(sportsTag);
      widget.politicsShades.clear();
      widget.politicsShades.addAll(politicsTag);
      widget.techShades.clear();
      widget.techShades.addAll(techTag);
    });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        color: grey1,
        child: LiquidPullToRefresh(
          animSpeedFactor: 2,
          onRefresh: () async {
            await refresh();
          },
          backgroundColor: grey1,
          color: aquamarine,
          showChildOpacityTransition: false,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.builder(
              itemCount: 16,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                if(index % 2 == 0 && index != 0 && index != 2) return Divider(color: aquamarine, thickness: 2);
                if(index == 1) return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: DropdownButton(
                    dropdownColor: grey2,
                    value: dropdownValue,
                    underline: SizedBox(),
                    items: dropdownList.map((String dropDownStringItem) {
                      return DropdownMenuItem<String>(
                        value: dropDownStringItem,
                        child: Text(
                          dropDownStringItem,
                          style: grey5BoldTextStyle.copyWith(fontSize: 12),
                        ),
                      );
                    }).toList(),
                    onChanged: (value) async {
                      await DiscoverService().getTopShades(value).then((result) {
                        setState(() {
                          dropdownValue = value;
                          widget.topShades.clear();
                          widget.topShades.addAll(result);
                        });
                      });
                    },
                  ),
                );
                if(index == 3) return SizedBox(
                  height: 250,
                    child: widget.topShades.length != 0 ? ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: widget.topShades.length,
                      itemBuilder: (BuildContext context, int index) {
                        return VerticalShadeCard(
                          shade: widget.topShades[index],
                          widget: ShadeProfilePage(
                            shadowUID: widget.shadowUID,
                            shade: widget.topShades[index],
                          ),
                        );
                      },
                    )
                    : Loading(),
                );
                if(index == 5) return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Sports', style: whiteBoldTextStyle),
                );
                if(index == 7) return SizedBox(
                  height: 250,
                  child: widget.topShades.length != 0 ? ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: widget.sportsShades.length,
                    itemBuilder: (BuildContext context, int index) {
                      return VerticalShadeCard(
                        shade: widget.sportsShades[index],
                        widget: ShadeProfilePage(
                          shadowUID: widget.shadowUID,
                          shade: widget.sportsShades[index],
                        ),
                      );
                    },
                  )
                      : Loading(),
                );

                if(index == 9) return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Politics', style: whiteBoldTextStyle.copyWith(fontSize: 18)),
                );
                if(index == 11) return SizedBox(
                  height: 250,
                  child: widget.politicsShades.length != 0 ? ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: widget.politicsShades.length,
                    itemBuilder: (BuildContext context, int index) {
                      return VerticalShadeCard(
                        shade: widget.politicsShades[index],
                        widget: ShadeProfilePage(
                          shadowUID: widget.shadowUID,
                          shade: widget.politicsShades[index],
                        ),
                      );
                    },
                  )
                      : Loading(),
                );
                if(index == 13) return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Tech', style: whiteBoldTextStyle),
                );
                if(index == 15) return SizedBox(
                  height: 250,
                  child: widget.techShades.length != 0 ? ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: widget.techShades.length,
                    itemBuilder: (BuildContext context, int index) {
                      return VerticalShadeCard(
                        shade: widget.techShades[index],
                        widget: ShadeProfilePage(
                          shadowUID: widget.shadowUID,
                          shade: widget.techShades[index],
                        ),
                      );
                    },
                  )
                      : Loading(),
                );
                return Container();
              },
            ),
          ),
        )
    );
  }
}

class VerticalShadeCard extends StatefulWidget {
  final Shade shade;
  final Widget widget;
  VerticalShadeCard({this.shade, this.widget});

  @override
  _VerticalShadeCardState createState() => _VerticalShadeCardState();
}

class _VerticalShadeCardState extends State<VerticalShadeCard> {
  List url;

  @override
  void initState() {
    super.initState();
    CloudStorageService().getURL(widget.shade.imagePath).then((value) {
      setState(() {
        url = value;
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => widget.widget
          ),
        );
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
              color: grey2,
              borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          width: 150,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 64,
                  width: 64,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    child: url != null ? Image(image:
                    NetworkToFileImage(
                        url: url[1],
                        file: File(url[0]),
                        debug: true)
                    ) : Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(aquamarine)),
                    ),
                  ),
                ),
                Flexible(
                  child: Text(
                    widget.shade.name.toUpperCase(),
                    style: aquamarineBoldTextStyle.copyWith(fontSize: 18),
                  ),
                ),
                SizedBox(height: 32),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'polls',
                          style: whiteTitleTextStyle.copyWith(fontSize: 12),
                        ),
                        Text(
                          'shadows',
                          style: whiteTitleTextStyle.copyWith(fontSize: 12),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          widget.shade.numPolls.toString() ?? 0.toString(),
                          style: aquamarineBoldTextStyle.copyWith(fontSize: 16),
                        ),
                        Text(
                          widget.shade.numShadows.toString(),
                          style: aquamarineBoldTextStyle.copyWith(fontSize: 16),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(height: 16),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(text: 'view top ', style: whiteBoldTextStyle.copyWith(fontSize: 8)),
                      TextSpan(text: 'polls', style: aquamarineBoldTextStyle.copyWith(fontSize: 8))
                    ]
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

