import 'dart:io';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:network_to_file_image/network_to_file_image.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Authenticate/Authenticate.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadeModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Models/ShadowProfileModel.dart';
import 'package:shadow/Pages/HomePage/Discover.dart';
import 'package:shadow/Pages/HomePage/Following.dart';
import 'package:shadow/Pages/HomePage/PollPreview.dart';
import 'package:shadow/Pages/ProfilePages/ShadeProfilePage.dart';
import 'package:shadow/Services/CloudStorageService.dart';
import 'package:shadow/Services/DatabaseService.dart';
import 'package:shadow/Services/FirestoreService.dart';
import 'package:shadow/Services/SearchService.dart';
import 'package:shadow/SharedWidgets/HomePageElements.dart';
import 'package:shadow/SharedWidgets/PollMenu.dart';
import 'package:shadow/main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

const kExpandedHeight = 150.0;

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {

  static int lastRefresh = Shadow.initialRefresh ? DateTime.now().toUtc().millisecondsSinceEpoch : lastRefresh;
  static int currentNumShades = 0;

  ScrollController _scrollViewController;
  TabController _tabController;
  TextEditingController _textEditingController;

  bool isVisible = true;
  bool isMenuVisible = false;
  bool searchVisible = false;
  double opacity = 1.0;
  double yTranslate = 0.0;
  double animatedWidth = 0.0;
  String dropdownValue = 'Latest';
  List<String> dropdownList = ['Trending', 'Latest', 'Oldest', 'Most Votes'];
  
  List<Shade> topShades = [];
  List<Shade> sportsShades = [];
  List<Shade> politicsShades = [];
  List<Shade> techShades = [];

  PageController pageController =  PageController(
    initialPage: 2
  );

  List results = [];
  int currentResults = 0;
  int currentIndex;
  List<Poll> pollList = List<Poll>();

  Poll previewPoll = Poll();
  bool isPreviewPoll = false;

  FirebaseShadow shadow = FirebaseShadow();

  var timeAgo = (DateTime.now()).subtract(Duration(hours: 24)).toUtc().millisecondsSinceEpoch;

  @override
  void initState() {
    super.initState();
    Shadow.initialRefresh = true;
    _textEditingController = TextEditingController();
    _scrollViewController = ScrollController();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _scrollViewController.dispose();
    _tabController.dispose();
    super.dispose();
  }

  updateLastRefresh(int current) {
    setState(() {
      lastRefresh = current;
    });
  }

  storePolls(List current) {
    setState((){
      pollList = current;
    });
  }

  void _selectCard(int index) {
    setState(() {
      currentIndex = currentIndex == index ? -1 : index;
    });
  }

  void _openShadeProfilePage(Shade shade, String shadowUID) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => ShadeProfilePage(shadowUID: shadowUID, shade: shade)),
    );
  }

  void _updateResults(String value) async {
//    if(currentModifierIndex == 0) {
    List tempList = [];
    await SearchService()
        .searchShadeByName(value)
        .then((QuerySnapshot docs) async {
      for (int i = 0; i < docs.documents.length; i++) {
        List tempUrl = await CloudStorageService().getURL(
            docs.documents[i]['imagePath']);
//        String url = tempUrl[0];

        tempList.add(
            [
              Shade(
                tags: docs.documents[i]['tags'],
                uid: docs.documents[i]['uid'],
                authorUsername: docs.documents[i]['authorUsername'],
                access: docs.documents[i]['access'],
                name: docs.documents[i]['name'],
                description: docs.documents[i]['description'],
                imagePath: docs.documents[i]['imagePath'],
                guidelines: docs.documents[i]['guidelines'],
                shadowList: docs.documents[i]['shadowList'],
                pollList: docs.documents[i]['pollList'],
                pendingShadows: docs.documents[i]['pendingShadows'],
                adminList:  docs.documents[i]['adminList'],
                modifiers: docs.documents[i]['modifiers'],
                nameLowercase: docs.documents[i]['nameLowercase'],
                numShadows: docs.documents[i]['numShadows'],
                timestamp: DateTime.fromMillisecondsSinceEpoch(docs.documents[i]['timestamp']),
              ),
              Image(image:
              NetworkToFileImage(
                  url: tempUrl[1],
                  file: File(tempUrl[0]),
                  debug: true)
              )
            ]
        );
      }
    });

    int maxIndex;
    int max;
    for(int i = 0; i < tempList.length - 1; i++) {
      maxIndex = -1;
      max = -1;
      for(int j = i; j < tempList.length; j++) {
        if(tempList[j][0].numShadows > max) {
          maxIndex = j;
          max = tempList[j][0].numShadows;
        }
      }
      var temp = tempList[maxIndex];
      tempList[maxIndex] = tempList[i];
      tempList[i] = temp;
    }

    setState(() {
      results = tempList;
      currentResults = results.length < 2 ? results.length : 2;
    });
  }

  void graphPressed(Poll poll) {
    setState(() {
      previewPoll = poll;
      isPreviewPoll = true;
    });
  }

  void menuPressed() {
    setState(() {
      isMenuVisible = true;
    });
  }

  void closePreview() {
    setState(() {
      isPreviewPoll = false;
    });
  }

  void closeMenu() {
    setState(() {
      isMenuVisible = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final firebaseShadow = Provider.of<FirebaseShadow>(context);

    if (firebaseShadow == null) {
      return Authenticate();
    } else {
      void _refreshPolls() async {
        List<Poll> polls = List<Poll>();

        ShadowProfile profile = await FirestoreService().getShadow(firebaseShadow.uid);
        List userShadeList = profile.joinedShades;

        List<List<Poll>> receivedShades = [];
        for(String shadeID in userShadeList) {
          switch (dropdownValue) {
            case 'Trending' : {
//              receivedShades.add(await SearchService().getPollsByRecent(widget.timeAgo, widget.firebaseShadow.uid, shadeID, true));
              break;
            }
            case 'Latest' : {
              if (await SearchService().morePollsAvailable(lastRefresh, shadeID)
                  || Shadow.initialRefresh || profile.joinedShades.length != currentNumShades) {
                receivedShades.add(await SearchService().getPollsByRecent(
                    timeAgo, shadeID, true));
              }

              break;
            }
            case 'Oldest' : {
              receivedShades.add(await SearchService().getPollsByRecent(timeAgo, shadeID, false));
              break;
            }
            case 'Most Votes' : {
//              receivedShades.add(await SearchService().getPollsByRecent(widget.timeAgo, widget.firebaseShadow.uid, shadeID, true));
              break;
            }
          }
        }

        for(var shade in receivedShades) {
          for(Poll poll in shade) {
            if(poll.shadowsVoted[firebaseShadow.uid] == null) {
              polls.add(poll);
            }
          }
        }

        switch (dropdownValue) {
          case 'Latest' : {
            polls.sort((a, b) => b.timestamp.compareTo(a.timestamp));
            break;
          }
          case 'Oldest' : {
            polls.sort((a, b) => a.timestamp.compareTo(b.timestamp));
            break;
          }
        }

        setState(() {
          if(polls.length != 0 || currentNumShades != profile.joinedShades.length) pollList = polls;
          if(Shadow.initialRefresh) Shadow.initialRefresh = false;
          currentNumShades = profile.joinedShades.length;
        });
      }

      if(Shadow.initialRefresh) {
        _refreshPolls();
      }

      return SafeArea(
        child: Stack(
          children: [
            Scaffold(
              backgroundColor: Colors.transparent,
              body: Column(
                children: [
                  Expanded(
                    flex: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: grey2,
                            borderRadius: BorderRadius.all(Radius.circular(5))
                        ),
                        child: Row(
                          children: [
                            Expanded(flex: 1,
                                child: IconButton(
                                    icon: Icon(Icons.search),
                                    color: grey4,
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    onPressed: () {
                                      setState(() {
                                        yTranslate = MediaQuery.of(context).size.height - 250;
                                        opacity = 0.0;
                                        animatedWidth = MediaQuery.of(context).size.width / 5;
                                        searchVisible = true;
                                      });
                                    }
                                )
                            ),
                            Expanded(
                              flex: 5,
                              child: TextFormField(
                                controller: _textEditingController,
                                onTap: () {
                                  setState(() {
                                    yTranslate = MediaQuery.of(context).size.height - 250;
                                    opacity = 0.0;
                                    animatedWidth = MediaQuery.of(context).size.width / 5;
                                    searchVisible = true;
                                  });
                                },
                                onChanged: (value) {
                                  value == '' ? currentIndex = -1 : results = [];
                                  results = [];
                                  _updateResults(value);
                                },
                                style: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                                decoration: InputDecoration(
                                  hintText: 'Search',
                                  hintStyle: TextStyle(color: grey4, fontFamily: 'Gilroy', fontWeight: FontWeight.bold),
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: AnimatedOpacity(
                                  duration: Duration(milliseconds: 250),
                                  opacity: 1.0 - opacity,
                                  child: IconButton(
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    icon: Icon(Icons.close),
                                    color: aquamarine,
                                    onPressed: () {
                                      setState(() {
                                        _textEditingController.clear();
                                        searchVisible = false;
                                        yTranslate = 0.0;
                                        opacity = 1.0;
                                        animatedWidth = 0.0;
                                      });
                                      FocusScope.of(context).requestFocus(FocusNode());
                                    },
                                  )
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: searchVisible,
                    child: Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                        child: ListView.builder(itemBuilder: (BuildContext context, int index){
                          if(index >= currentResults) {
                            currentResults = currentResults + 10 >= results.length
                                ? results.length
                                : currentResults + 10;
                            Future.delayed(Duration(seconds: 1));
                          }
                          if(index == results.length && results.length == 0) {
                            return Text("No Shades match your search", style: whiteNormalTextStyle,);
                          }
                          if(index >= results.length) {
                            return null;
                          }
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4.0),
                            child: ShadeCard(
                              shade: results[index][0],
                              shadowUID: firebaseShadow.uid,
                              image: results[index][1],
                              index: index,
                              onTap: _openShadeProfilePage, // _openShadeProfilePage // change to open shade page ------------------------
                              isSelected: currentIndex == index ? true : false,
                            ),
                          );
                        }),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: !searchVisible,
                    child: Expanded(
                      flex: 1,
                      child: AnimatedOpacity(
                        duration: Duration(milliseconds: 500),
                        opacity: opacity,
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 500),
                          transform: Matrix4.identity()
                            ..translate(0.0, yTranslate),
                          child: NestedScrollView(
                            controller: _scrollViewController,
                            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                              return <Widget>[
                                SliverAppBar(
                                  title: Container(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          flex: 0,
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(horizontal:8.0),
                                            child: DropdownButton(
                                              dropdownColor: grey2,
                                              value: dropdownValue,
                                              underline: SizedBox(),
                                              items: dropdownList.map((String dropDownStringItem) {
                                                return DropdownMenuItem<String>(
                                                  value: dropDownStringItem,
                                                  child: Text(
                                                    dropDownStringItem,
                                                    style: grey5BoldTextStyle.copyWith(fontSize: 12),
                                                  ),
                                                );
                                              }).toList(),
                                              onChanged: (String value) {
                                                setState(() {
                                                  dropdownValue = value;
                                                  _refreshPolls();
                                                });
                                              },
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                        Expanded(
                                          flex: 0,
                                          child: IconButton(
                                            icon: Icon(Icons.arrow_left, color: aquamarine),
                                            splashColor: Colors.transparent,
                                            highlightColor: Colors.transparent,
                                            onPressed: () {
                                              if(pageController.page.toInt() > 0) {
                                                pageController.previousPage(duration: Duration(milliseconds: 200), curve: Curves.easeInCubic);
                                              }
                                            },
                                          ),
                                        ),
                                        Expanded(
                                          flex: 0,
                                          child: Padding(
                                            padding: const EdgeInsets.only(top: 8.0),
                                            child: Container(
                                              height: 20,
                                              width: 52,
                                              child: Center(
                                                child: PageView(
                                                  controller: pageController,
                                                  children: [
                                                    Text('10 mins', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                                    Text('1 hour', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                                    Text('24 hours', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                                    Text('1 week', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                                    Text('1 month', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                                    Text('6 months', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                                    Text('1 year', textAlign: TextAlign.center, style: grey5BoldTextStyle.copyWith(fontSize: 12)),
                                                  ],
                                                  scrollDirection: Axis.horizontal,
                                                  onPageChanged: (index) {
                                                    switch(index) {
                                                      case 0: {
                                                        setState(() {
                                                          timeAgo = (DateTime.now()).subtract(Duration(minutes: 10)).toUtc().millisecondsSinceEpoch;
                                                        });
                                                        break;
                                                      }
                                                      case 1: {
                                                        setState(() {
                                                          timeAgo = (DateTime.now()).subtract(Duration(minutes: 60)).toUtc().millisecondsSinceEpoch;
                                                        });
                                                        break;
                                                      }
                                                      case 2: {
                                                        setState(() {
                                                          timeAgo = (DateTime.now()).subtract(Duration(days: 1)).toUtc().millisecondsSinceEpoch;
                                                        });
                                                        break;
                                                      }
                                                      case 3: {
                                                        setState(() {
                                                          timeAgo = (DateTime.now()).subtract(Duration(days: 7)).toUtc().millisecondsSinceEpoch;
                                                        });
                                                        break;
                                                      }
                                                      case 4: {
                                                        setState(() {
                                                          timeAgo = (DateTime.now()).subtract(Duration(days: 31)).toUtc().millisecondsSinceEpoch;
                                                        });
                                                        break;
                                                      }
                                                      case 5: {
                                                        setState(() {
                                                          timeAgo = (DateTime.now()).subtract(Duration(days: 126)).toUtc().millisecondsSinceEpoch;
                                                        });
                                                        break;
                                                      }
                                                      case 6: {
                                                        setState(() {
                                                          timeAgo = (DateTime.now()).subtract(Duration(days: 365)).toUtc().millisecondsSinceEpoch;
                                                        });
                                                        break;
                                                      }
                                                    }
                                                    setState(() {
                                                      _refreshPolls();
                                                    });
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 0,
                                          child: IconButton(
                                            icon: Icon(Icons.arrow_right, color: aquamarine),
                                            splashColor: Colors.transparent,
                                            highlightColor: Colors.transparent,
                                            onPressed: () {
                                              if(pageController.page.toInt() < 5) {
                                                pageController.nextPage(duration: Duration(milliseconds: 200), curve: Curves.easeInCubic);
                                              }
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  backgroundColor: grey1,
                                  pinned: true,
                                  floating: true,
                                  automaticallyImplyLeading: false,
                                  bottom: TabBar(
                                    tabs: <Tab>[
                                      Tab(text: "Following"),
                                      Tab(text: "Discover"),
                                    ],
                                    indicatorColor: aquamarine,
                                    labelColor: aquamarine,
                                    labelStyle: aquamarineBoldTextStyle,
                                    unselectedLabelColor: white,
                                    unselectedLabelStyle: whiteBoldTextStyle,
                                    controller: _tabController,
                                  ),
                                ),
                              ];
                            },
                            body: TabBarView(
                              physics: NeverScrollableScrollPhysics(),
                              children: <Widget>[
                                Following(
                                  updateLastRefresh: updateLastRefresh,
                                  refreshPolls: _refreshPolls,
                                  currentPolls: pollList,
                                  storePolls: storePolls,
                                  firebaseShadow: firebaseShadow,
                                  timeAgo: timeAgo,
                                  dropdownValue: dropdownValue,
                                  graphPressed: graphPressed,
                                  closePreview: closePreview,
                                  menuPressed: menuPressed,
                                ),
                                Discover(
                                    shadowUID: firebaseShadow.uid.toString(),
                                    topShades: topShades,
                                    sportsShades: sportsShades,
                                    politicsShades: politicsShades,
                                    techShades: techShades
                                ),
                              ],
                              controller: _tabController,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
  }
//  }
}