import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/PollCards/PollCardDead.dart';
import 'package:shadow/SharedWidgets/HomePageElements.dart';

import '../../Constants/DesignConstants.dart';



// ignore: must_be_immutable
class Following extends StatelessWidget {
  final FirebaseShadow firebaseShadow;
  final int timeAgo;
  final String dropdownValue;
  final Function storePolls;
  final List<Poll> currentPolls;
  final Function refreshPolls;
  final Poll previewPoll;
  final bool isPreviewPoll;
  final Function graphPressed;
  final Function closePreview;
  final Function menuPressed;
  final Function updateLastRefresh;

  Following({this.firebaseShadow, this.timeAgo, this.dropdownValue, this.storePolls, this.currentPolls, this.refreshPolls, this.previewPoll, this.isPreviewPoll, this.graphPressed, this.closePreview, this.menuPressed, this.updateLastRefresh});

  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: grey1,
      child: LiquidPullToRefresh(
        animSpeedFactor: 2,
        onRefresh: () async {
          await refreshPolls();
          await updateLastRefresh(DateTime.now().toUtc().millisecondsSinceEpoch,);
        },
        backgroundColor: grey1,
        color: aquamarine,
        showChildOpacityTransition: false,
        child: ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
          // ignore: missing_return
          itemBuilder: (BuildContext context, int i){
            if(i >= currentIndex) {
              currentIndex + 10 < currentPolls.length ? currentIndex += 10 : currentIndex = currentPolls.length;
            }
            if(i == currentPolls.length) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "No more polls available, join more shades to unlock more or " +
                      "refresh to find more!",
                      textAlign: TextAlign.center,
                      style: grey4BoldTextStyle.copyWith(fontSize: 12),
                    ),
                  ),
                ),
              );
            }
            if(i > currentPolls.length) {
              return null;
            }
            return Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 16, 8, 0),
              child: PollCardDead(index: i, currentPolls: currentPolls, poll: currentPolls[i], shadowUID: firebaseShadow.uid, menuPressed: menuPressed),
//              child: !currentPolls[i].modifiers[0]
//                  ? PollCard(shadowUID: firebaseShadow.uid, poll: currentPolls[i], menuPressed: menuPressed)
//                  : PollCardGraph(shadowUID: firebaseShadow.uid, poll: currentPolls[i], graphPressed: graphPressed),
            );
          }
        ),
      ),
    );
  }
}
