import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Constants/DesignConstants.dart';
import 'package:shadow/Models/OptionModel.dart';
import 'package:shadow/Models/PollModel.dart';
import 'package:shadow/SharedWidgets/FormElements.dart';
import 'package:shadow/SharedWidgets/GraphWidgets/MiniGraph.dart';

class PollPreview extends StatefulWidget {
  final Poll previewPoll;
  final Function closePreview;

  PollPreview({this.previewPoll, this.closePreview});

  @override
  _PollPreviewState createState() => _PollPreviewState();
}

class _PollPreviewState extends State<PollPreview> {
  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              SizedBox(width: 32),
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                  icon: Icon(Icons.arrow_back, color: white),
                  onPressed: () => widget.closePreview(),
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(width: 48),
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      widget.previewPoll.name.toString().toUpperCase(),
                      style: whiteBoldTextStyle.copyWith(fontSize: 20),
                    ),
                    SizedBox(height: 8),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(text: 'shadow[', style: whiteBoldTextStyle.copyWith(fontSize: 18)),
                              TextSpan(text: widget.previewPoll.authorUsername.toUpperCase(), style: aquamarineBoldTextStyle.copyWith(fontSize: 18)),
                              TextSpan(text: ']', style: whiteBoldTextStyle.copyWith(fontSize: 18)),
                            ]
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Row(
                      children: [
                        Text(
                          'VOTES',
                          style: whiteBoldTextStyle.copyWith(fontSize: 16),
                        ),
                        Spacer(),
                        Text(
                          widget.previewPoll.voteCount.toString(),
                          style: aquamarineBoldTextStyle.copyWith(fontSize: 32),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(width: 48)
            ],
          ),
          SizedBox(height: 32),
          Center(child: MiniGraph(height: 100.0, width: 200.0, optionsList: widget.previewPoll.options, isColored: true)),
          SizedBox(height: 64),
          Row(
            children: [
              SizedBox(width: 48),
              Expanded(flex: 1, child: OptionDisplay(options: widget.previewPoll.options)),
              SizedBox(width: 48),
            ],
          )
        ],
      ),
    );
  }
}

class OptionDisplay extends StatefulWidget {
  final List options;
  final int optionSelected;
  OptionDisplay({this.options, this.optionSelected});

  @override
  _OptionDisplayState createState() => _OptionDisplayState();
}

class _OptionDisplayState extends State<OptionDisplay> {
  int optionNameIndex = 0;

  @override
  Widget build(BuildContext context) {
    Color color;

    switch (optionNameIndex) {
      case 0: {
        color = ColorChooser.graphColors[int.parse(widget.options[0]['color'])];
        break;
      }
      case 1: {
        color =  ColorChooser.graphColors[int.parse(widget.options[1]['color'])];
        break;
      }
      case 2: {
        color =  ColorChooser.graphColors[int.parse(widget.options[2]['color'])];
        break;
      }
      case 3: {
        color =  ColorChooser.graphColors[int.parse(widget.options[3]['color'])];
        break;
      }
      case 4: {
        color = ColorChooser.graphColors[int.parse(widget.options[4]['color'])];
        break;
      }
      default:
        color = aquamarine;
        break;
    }

    widget.optionSelected != null ? optionNameIndex = widget.optionSelected : 0;

    List notAllowed = ['Enter Option 3', 'Enter Option 4', 'Enter Option 5'];

    List tempList = List();

    for(var option in widget.options) {
      if(!notAllowed.contains(option['option'])) tempList.add((option['votes']));
    }

    return Container(
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(10))
      ),
      child: Padding(
        padding: const EdgeInsets.all(32),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  color: optionNameIndex == 0 ? white : Colors.transparent,
                  width: 25,
                  height: 25,
                  child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: MaterialButton(
                      color: ColorChooser.graphColors[int.parse(widget.options[0]['color'])],
                      onPressed: () {
                        setState(() {
                          optionNameIndex = 0;
                        });
                      },
                    ),
                  ),
                ),
                Container(
                  color: optionNameIndex == 1 ? white : Colors.transparent,
                  width: 25,
                  height: 25,
                  child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: MaterialButton(
                      color: ColorChooser.graphColors[int.parse(widget.options[1]['color'])],
                      onPressed: () {
                        setState(() {
                          optionNameIndex = 1;
                        });
                      },
                    ),
                  ),
                ),
                if(tempList.length == 3) Container(
                  color: optionNameIndex == 2 ? white : Colors.transparent,
                  width: 25,
                  height: 25,
                  child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: MaterialButton(
                      color: ColorChooser.graphColors[int.parse(widget.options[2]['color'])],
                      onPressed: () {
                        setState(() {
                          optionNameIndex = 2;
                        });
                      },
                    ),
                  ),
                ),
                if(tempList.length == 4) Container(
                  color: optionNameIndex == 3 ? white : Colors.transparent,
                  width: 25,
                  height: 25,
                  child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: MaterialButton(
                      color: ColorChooser.graphColors[int.parse(widget.options[3]['color'])],
                      onPressed: () {
                        setState(() {
                          optionNameIndex = 3;
                        });
                      },
                    ),
                  ),
                ),
                if(tempList.length == 5) Container(
                  color: optionNameIndex == 4 ? white : Colors.transparent,
                  width: 25,
                  height: 25,
                  child: Padding(
                    padding: const EdgeInsets.all(1),
                    child: MaterialButton(
                      color: ColorChooser.graphColors[int.parse(widget.options[4]['color'])],
                      onPressed: () {
                        setState(() {
                          optionNameIndex = 4;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 32),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      widget.options[optionNameIndex]['option'].toString(),
                      style: TextStyle(
                        color: color,
                        fontWeight: FontWeight.bold,
                        fontSize: 32
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 2.0),
                      child: Text(
                        widget.options[optionNameIndex]['description'].toString(),
                        style: whiteBoldTextStyle.copyWith(fontSize: 18),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
