import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:confetti/confetti.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/SharedWidgets/CircleProgressBar.dart';
import 'package:shadow/SharedWidgets/Confetti.dart';

import '../main.dart';

class StorageUploader extends StatefulWidget {
  final File file;
  final List values;
  final Function updateColors;

  StorageUploader({this.file, this.values, this.updateColors});

  @override
  _StorageUploaderState createState() => _StorageUploaderState();
}

class _StorageUploaderState extends State<StorageUploader> {
  final FirebaseStorage storage = FirebaseStorage(storageBucket: 'gs://shadow-aef13.appspot.com');
  var uploads = 0;
  var opacity = 0.0;
  var textOpacity = 1.0;
  StorageUploadTask uploadTask;

  void startUpload() {
    uploads += 1;
    if(uploads == 1) {
      String filePath = 'shades/${widget.values[7].toString().trim()}.png';

      setState(() {
        uploadTask = storage.ref().child(filePath).putFile(widget.file);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final scaffoldKey = new GlobalKey<ScaffoldState>();
    startUpload();

    return StreamBuilder<StorageTaskEvent>(
      stream: uploadTask.events,
      builder: (context, snapshot) {
        var event = snapshot?.data?.snapshot;

        double progressPercent = event != null
            ? event.bytesTransferred / event.totalByteCount
            : 0;

        if(progressPercent * 100 == 100.00) {
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              opacity = 1.0;
              textOpacity = 0.0;
            });
          });
        }

        return Container(
          child: Stack(
            children: [
              Center(
                child: Container(
                  width: MediaQuery.of(context).size.width - 64,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleProgressBar(
                        foregroundColor: aquamarine,
                        backgroundColor: grey3,
                        value: progressPercent,
                      )
                    ],
                  ),
                ),
              ),
              Opacity(
                opacity: opacity,
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width - 64,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                RichText(
                                  text: TextSpan(
                                    children: [
                                      TextSpan(text: 'shade[', style: TextStyle(
                                        color: white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                      )),
                                      TextSpan(text: widget.values[1], style: TextStyle(
                                        color: aquamarine,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                      )),
                                      TextSpan(text: ']', style: TextStyle(
                                        color: white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold
                                      )),
                                    ]
                                  ),
                                ),
                                Spacer(),
                                Text(
                                  widget.values[2].toString().toUpperCase(),
                                  style: TextStyle(
                                    color: aquamarine,
                                    fontWeight: FontWeight.bold
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 8),
                            Container(
                              width: MediaQuery.of(context).size.width - 64,
                              decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.all(Radius.circular(10))
                              ),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Text(
                                          widget.values[3],
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: grey4,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 16),
                                      Container(
                                        height: 100,
                                        width: 100,
                                        child: ClipRRect(
                                            borderRadius: BorderRadius.all(Radius.circular(10)),
                                            child: Image.file(widget.values[4])
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 16),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Guidelines',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        color: aquamarine,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      widget.values[6],
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        color: grey3,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 16),
                                  Container(
                                    width: MediaQuery.of(context).size.width - 64,
                                    child: Row(
                                      children: [
                                        FlatButton.icon(
                                          icon: Icon(Icons.add, color: aquamarine, size: 35),
                                          label: Text(
                                            'Create Another Shade',
                                            style: TextStyle(
                                                color: white,
                                                fontWeight: FontWeight.bold
                                            ),
                                          ),
                                          onPressed: () {
                                            widget.updateColors([white, white, aquamarine, white, white]);
                                            Navigator.of(context).pushNamedAndRemoveUntil('/creationPage', (route) => false);
                                          },
                                        ),
                                        Spacer(),
                                        IconButton(
                                          icon: Icon(Icons.home, color: aquamarine),
                                          onPressed: () {
                                            widget.updateColors([aquamarine, white, white, white, white]);
                                            Navigator.of(context).pushNamedAndRemoveUntil('/', (route) => false);
                                          },
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              if(progressPercent * 100 == 100.00) AnimatedOpacity(
                opacity: textOpacity,
                duration: Duration(seconds: 2),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: Text('Shade Created Successfully', style: TextStyle(
                        color: aquamarine,
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                    )
                    ),
                  ),
                ),
              ),
              if(context != null && progressPercent * 100 == 100.00) Confetti()
            ],
          ),
        );
      },
    );
  }
}
