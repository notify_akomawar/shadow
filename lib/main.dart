import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Constants/ColorConstants.dart';
import 'package:shadow/Models/ShadowModel.dart';
import 'package:shadow/Pages/CreationPage/CreationPage.dart';
import 'package:shadow/Pages/HomePage/HomePage.dart';
import 'package:shadow/Pages/LivePollPage/LivePollPage.dart';
import 'package:shadow/Pages/PrivatePage/PrivatePage.dart';
import 'package:shadow/Pages/SettingsPage/SettingsPage.dart';
import 'package:shadow/Services/AuthService.dart';
import 'package:shadow/Services/FirestoreService.dart';

import 'Models/ShadeModel.dart';

void main() => runApp(Shadow());



class Shadow extends StatelessWidget {

  static bool initialRefresh = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'shadow',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: 'Gilroy',
      ),
      home: ShadowStateful(),
    );
  }
}

class ShadowStateful extends StatefulWidget {

  @override
  _ShadowStatefulState createState() => _ShadowStatefulState();
}

class _ShadowStatefulState extends State<ShadowStateful> {

  List iconColors = [aquamarine, white, white, white, white];

  final _navigatorKey = GlobalKey<NavigatorState>();

  void _updateColors(List colors) {
    setState(() {
      iconColors = colors;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: grey1,
      body: Stack(
        children: [
          Navigator(
            key: _navigatorKey,
            initialRoute: '/',
            onGenerateRoute: (RouteSettings settings) {
              Widget widget;
              switch (settings.name) {
                case '/':
                  widget = HomeProvider();
                  break;
                case '/chats':
                  widget = ChatProvider();
                  break;
                case '/creationPage':
                  widget = CreatePollProvider(updateColors: _updateColors);
                  break;
                case '/livePollPage':
                  widget = LivePollProvider();
                  break;
                case '/settingsPage':
                  widget = SettingsProvider();
                  break;
                default:
                  throw Exception('Invalid route: ${settings.name}');
              }
              return PageRouteBuilder(
                settings: settings,
                pageBuilder: (context, animation, secondaryAnimation) {
                  return widget;
                },
                transitionDuration: Duration(milliseconds: 50),
                transitionsBuilder: (context, animation, secondaryAnimations, child) => FadeTransition(opacity: animation, child: child),
              );
            },
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: grey1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                icon: Icon(Icons.home),
                onPressed: () {
                  _navigatorKey.currentState.pushNamed('/');
                  setState(() => iconColors = [aquamarine, white, white, white, white]);
                },
                color: iconColors[0]
            ),
            IconButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(Icons.chat_bubble),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/chats');
                setState(() => iconColors = [white, aquamarine, white, white, white]);
              },
              color: iconColors[1],
            ),
            IconButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(Icons.add),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/creationPage');
                setState(() => iconColors = [white, white, aquamarine, white, white]);
              },
              color: iconColors[2],
            ),
            IconButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(Icons.person),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/livePollPage');
                setState(() => iconColors = [white, white, white, aquamarine, white]);
              },
              color: iconColors[3],
            ),
            IconButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(Icons.settings),
              onPressed: () {
                _navigatorKey.currentState.pushNamed('/settingsPage');
                setState(() => iconColors = [white, white, white, white, aquamarine]);
              },
              color: iconColors[4],
            ),
          ],
        ),
      ),
    );
  }
}

class HomeProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return StreamProvider<FirebaseShadow>.value(
      value: AuthService().firebaseShadow,
      child: HomePage()
    );
  }
}

class ChatProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      child: PrivatePage(),
      providers: [
        StreamProvider<FirebaseShadow>.value(
          value: AuthService().firebaseShadow,
        ),
        StreamProvider<List<Shade>>.value(
          value: FirestoreService().shades,
        ),
//          StreamProvider<List<Shadow>.value(
//            value: FirestoreService().shadows,
//          ),
//          StreamProvider<List<Poll>>.value(
//            value: FirestoreService().polls,
//          ),
      ]
    );
//    return StreamProvider<FirebaseShadow>.value(
//      value: AuthService().firebaseShadow,
//      child: PrivatePage()
//    );
  }
}

class CreatePollProvider extends StatelessWidget {

  final Function updateColors;

  const CreatePollProvider({Key key, this.updateColors}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return StreamProvider<FirebaseShadow>.value(
        value: AuthService().firebaseShadow,
        child: CreationPage(updateColors: updateColors)
    );
  }
}


class SettingsProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return StreamProvider<FirebaseShadow>.value(
        value: AuthService().firebaseShadow,
        child: SettingsPage()
    );
  }
}

class LivePollProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
        child: LivePollPage(),
        providers: [
          StreamProvider<FirebaseShadow>.value(
            value: AuthService().firebaseShadow,
          ),
          StreamProvider<List<Shade>>.value(
            value: FirestoreService().shades,
          ),
        ]
    );
  }
}



