import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Pages/DiscussionPage/ChatRoomPage.dart';
import 'package:shadow/Services/ChatService.dart';
import '../Constants/ColorConstants.dart';
import '../Constants/ColorConstants.dart';
import '../Constants/DesignConstants.dart';
import '../Constants/DesignConstants.dart';
import '../Constants/DesignConstants.dart';
import '../Models/PollModel.dart';
import '../Pages/DiscussionPage/DiscussionPage.dart';
import '../Pages/VotePage/VotePage.dart';
import '../Services/FirestoreService.dart';

class PollCardAdmin extends StatefulWidget {
  final Poll poll;
  final String shadowUID;
  final Function menuPressed;
  final List<Poll> currentPolls;
  final int index;

  PollCardAdmin({this.poll, this.shadowUID, this.menuPressed, this.currentPolls, this.index});

  @override
  _PollCardAdminState createState() => _PollCardAdminState();
}

class _PollCardAdminState extends State<PollCardAdmin> {

  int vote;
  double translate = 0.0;
  bool menuIsVisible = false;

  @override
  void initState() {
    super.initState();
    if(widget.poll.shadowsRated[widget.shadowUID] == null) vote = 0;
    else if(widget.poll.shadowsRated[widget.shadowUID]) vote = 1;
    else vote = -1;

  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
//      onDoubleTap: (){
//        Navigator.push(context, PageRouteBuilder(
//          pageBuilder: (context, animation, secondaryAnimation) {
//            return ChatRoomPage(poll: widget.poll, shadowUID: widget.shadowUID);
//          },
//          transitionDuration: Duration(milliseconds: 50),
//          transitionsBuilder: (context, animation, secondaryAnimations, child) => FadeTransition(opacity: animation, child: child),
//        ));
//      },

      child: Container(
        decoration: BoxDecoration(
            color: grey2,
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(child: Text(widget.poll.name.toUpperCase(), style: aquamarineBoldTextStyle.copyWith(fontSize: 18))),
                      InkWell(
                        onTap: () async {
                          await FirestoreService().deletePoll(widget.poll.shadeUID, widget.poll.uid);
                          await FirestoreService().decrementNumPolls(widget.poll.shadeUID);
                        },
                        child: Icon(Icons.delete, color: Colors.redAccent),
                      )
                    ],
                  ),
                  SizedBox(height: 2),
                  Flexible(
                    child: Text(widget.poll.description, style: grey4BoldTextStyle.copyWith(fontSize: 14)),
                  ),
                  SizedBox(height: 16.0),
                  Divider(color: grey3.withOpacity(0.2), thickness: 2,),
                  Row(
                    children: [
                      Text(widget.poll.voteCount.toString() + ' ', textAlign: TextAlign.center, style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
                      Text('votes', style: grey5BoldTextStyle.copyWith(fontSize: 14)),
                      Spacer(),
                      Text(widget.poll.rating.toString() + ' ', textAlign: TextAlign.center, style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
                      Text('light', style: grey5BoldTextStyle.copyWith(fontSize: 14)),
                    ],
                  ),
                  Row(
                    children: [
                      Text('shade[' + widget.poll.shadeName.toString().toLowerCase() + ']', style: whiteNormalTextStyle.copyWith(fontSize: 12)),
                      Spacer(),
                      Text('shadow[' + widget.poll.authorUsername.toLowerCase() + ']', style: aquamarineNormalTextStyle.copyWith(fontSize: 12))
                    ],
                  ),
                ],
              ),
            ),
            Visibility(
              visible: menuIsVisible,
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text('save to favourites', style: whiteBoldTextStyle.copyWith(fontSize: 18)),
                          Spacer(),
                          Icon(Icons.bookmark, color: aquamarine)
                        ],
                      ),
                      Row(
                        children: [
                          Text('share', style: whiteBoldTextStyle.copyWith(fontSize: 18)),
                          Spacer(),
                          Icon(Icons.share, color: aquamarine)
                        ],
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
