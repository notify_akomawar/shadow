import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shadow/Pages/DiscussionPage/ChatRoomPage.dart';
import 'package:shadow/Services/ChatService.dart';
import '../Constants/ColorConstants.dart';
import '../Constants/ColorConstants.dart';
import '../Constants/DesignConstants.dart';
import '../Constants/DesignConstants.dart';
import '../Constants/DesignConstants.dart';
import '../Models/PollModel.dart';
import '../Pages/DiscussionPage/DiscussionPage.dart';
import '../Pages/VotePage/VotePage.dart';
import '../Services/FirestoreService.dart';

class PollCardAliveTemp extends StatefulWidget {
  final Poll poll;
  final String shadowUID;

  PollCardAliveTemp({this.poll, this.shadowUID,});

  @override
  _PollCardAliveTempState createState() => _PollCardAliveTempState();
}

class _PollCardAliveTempState extends State<PollCardAliveTemp> {

  int vote;
  double translate = 0.0;
  bool menuIsVisible = false;

  @override
  void initState() {
    super.initState();
    if(widget.poll.shadowsRated[widget.shadowUID] == null) vote = 0;
    else if(widget.poll.shadowsRated[widget.shadowUID]) vote = 1;
    else vote = -1;

  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) {
            return ChatRoomPage(poll: widget.poll, shadowUID: widget.shadowUID);
          },
          transitionDuration: Duration(milliseconds: 50),
          transitionsBuilder: (context, animation, secondaryAnimations, child) => FadeTransition(opacity: animation, child: child),
        ));
      },
//      onLongPress: () {
//        setState(() {
//          menuIsVisible = !menuIsVisible;
//        });
//      },
//      onTap: () {
//        int numOptions = 2;
//        if(widget.poll.options[2]['option'] != 'Enter Option 3') numOptions++;
//        if(widget.poll.options[3]['option'] != 'Enter Option 4') numOptions++;
//        if(widget.poll.options[4]['option'] != 'Enter Option 5') numOptions++;
//        Navigator.push(
//          context,
//          MaterialPageRoute(
//              builder: (context) => VotePage(
//                index: widget.index,
//                currentPolls: widget.currentPolls,
//                poll: widget.poll,
//                numOptions: numOptions,
//                shadowUID: widget.shadowUID,
//              )
//          ),
//        );
//      },

      onHorizontalDragUpdate: (details){
        setState(() {
          translate += details.delta.dx;
        });
      },

      onHorizontalDragEnd: (details) async {
        if(translate < -50) { //dislike
          if(widget.poll.shadowsRated[widget.shadowUID] != null
              && widget.poll.shadowsRated[widget.shadowUID]) {
            await FirestoreService().updateRating(
                Firestore.instance
                    .collection('shades')
                    .document(widget.poll.shadeUID)
                    .collection('polls')
                    .document(widget.poll.uid),
                widget.shadowUID,
                widget.poll.rating, -2);

            setState(() {
              widget.poll.rating -= 2;
              vote = -1;
              widget.poll.shadowsRated[widget.shadowUID] = false;
            });
          }
          else if(widget.poll.shadowsRated[widget.shadowUID] != null
              && !widget.poll.shadowsRated[widget.shadowUID]) {
            await FirestoreService().updateRating(
                Firestore.instance
                    .collection('shades')
                    .document(widget.poll.shadeUID)
                    .collection('polls')
                    .document(widget.poll.uid),
                widget.shadowUID,
                widget.poll.rating, 1, reset: true);

            setState(() {
              widget.poll.rating += 1;
              vote = 0;
              widget.poll.shadowsRated.removeWhere((key, value) => (key == widget.shadowUID));
            });
          }
          else if(widget.poll.shadowsRated[widget.shadowUID] == null) {
            await FirestoreService().updateRating(
                Firestore.instance
                    .collection('shades')
                    .document(widget.poll.shadeUID)
                    .collection('polls')
                    .document(widget.poll.uid),
                widget.shadowUID,
                widget.poll.rating, -1);

            setState(() {
              widget.poll.rating -= 1;
              vote = -1;
              widget.poll.shadowsRated[widget.shadowUID] = false;
            });
          }
        }

        if(translate > 50) { //like
          if(widget.poll.shadowsRated[widget.shadowUID] != null
              && !widget.poll.shadowsRated[widget.shadowUID]) {
            await FirestoreService().updateRating(
                Firestore.instance
                    .collection('shades')
                    .document(widget.poll.shadeUID)
                    .collection('polls')
                    .document(widget.poll.uid),
                widget.shadowUID,
                widget.poll.rating, 2);

            setState(() {
              widget.poll.rating += 2;
              vote = 1;
              widget.poll.shadowsRated[widget.shadowUID] = true;
            });

          }
          else if(widget.poll.shadowsRated[widget.shadowUID] != null
              && widget.poll.shadowsRated[widget.shadowUID]) {
            await FirestoreService().updateRating(
                Firestore.instance
                    .collection('shades')
                    .document(widget.poll.shadeUID)
                    .collection('polls')
                    .document(widget.poll.uid),
                widget.shadowUID,
                widget.poll.rating, -1, reset: true);

            setState(() {
              widget.poll.rating -= 1;
              vote = 0;
              widget.poll.shadowsRated.removeWhere((key, value) => (key == widget.shadowUID));
            });
          }
          else if(widget.poll.shadowsRated[widget.shadowUID] == null) {
            await FirestoreService().updateRating(
                Firestore.instance
                    .collection('shades')
                    .document(widget.poll.shadeUID)
                    .collection('polls')
                    .document(widget.poll.uid),
                widget.shadowUID,
                widget.poll.rating, 1);

            setState(() {
              widget.poll.rating += 1;
              vote = 1;
              widget.poll.shadowsRated[widget.shadowUID] = true;
            });
          }
        }

        setState(() {
          translate = 0;
        });
      },

      child: Container(
        decoration: BoxDecoration(
            color: grey2,
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                    child: Text(
                        widget.poll.name.toUpperCase(),
                        style: aquamarineBoldTextStyle.copyWith(fontSize: 18)
                    ),
//                    child: Row(
//                      children: [
//
//                        Spacer(),
//                        GestureDetector(
//                            onTap: () {
//                              menuIsVisible = !menuIsVisible;
//                            },
//                            child: Icon(Icons.more_vert, color: aquamarine, size: 18)
//                        )
//                      ],
//                    ),
                  ),
                  SizedBox(height: 2),
                  Flexible(
                    child: Text(widget.poll.description, style: grey4BoldTextStyle.copyWith(fontSize: 14)),
                  ),
                  SizedBox(height: 16.0),
                  Divider(color: grey3.withOpacity(0.2), thickness: 2,),
                  Row(
                    children: [
                      Text(widget.poll.voteCount.toString() + ' ', textAlign: TextAlign.center, style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
                      Text('votes', style: grey5BoldTextStyle.copyWith(fontSize: 14)),
                      Spacer(),
                      Text(widget.poll.rating.toString() + ' ', textAlign: TextAlign.center, style: aquamarineBoldTextStyle.copyWith(fontSize: 14)),
                      Text('light', style: grey5BoldTextStyle.copyWith(fontSize: 14)),
                    ],
                  ),
                  Row(
                    children: [
                      Text('shade[' + widget.poll.shadeName.toString().toLowerCase() + ']', style: whiteNormalTextStyle.copyWith(fontSize: 12)),
                      Spacer(),
                      Text('shadow[' + widget.poll.authorUsername.toLowerCase() + ']', style: aquamarineNormalTextStyle.copyWith(fontSize: 12))
                    ],
                  ),
//            Row(
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: [
//                Text(widget.poll.voteCount.toString() + ' ', textAlign: TextAlign.center, style: aquamarineBoldTextStyle.copyWith(fontSize: 16)),
//                Text('votes', style: grey5BoldTextStyle.copyWith(fontSize: 14)),
//                Spacer(),
//                Text('shade[' + widget.poll.shadeName.toString().toLowerCase() + ']', style: whiteNormalTextStyle.copyWith(fontSize: 14))
//              ],
//            ),
//            Row(
//              crossAxisAlignment: CrossAxisAlignment.center,
//              children: [
//                Text(widget.poll.rating.toString() + ' ', textAlign: TextAlign.center, style: aquamarineBoldTextStyle.copyWith(fontSize: 16)),
//                Text('light', style: grey5BoldTextStyle.copyWith(fontSize: 14)),
//                Spacer(),
//                Text('shadow[' + widget.poll.authorUsername.toLowerCase() + ']', style: aquamarineNormalTextStyle.copyWith(fontSize: 14))
//              ],
//            )
                ],
              ),
            ),
            Visibility(
              visible: menuIsVisible,
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text('save to favourites', style: whiteBoldTextStyle.copyWith(fontSize: 18)),
                          Spacer(),
                          Icon(Icons.bookmark, color: aquamarine)
                        ],
                      ),
                      Row(
                        children: [
                          Text('share', style: whiteBoldTextStyle.copyWith(fontSize: 18)),
                          Spacer(),
                          Icon(Icons.share, color: aquamarine)
                        ],
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
