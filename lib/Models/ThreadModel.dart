import 'package:shadow/Models/MessageModel.dart';

class Thread {
  final List<Message> threadMessages;
  final List<Message> childMessages;
  final String threadName;
  final String chatName;
  final String creatorUID;
  final String creatorUsername;
  final String pollUID;
  final String shadeUID;
  final String threadUID;
  final DateTime timestamp;

  Thread({this.creatorUsername, this.threadMessages, this.childMessages, this.chatName, this.creatorUID, this.pollUID, this.shadeUID, this.threadUID, this.timestamp, this.threadName});

  @override
  String toString() {
    return childMessages.toString();
  }
}