import 'package:flutter/material.dart';

class Option {
  final String title;
  final String description;
  final Color color;
  final int votes;

  Option({this.title, this.description, this.color, this.votes = 0});

  @override
  String toString() {
    // TODO: implement toString
    return title + " | " + description + " | " + color.toString();
  }
}