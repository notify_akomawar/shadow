import 'package:flutter/cupertino.dart';
import 'package:shadow/Models/PollModel.dart';

class Shade{
  final String uid;
  final String authorUsername;
  final String access;
  final String name;
  String description;
  final String imagePath;
  String guidelines;
  final List shadowList;
  final List pollList;
  final List pendingShadows;
  final List adminList;
  final List modifiers;
  final int numShadows;
  final String nameLowercase;
  final DateTime timestamp;
  final List tags;
  final int numPolls;

  Shade(
      {
        this.numPolls,
        this.tags,
        this.timestamp,
        this.nameLowercase,
        this.numShadows,
        this.uid,
        this.authorUsername,
        this.access,
        this.name,
        this.description,
        this.imagePath,
        this.guidelines,
        this.shadowList,
        this.pollList,
        this.pendingShadows,
        this.adminList,
        this.modifiers
      }
    );

  String toString() {
    return name + " | " + numPolls.toString();
  }
}

//List of Shadows
//String access = [‘private’, ‘public’]
//List of Polls
//List of PendingShadows
//List of Admins
//Profile Image, Name, Description, Guidelines
//Owner - username + uid
//Modifiers []