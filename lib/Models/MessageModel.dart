class Message {
  final String username;
  final String option;
  final String message;
  final DateTime timestamp;
  final bool selected;
  final String authorUID;

  Message({this.authorUID,this.username, this.option, this.message, this.timestamp, this.selected});

  @override
  String toString() {
    return '\n authorUID: $authorUID \n ' + 'authorUsername: $username \n ' + 'option: $option \n ' + 'message: $message \n ' + 'selected: $selected \n' + 'timestamp: $timestamp';
  }

  bool contains(List messages, Message other) {
    for(Message message in messages) {
      if(
        message.message == other.message &&
        message.option == other.option &&
        message.timestamp == other.timestamp &&
        message.authorUID == other.authorUID &&
        message.username == other.username &&
        message.selected == other.selected
      ) return true;
    }
    return false;
  }

  void remove(List messages, Message other) {
    for(int i = 0; i < messages.length; i++) {
      if(
        messages[i].message == other.message &&
        messages[i].option == other.option &&
        messages[i].timestamp == other.timestamp &&
        messages[i].authorUID == other.authorUID &&
        messages[i].username == other.username &&
        messages[i].selected == other.selected
      ) {
        messages.removeAt(i);
      }
    }
  }
}