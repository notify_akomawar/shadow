import 'MessageModel.dart';

class Chat {
  final String chatName;
  final List<Message> messageList;

  Chat({this.chatName, this.messageList});
}