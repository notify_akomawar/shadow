import 'package:flutter/material.dart';
import 'package:shadow/Models/OptionModel.dart';

class OptionList {
  final List options;

  OptionList({this.options});

  @override
  String toString() {
    // TODO: implement toString
    String str = "";
    options.forEach((element) {str = str + element.toString() + " -- ";});
    return str;
  }

  Option getOption(index) {
    return options[index];
  }

  void setOptionTitle(index, value) {
    options[index].title = value;
  }
  void setOptionDesc(index, value) {
    options[index].description = value;
  }
  void setOptionColor(index, value) {
    options[index].color = value;
  }
}