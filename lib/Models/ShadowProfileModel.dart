import 'package:flutter/material.dart';

class ShadowProfile {
  final int light;
  final String uid;
  final String username;
  String description;
  final String email;
  final String profileImage;
  final List shadowers;
  List joinedShades;

  ShadowProfile({this.joinedShades, this.light, this.uid, this.username, this.description, this.email, this.profileImage, this.shadowers});
}