import 'package:cloud_firestore/cloud_firestore.dart';

class Poll {

  final String uid;
  final bool live;
  final String name, description;
  final String authorUsername, authorUID;
  final String shadeName, shadeUID, imagePath;
  int voteCount, rating;
  final int voteCountLimit;
  final List modifiers;
  final DateTime timestamp;
  List options;
  final Map<String, bool> shadowsRated;
  final Map<String, List> shadowsVoted;

      Poll(
      {
        this.voteCountLimit,
        this.shadowsVoted,
        this.options,
        this.uid,
        this.live,
        this.authorUID,
        this.shadeName,
        this.shadeUID,
        this.imagePath,
        this.rating,
        this.name,
        this.description,
        this.modifiers,
        this.voteCount,
        this.authorUsername,
        this.timestamp,
        this.shadowsRated,
      }
    );

  @override
  String toString() {
    return "\nuid: " + uid.toString()
        + "\nlive: " + live.toString()
        + "\nauthorUID: " + authorUID.toString()
        + "\nshadeUID: " + shadeUID.toString()
        + "\nshadowsVoted: " + shadowsVoted.toString()
        + "\n option1: " + options[0].toString()
        + "\n option2: " + options[1].toString()
        + "\n option3: " + options[2].toString();
  }
}